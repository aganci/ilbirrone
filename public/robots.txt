# See http://www.robotstxt.org/wc/norobots.html for documentation on how to use the robots.txt file
Sitemap: http://www.beerky.it/sitemap.xml.gz
User-Agent: *
Disallow: /admin/
Disallow: /utenti/accedi
Disallow: /utenti/esci
Disallow: /utenti/registrati
Disallow: /utenti/auth/facebook
Disallow: /eventi/seleziona-tipo-evento
Disallow: /eventi-birrerie/nuovo
Disallow: /eventi-pubblici/nuovo
Disallow: /recensioni/nuova
Disallow: /birre/nuova
Disallow: /birrerie/nuova
Disallow: /recensioni-birre/nuova
Disallow: /cities
Disallow: /provinces
Disallow: /visitors/visit_pub