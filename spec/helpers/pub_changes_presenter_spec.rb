require 'models/pub_changes'
require 'presenters/pub_changes_presenter'

describe 'PubChangesPresenter' do
  let(:pub) { double(:pub, id: 1234, name: 'a name', email: 'pub@example.com', website: 'http://www.pub.com', closing_days: '0,1',
    full_address: 'an address', phone: 'a phone number', description: 'some description', pub_type: 'Birreria',
    latitude: 1, longitude: -1) }

  let(:view) {
    view = double(:view)
  }

  before do
    allow(view).to receive(:t).with('date.day_names').and_return(['Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab', 'Dom'])
  end

  it "should get different name" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.name').and_return('translated name')

    differences = change_and_get_differences(:name, 'another name')

    expect(differences).to eq({'translated name' => 'another name'})
  end

  it "should get different full address" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.full_address').and_return('translated address')
    
    differences = change_and_get_differences(:full_address, 'another address')

    expect(differences).to eq({'translated address' => 'another address'})
  end

  it "should get different phone" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.phone').and_return('translated phone')

    differences = change_and_get_differences(:phone, 'another phone number')

    expect(differences).to eq({'translated phone' => 'another phone number'})
  end
  
  it "should get different description" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.description').and_return('translated description')
    allow(view).to receive(:render_description).with('another description').and_return('rendered description')
    
    differences = change_and_get_differences(:description, 'another description')

    expect(differences).to eq({'translated description' => 'rendered description'})
  end
  
  it "should get different pub type" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.pub_type').and_return('translated pub type')
    
    differences = change_and_get_differences(:pub_type, 'another pub type')

    expect(differences).to eq({'translated pub type' => 'another pub type'})
  end

  it "should get closed activity" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.closed').and_return('translated closed')
    
    differences = change_and_get_differences(:closed, true)

    expect(differences).to eq({'translated closed' => ''})
  end
  
  it "should get duplicated activity" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.duplicated').and_return('translated duplicated')
    
    differences = change_and_get_differences(:duplicated, true)

    expect(differences).to eq({'translated duplicated' => ''})
  end

  it "should not include email in differences" do
    user = double(:user, email: 'a-user@example.org')
    changes = PubChangesPresenter.new(PubChanges.from(pub, user), view)

    differences = changes.differences_from(pub, user)

    expect(differences).to eq({})
  end

  it "should get note" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.note').and_return('translated note')
    allow(view).to receive(:render_description).with('another note').and_return('rendered note')
    
    differences = change_and_get_differences(:note, 'another note')

    expect(differences).to eq({'translated note' => 'rendered note'})
  end

  it "should get coordinates" do
    allow(view).to receive(:t).with('activerecord.attributes.pub_changes.coordinates').and_return('translated coordinates')
    
    differences = change_and_get_differences(:coordinates, 'another coordinates')

    expect(differences).to eq({'translated coordinates' => 'another coordinates'})
  end

  def change_and_get_differences(message, value)
    changes = PubChangesPresenter.new(PubChanges.from(pub, nil), view)
    changes.send((message.to_s+"=").to_sym, value)
    changes.differences_from(pub, nil)
  end
end