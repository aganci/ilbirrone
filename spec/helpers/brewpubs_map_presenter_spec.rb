require 'presenters/brewpubs_map_presenter'
require 'presenters/breadcrumb'

describe "BrewpubsMapPresenter" do
  let(:view) { double 'view' }
  let(:presenter) do 
    BrewpubsMapPresenter.new('Milano').tap do |p|
      p.view = view
    end
  end

  it "should format title" do
    expect(presenter.title).to eq("Mappa dei brewpub in provincia di Milano")
  end

  it "should get breadcrumb" do
    allow(view).to receive(:brewpub_path).and_return('/brewpub-path')
    allow(view).to receive(:brewpub_path).with(provincia: 'Milano').and_return('/brewpub-milano-path')

    expect(presenter.breadcrumb).to eq([Breadcrumb.new("Brewpub", '/brewpub-path'), Breadcrumb.new('Milano', '/brewpub-milano-path'), Breadcrumb.new("Mappa")])
  end

  it "should format link to list" do
    allow(view).to receive(:brewpub_path).with(provincia: 'Milano').and_return('/brewpub-milano-path')
    allow(view).to receive(:link_to).with("Elenco", '/brewpub-milano-path').and_return('<a href="#">Milano</a>')

    expect(presenter.link_to_list).to eq('<a href="#">Milano</a>')  
  end
end