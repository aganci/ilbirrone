require 'rails_helper'

describe RedirectToCurrentPageAfterSignInHelper do
  it "should not store current path to session given a user path" do
    allow(helper.request).to receive(:fullpath).and_return('/utenti')
    
    helper.store_location

    expect(session[:previous_url]).to be_nil
  end
  
  it "should store current path to session" do
    allow(helper.request).to receive(:fullpath).and_return('/some-path')
    
    helper.store_location

    expect(session[:previous_url]).to eq("/some-path")
  end
  
  it "should store current path to session given user profile path" do
    allow(helper.request).to receive(:fullpath).and_return('/utenti/123')
    
    helper.store_location

    expect(session[:previous_url]).to eq("/utenti/123")
  end
end