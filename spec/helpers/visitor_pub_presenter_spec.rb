require 'presenters/pub_presenter'
require 'presenters/microbrewery_presenter'

describe 'visitor pub presenter' do
  let(:pub) { double("pub", pub_type: 'Birreria', visitor_count: 5, visitor?: false) }
  let(:view) { double("view", current_user: nil) }
  
  it "should not render link given a signed in user that already visit the pub" do
    user = double('user')
    allow(pub).to receive(:visitor?).with(user).and_return(true)
    allow(view).to receive(:current_user).and_return(user)
    
    presenter = PubPresenter.create(pub, view)

    expect(presenter.render_visitor_link).to eq("")
  end

  it "should not render given a microbrevery" do
    presenter = PubPresenter.create(double(pub_type: 'Microbirrificio'), view)

    expect(presenter.render_visitor_link).to eq("")
  end
end