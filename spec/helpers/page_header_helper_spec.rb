#encoding: utf-8
require 'rails_helper'

describe PageHeaderHelper do
  it "render open graph title" do
    allow(helper).to receive(:content_for?).with(:title).and_return(true)
    allow(helper).to receive(:content_for).with(:title).and_return('a title')

    expect(helper.og_title).to eq("a title")
  end

  it "render open graph default title" do
    allow(helper).to receive(:content_for?).with(:title).and_return(false)
    allow(helper).to receive(:content_for).with(:title).and_return('a title')

    expect(helper.og_title).to eq("Recensioni su birre birrerie pub microbirrifici brewpub ed eventi dal mondo della birra")
  end
end