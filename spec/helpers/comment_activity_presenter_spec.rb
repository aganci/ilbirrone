require 'presenters/comment_activity_presenter'

describe 'comment activity presenter' do
  let(:user) { double 'user' }
  let(:comment) { double('comment', user: user, created_at: nil, avatar?: false, cover?: false, commentable_type: nil) }
  let(:view) { double('view') }
  let(:presenter) { CommentActivityPresenter.new(comment, view) }

  it "should render cover changed event" do
    allow(comment).to receive(:cover?).and_return(true)

    expect(view).to receive(:render).with(partial: "home_page/changed_cover_activity", locals: {comment: comment})
    
    presenter.render_description
  end

  it "should render comment added event" do
    expect(view).to receive(:render).with(partial: "home_page/comment_activity", locals: {comment: comment})
    
    presenter.render_description
  end

  describe "user comment" do
    let(:another_user) { double 'another user' }

    before do
      allow(comment).to receive(:commentable_type).and_return("User")
    end

    it "should render text comment on another user profile" do
      allow(comment).to receive(:commentable).and_return(another_user)

      expect(view).to receive(:render).with(partial: "home_page/user_comment_on_another_user", locals: {comment: comment})

      presenter.render_description
    end

    it "should render text comment on same user profile" do
      allow(comment).to receive(:commentable).and_return(user)

      expect(view).to receive(:render).with(partial: "home_page/user_comment", locals: {comment: comment})

      presenter.render_description
    end

  end
end