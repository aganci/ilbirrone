require 'rails_helper'

describe SimpleForm::Inputs do
  before do
    @review = FactoryGirl.build(:review, rating: 3)
    @form = render_rating
  end

  it "should render the input containing the rating value" do
    expect(@form.css(rating_input_selector(@review.rating))).to_not be_empty
  end

  it "should render the div containg rating stars" do
    target_input_id = @form.css(rating_input_selector(@review.rating)).first[:id]
    rating_id = "#{target_input_id}_rating"
    
    expect(@form.css("##{rating_id}")).to_not be_empty
  end

  it "should render input target for raty gem" do
    target_input_id = @form.css(rating_input_selector(@review.rating)).first[:id]

    expect(@form.content).to include("render_editable_rating('review_rating_rating', '#{target_input_id}', 3)")
  end

  private
  def rating_input_selector(value)
    "input[value='#{value}']"
  end

  def render_rating
    html = helper.simple_form_for @review do |f|
      f.input(:rating, as: :rating)
    end
    Nokogiri::HTML::fragment(html)
  end
end
