require 'rails_helper'

describe "pub presenter" do
  before do
    @pub = Pub.new
    @presenter = PubPresenter.new(@pub, self)
  end

  describe "website" do
    it "should add http when missing" do
      @pub.website = "www.example.org"
      expect(@presenter.website).to eq(link_to("Vai al sito", "http://www.example.org", target: "_blank", rel: 'nofollow'))
    end
    
    it "should left unchanged when starts with http" do
      @pub.website = "http://www.example.org"
      expect(@presenter.website).to eq(link_to("Vai al sito", "http://www.example.org", target: "_blank", rel: 'nofollow'))
    end
    
    it "should left unchanged when starts with https" do
      @pub.website = "https://www.example.org"
      expect(@presenter.website).to eq(link_to("Vai al sito", "https://www.example.org", target: "_blank", rel: 'nofollow'))
    end
  end

  it "should render default photo when pub photo not exists" do
    view = double('view')
    expect(view).to receive(:image_tag).and_return("<img src='/a-photo.jpg'>")
    @presenter = PubPresenter.new(@pub, view)
    
    expect(@presenter.render_photo).to eq("<img src='/a-photo.jpg'>")
  end

  it "should render pub photo" do
    allow(@pub).to receive(:images).and_return([FactoryGirl.build(:cover)])
    view = double('view')
    expect(view).to receive(:render).with(partial: 'shared/photo', locals:{imageable: @pub}).and_return("<img src='/a-photo.jpg'>")
    @presenter = PubPresenter.new(@pub, view)
    
    expect(@presenter.render_photo).to eq("<img src='/a-photo.jpg'>")
  end

  it "should render pub type link" do
    @pub.pub_type = "Birreria"
    @presenter = PubPresenter.create(@pub, self)
    expect(@presenter.pub_type_link).to eq(link_to("Birreria", pubs_path))

    @pub.pub_type = "Brewpub"
    @presenter = PubPresenter.create(@pub, self)
    expect(@presenter.pub_type_link).to eq(link_to("Brewpub", brewpub_path))

    @pub.pub_type = "Microbirrificio"
    @presenter = PubPresenter.create(@pub, self)
    expect(@presenter.pub_type_link).to eq(link_to("Microbirrificio", microbreweries_path))
  end

  it "should show number of events" do
    @pub.pub_type = 'Birreria'
    @presenter = PubPresenter.create(@pub, self)
    
    expect(@presenter.tab_events.text).to eq("Eventi")
    
    expect(@presenter.tab_events([FactoryGirl.build(:pub_event), FactoryGirl.build(:pub_event)]).text).to eq("Eventi (2)")
  end

  it "should show number of reviews" do
    @pub.pub_type = 'Birreria'
    @presenter = PubPresenter.create(@pub, self)
    
    expect(@presenter.tab_reviews.text).to eq("Recensioni")
    
    @pub.reviews << FactoryGirl.build(:review)    
    @pub.reviews << FactoryGirl.build(:review)    

    expect(@presenter.tab_reviews.text).to eq("Recensioni (2)")
  end

  it "should show number of beers" do
    @pub.pub_type = 'Brewpub'
    @presenter = PubPresenter.create(@pub, self)
    
    expect(@presenter.tab_beers.text).to eq("Birre")
    
    @pub.beers << FactoryGirl.build(:beer)    
    @pub.beers << FactoryGirl.build(:beer)    

    expect(@presenter.tab_beers.text).to eq("Birre (2)")
  end

  it "should show number of comment" do
    @pub.pub_type = 'Birreria'
    @presenter = PubPresenter.create(@pub, self)
    
    expect(@presenter.tab_comments.text).to eq("Commenti")
    
    @pub.comments << FactoryGirl.build(:comment)

    expect(@presenter.tab_comments.text).to eq("Commenti (1)")
  end
  
  it "should not count cover comments" do
    @pub.pub_type = 'Birreria'
    @presenter = PubPresenter.create(@pub, self)
    
    @pub.comments << FactoryGirl.build(:cover)

    expect(@presenter.tab_comments.text).to eq("Commenti")
  end

end