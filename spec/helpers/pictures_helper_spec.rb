require 'rails_helper'

describe PicturesHelper do
  it "should render pub photo" do
    pub = FactoryGirl.create(:pub_with_cover)
    
    expect(pub.images.size).to eq(1)
    expect(render_photo(pub).to_s).to include('a-path.jpg')    
  end
  
  it "should render beer photo" do
    beer = FactoryGirl.build(:beer)
    beer.comments << FactoryGirl.build(:cover, path: 'beers/image.jpg')
    
    expect(render_photo(beer).to_s).to include('beers/image.jpg')    
  end
end