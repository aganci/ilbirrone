require 'rails_helper'

describe GeolocationHelper do
  it "should render where are you question given a mobile request" do
    expect(helper).to receive(:render).with(partial: 'shared/where_are_you_question')

    helper.render_where_are_you_question(mobile_request)
  end

  it "should not render where are you question given a mobile request" do
    expect(helper).to_not receive(:render)

    helper.render_where_are_you_question(desktop_request)
  end

  it "should not render given a session that already has been rendered" do
    cookies[:where_are_you] = true

    expect(helper).to_not receive(:render)

    helper.render_where_are_you_question(mobile_request)
  end
  
  it "should set cookie" do
    allow(helper).to receive(:render)

    helper.render_where_are_you_question(mobile_request)

    expect(cookies[:where_are_you]).to eq(true)
  end

  it "should store current path in session" do
    allow(helper).to receive(:render)

    helper.render_where_are_you_question(mobile_request(original_url: 'www.example.org/current-path'))
    
    expect(session[:return_to]).to eq("www.example.org/current-path")    
  end

  def mobile_request(options = {})
    double({user_agent: iphone_user_agent, original_url: 'any'}.merge(options))
  end

  def desktop_request
    double(user_agent: desktop_user_agent, original_url: 'any')
  end

  def iphone_user_agent
    'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16'
  end

  def desktop_user_agent
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0'
  end
end