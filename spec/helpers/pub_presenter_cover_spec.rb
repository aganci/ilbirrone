require 'presenters/pub_presenter'

describe "pub presenter add cover" do
  let(:view) { double('view') }
  let(:pub) { double('pub', id: 1234) }
  let(:presenter) { PubPresenter.new(pub, view) }

  it "should render login button given a not logged user" do
    given_a_pub_without_cover
    given_a_not_logged_user
    expect(view).to receive(:render).with(partial: 'comments/add_cover_login_button')

    presenter.render_add_cover
  end

  it "should not render button given a pub with a cover" do
    given_a_pub_with_a_cover
    given_a_not_logged_user
    expect(view).to_not receive(:render)

    presenter.render_add_cover
  end

  it "should render add cover form given a logged user" do
    given_a_pub_without_cover
    given_a_logged_user
    expect(view).to receive(:render).with(partial: 'comments/add_cover_form', locals: {commentable_id: pub.id, commentable_type: pub.class.name})

    presenter.render_add_cover
  end

  it "should render delete cover" do
    a_user = double('user')
    allow(a_user).to receive(:admin?).and_return(false)
    cover = given_a_pub_with_a_cover_owned_by(a_user)
    given_a_logged_user(a_user)

    expect(view).to receive(:render).with(partial: 'comments/delete_cover', locals: {comment: cover})

    presenter.render_delete_cover
  end
  
  it "should render delete cover given an admin user" do
    a_user = double('user')
    allow(a_user).to receive(:admin?).and_return(true)
    cover = given_a_pub_with_a_cover
    given_a_logged_user(a_user)

    expect(view).to receive(:render).with(partial: 'comments/delete_cover', locals: {comment: cover})

    presenter.render_delete_cover
  end

  it "should not render delete cover given a pub without cover" do
    given_a_pub_without_cover

    expect(view).to_not receive(:render)
    
    presenter.render_delete_cover
  end
  
  it "should not render delete cover given a pub with a cover owned by another user" do
    a_user = double('a user')
    another_user = double('another user')
    allow(another_user).to receive(:admin?).and_return(false)
    given_a_pub_with_a_cover_owned_by(a_user)
    given_a_logged_user(another_user)

    expect(view).to_not receive(:render)
    
    presenter.render_delete_cover
  end

  def given_a_pub_without_cover
    allow(pub).to receive(:cover?).and_return(false)
  end
  
  def given_a_pub_with_a_cover
    allow(pub).to receive(:cover?).and_return(true)
    cover = double('cover')
    allow(pub).to receive(:cover).and_return(cover)
    cover
  end

  def given_a_not_logged_user
    allow(view).to receive(:signed_in?).and_return(false)
  end
  
  def given_a_logged_user(user = nil)
    allow(view).to receive(:signed_in?).and_return(true)
    allow(view).to receive(:current_user).and_return(user)
  end

  def given_a_pub_with_a_cover_owned_by(user)
    allow(pub).to receive(:cover_owner).and_return(user)
    given_a_pub_with_a_cover
  end
end