require 'rails_helper'

describe 'event presenter' do
  include PicturesHelper

  it "should show only one date when start and end date are equal" do
    event = FactoryGirl.build(:pub_event, start_date: Date.new(2013, 4, 28), end_date: Date.new(2013, 4, 28))

    presenter = EventPresenter.new(event, self)

    expect(presenter.when).to eq("Domenica 28 aprile 2013")
  end

  it "should render no photo present when event has no photo" do
    presenter = EventPresenter.new(FactoryGirl.build(:pub_event), self)

    expect(presenter.photo_tag).to eq(image_tag('no-photo.jpg'))
  end

  it "should render photo" do
    event = FactoryGirl.build(:pub_event, title: 'a title')
    event.comments << FactoryGirl.build(:cover, path: 'a-path')
    presenter = EventPresenter.new(event, self)

    expect(presenter.photo_tag).to eq(tag(:img, src: "http://ilbirrone.it/a-path", alt: 'a title'))
  end
end