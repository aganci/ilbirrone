require 'rails_helper'

describe 'PublicEventPresenter' do
  it "should include location in full address rendering" do
    event = OpenStruct.new(full_address: 'an address', location: 'a location')

    presenter = PublicEventPresenter.new(event, nil)

    expect(presenter.render_full_address).to eq('a location - an address')
  end
end