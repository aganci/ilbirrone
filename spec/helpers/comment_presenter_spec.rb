require 'presenters/comment_presenter'

describe "CommentPresenter" do
  let(:view) { double 'view', current_user: nil }
  let(:user) {double 'user', admin?: false}
  let(:comment) { double 'comment', user: user, commentable: nil }
  let(:presenter) { CommentPresenter.new(comment, view) }

  it "should render form" do
    allow(view).to receive(:signed_in?).and_return(true)
    expect(view).to receive(:render).with(partial: 'comments/new', locals: {comment: comment})

    presenter.render_form
  end

  it "should render sign in invitation" do
    allow(view).to receive(:signed_in?).and_return(false)
    expect(view).to receive(:render).with(partial: 'comments/sign_in_invitation')

    presenter.render_form
  end

  it "should render commands" do
    allow(view).to receive(:current_user).and_return(user)
    expect(view).to receive(:render).with(partial: 'comments/commands', locals: {comment: presenter})

    presenter.render_commands
  end

  it "should not render commands for not signed in user" do
    allow(view).to receive(:current_user).and_return(nil)

    expect(presenter.render_commands).to be_nil
  end

  it "should not render commands for signed in user different from current user" do
    allow(view).to receive(:current_user).and_return(double 'another user', admin?: false)

    expect(presenter.render_commands).to be_nil
  end

  it "should get default sharing image given a comment without image" do
    allow(view).to receive(:asset_url).with("facebook-logo.png").and_return('http://www.beerky.it/facebook-logo.png')
    allow(comment).to receive(:has_image?).and_return(false)

    expect(presenter.sharing_image_url).to eq('http://www.beerky.it/facebook-logo.png')
  end

  it "should get sharing image url" do
    allow(comment).to receive(:has_image?).and_return(true)
    expect(view).to receive(:photo_url).with(comment).and_return('http://www.beerky.it/a-photo-url.png')
    
    expect(presenter.sharing_image_url).to eq('http://www.beerky.it/a-photo-url.png')
  end

  it "should can delete a comment given an admin user" do
    allow(view).to receive(:current_user).and_return(user)
    allow(user).to receive(:admin?).and_return(true)

    expect(presenter.can_delete?).to eq(true)
  end

  it "should can delete a comment given the recipient as logged user" do
    recipient = double(:recipient, admin?: false)
    allow(view).to receive(:current_user).and_return(recipient)
    allow(comment).to receive(:commentable).and_return(recipient)

    expect(presenter.can_delete?).to eq(true)
  end
end