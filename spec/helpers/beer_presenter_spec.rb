require 'rails_helper'

describe "beer presenter" do
  it "should get empty array when optional data is not specified" do
    beer = Beer.new
    presenter = BeerPresenter.new(beer, nil)

    expect(presenter.optional_data).to be_empty
  end
  
  it "should get optional data" do
    beer = Beer.new(fermentation: 'Alta')
    presenter = BeerPresenter.new(beer, nil)

    fermentation = presenter.optional_data.first

    expect(fermentation.title).to eq(Beer.human_attribute_name(:fermentation))
    expect(fermentation.value).to eq("Alta")
  end

  it "should render description" do
    view = double("view")
    beer = Beer.new(description: 'a beer description')
    expect(view).to receive(:render).with(partial: "beer_description", locals: {description: 'a beer description'})
    
    presenter = BeerPresenter.new(beer, view)

    presenter.render_description
  end

  it "should render empty string when description is not specified" do
    view = double("view")
    beer = Beer.new
    
    presenter = BeerPresenter.new(beer, view)

    presenter.render_description
  end

  it "should render photo" do
    view = double('view')
    beer = FactoryGirl.build(:beer, comments: [FactoryGirl.build(:cover)])
    expect(view).to receive(:render).with(partial: "shared/photo", locals: {imageable: beer}).and_return('a rendered image')
    presenter = BeerPresenter.new(beer, view)

    expect(presenter.render_photo).to eq('a rendered image')
  end

end