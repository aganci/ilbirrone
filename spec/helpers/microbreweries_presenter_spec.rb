require 'rails_helper'

describe "microbreweries presenter" do
  before do
    @view = double('view')
  end
  
  it "should render table title with region name" do
    presenter = MicrobreweriesPresenter.create(@view, ['1', '2'], 'a region')

    expect(presenter.heading).to eq("Microbirrifici a region")
    expect(presenter.sub_heading).to eq("a region (2)")
  end

  it "should render table title when region is not specified" do
    presenter = MicrobreweriesPresenter.create(@view, ['1', '2', '3'], nil)

    expect(presenter.heading).to eq("Microbirrifici d'Italia")
    expect(presenter.sub_heading).to eq("Nuove segnalazioni da tutte le regioni (3)")
  end

  it "should render page title" do
    presenter = MicrobreweriesPresenter.create(@view, nil, nil)

    expect(presenter.title).to eq("Microbirrifici d'Italia")
  end
  
  it "should render page title with region" do
    presenter = MicrobreweriesPresenter.create(@view, nil, 'a region')

    expect(presenter.title).to eq("Microbirrifici della regione a region")
  end

  it "should render view with microbreweries" do
    a_microbrevery = Object.new
    presenter = MicrobreweriesPresenter.create(@view, [a_microbrevery], 'a region')
    
    expect(@view).to receive(:render).with(partial: "microbreweries", locals: {microbreweries: [a_microbrevery]})

    presenter.render
  end

  it "should render view without microbreweries" do
    presenter = MicrobreweriesPresenter.create(@view, [], 'a region')

    expect(@view).to receive(:render).with(partial: "no_microbreweries")

    presenter.render
  end
end