require 'rails_helper'

describe PlacesHelper do
  it "should render cities as a javascript array" do
    expect(render_cities).to include('"Alba, Cuneo, Piemonte"')
  end  

  it "should omit province given a city with the same name" do
    expect(format_city(["Milano", "Milano", "Lombardia"])).to eq("Milano, Lombardia")
  end
end