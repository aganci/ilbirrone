require 'presenters/user_presenter'

describe "user presenter" do
  let(:view) { double('view') }
  let(:user) { double('user') }
  let(:presenter) { UserPresenter.new(user, view) }

  it "should render select avatar" do
    allow(view).to receive(:current_user).and_return(user)
    
    expect(view).to receive(:render).with(partial: 'users/select_avatar')

    presenter.render_select_avatar    
  end

  it "should not render select avatar when user page is from different user" do
    allow(view).to receive(:current_user).and_return(double('another_user'))

    presenter.render_select_avatar    
  end

  it "should render edit profile link" do
    allow(view).to receive(:current_user).and_return(user)
    allow(view).to receive(:edit_user_registration_path).and_return("/edit-path")

    expect(view).to receive(:link_to).with("Modifica Profilo", "/edit-path")

    presenter.render_edit_profile
  end
  
  it "should not render edit profile given another logged user" do
    allow(view).to receive(:current_user).and_return(double('another_user'))

    presenter.render_edit_profile
  end

  it "should render message given a user without visits" do
    allow(user).to receive(:visited_pubs).and_return([])

    expect(view).to receive(:render).with(partial: 'users/no_visits')
    
    presenter.render_map
  end
end