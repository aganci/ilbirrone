require 'presenters/user_comment_presenter'

describe 'user comment presenter' do
  let(:user) { double(:user) }
  let(:view) { double(:view, signed_in?: true, current_user: double(:another_user)) }
  let(:presenter) { UserCommentPresenter.new(user, view) }

  it "should render new comment form" do
    allow(user).to receive(:username).and_return("Alfonso")

    expect(view).to receive(:render).with(partial: 'comments/new_user_comment', locals: {user: user, message: "Lascia un messaggio per Alfonso"})

    presenter.render_new_form
  end

  it "should invite to sign in" do
    allow(view).to receive(:signed_in?).and_return(false)

    expect(view).to receive(:render).with(partial: 'comments/invite_to_sign_in_for_new_message', locals: {user: user})

    presenter.render_new_form
  end

  it "should render the form given a signed in user in his page" do
    allow(view).to receive(:current_user).and_return(user)

    expect(view).to receive(:render).with(partial: 'comments/new_user_comment', locals: {user: user, message: 'A cosa stai pensando?'})

    presenter.render_new_form
  end
end