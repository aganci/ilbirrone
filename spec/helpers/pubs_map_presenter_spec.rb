require 'presenters/pubs_map_presenter'
require 'presenters/breadcrumb'

describe "PubsMapPresenter" do
  let(:view) {double 'view'}
  let(:presenter) do 
    PubsMapPresenter.new('Milano').tap do |p|
      p.view = view
    end
  end

  it "should format title" do
    expect(presenter.title).to eq("Mappa delle birrerie e pub in provincia di Milano")
  end

  it "should get breadcrumb" do
    allow(view).to receive(:pubs_path).and_return('/pubs-path')
    allow(view).to receive(:pubs_path).with(provincia: 'Milano').and_return('/pubs-milano-path')

    expect(presenter.breadcrumb).to eq([Breadcrumb.new("Birrerie e Pub", '/pubs-path'), Breadcrumb.new('Milano', '/pubs-milano-path'), Breadcrumb.new("Mappa")])
  end

  it "should format link to list" do
    allow(view).to receive(:pubs_path).with(provincia: 'Milano').and_return('/pubs-milano-path')
    allow(view).to receive(:link_to).with("Elenco", '/pubs-milano-path').and_return('<a href="#">Milano</a>')

    expect(presenter.link_to_list).to eq('<a href="#">Milano</a>')
  end
end