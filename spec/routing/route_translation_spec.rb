require 'rails_helper'

describe PubsController do
  it "translates show pub route" do
    expect(get: '/birrerie/10').to route_to(controller: "pubs", action: "show", id: "10")
  end

  it "translates search pubs route" do
    expect(get: '/birrerie/cerca?name=any').to route_to(controller: "pubs", action: "search", name: 'any')
  end

  it "translates new pub route" do
    expect(get: '/birrerie/nuova').to route_to(controller: "pubs", action: "new")
  end
end

describe Admin::PubsController do
  it "translates index pubs route" do
    expect(get: '/admin/birrerie').to route_to(controller: "admin/pubs", action: "index")
  end
  
  it "translates edit pub route" do
    expect(get: 'admin/birrerie/10/modifica').to route_to(controller: "admin/pubs", action: "edit", id: "10")
  end
end

describe ReviewsController do
  it "translates new review route" do
    expect(get: '/recensioni/nuova').to route_to(controller: "reviews", action: "new")
  end
end

describe ApplicationController do
  it "translates new session route" do
    expect(get: '/utenti/accedi').to route_to(controller: "devise/sessions", action: "new")
  end

  it "translates destroy session route" do
    expect(delete: '/utenti/esci').to route_to(controller: "devise/sessions", action: "destroy")
  end
end