FactoryGirl.define do 
  factory :user do
    username                'a username'
    sequence(:email)        { |n| "user#{n}@example.com" }                 
    password                'secret'
    password_confirmation   'secret'
    privacy_policy_accepted true
    admin                   false
    
    factory :admin do
      admin true
    end

    factory :user_with_cover do
      after(:create) do |user, evaluator|
        create(:cover, commentable: user, user: user )
      end    
    end

  end

  factory :cover, class: Comment do
    image_type 'Cover'
    path 'a-path.jpg'
    width 100
    height 100
  end

  factory :pub do
    user
    name       "Pub name"
    city       "a city"
    province   "a province"
    region     "a region"
    address    "via del tongo 2"
    status     "approved"
    pub_type   "Birreria"
    country    "Italia"
    postcode   "12345"

    factory :pub_with_cover do
      after(:create) do |pub, evaluator|
        create(:cover, commentable: pub, user: pub.user )
      end    
    end
  end
  
  factory :review do
    user
    pub
    date { 1.day.ago }
    rating  5
    description { Forgery(:lorem_ipsum).characters(100) }
  end

  factory :beer do
    user
    pub
    name "Beer name"
    alcohol_by_volume "4,8"
    color "A beer color"

    factory :beer_with_cover do
      after(:create) do |beer, evaluator|
        create(:cover, commentable: beer, user: beer.user )
      end    
    end
  end

  factory :beer_review do
    user
    beer
    content { Forgery(:lorem_ipsum).characters(100) }
    rating 4
  end

  factory :pub_event do
    pub
    user
    start_date { Date.today }
    end_date { Date.today }
    title 'an event title'
    description { Forgery(:lorem_ipsum).characters(100) }
    website 'http://www.example.org'

    factory :pub_event_with_cover do
      after(:create) do |event, evaluator|
        create(:cover, commentable: event, user: event.user )
      end    
    end
  end
  
  factory :public_event do
    user
    start_date { Date.today }
    end_date { Date.today }
    title 'an event title'
    description { Forgery(:lorem_ipsum).characters(100) }
    website 'http://www.example.org'
    location 'a location'
    address 'an address'
    city 'a city'
    province 'a province'
    region 'a region'
    postcode '12345'
  end

  factory :comment do
    user
    content { Forgery(:lorem_ipsum).characters(200) }
    created_at Date.today
    association :commentable, factory: :pub
  end

  factory :user_comment, class: Comment do
    user
    content { Forgery(:lorem_ipsum).characters(500) }
    created_at Date.today
    association :commentable, factory: :user
  end

  factory :pre_registration do
    user
  end

  factory :visitor do
    confirmed true
  end
end