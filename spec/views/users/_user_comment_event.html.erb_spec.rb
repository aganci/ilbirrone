require 'rails_helper'

describe 'users/_user_comment_event.html.erb' do
  let(:sender) { FactoryGirl.build(:user, id: 1, username: 'sender') }
  let(:comment) { FactoryGirl.build(:comment, id: 1234, user: sender) }
  let(:recipient) { FactoryGirl.build(:user, id: 2, username: 'recipient') }

  before do
    define_current_user_helper nil
  end
    
  it "should render sender and recipient of message" do
    comment.commentable = recipient
    
    render_comment    

    expect(@html.css('p')[0].text).to eq("sender > recipient")
  end

  it "should render user status" do
    comment.commentable = sender
    
    render_comment

    expect(@html.css('p')[0].text).to eq("sender")
  end

  it "should format line breaks" do
    comment.commentable = recipient
    comment.content = "first line\nsecond line"

    render_comment

    expect(@html.css('p')[1].to_s).to eq("<p>first line<br>second line</p>")
  end

  def render_comment
    render_view partial: 'users/user_comment_event', locals: {comment: comment}
  end
end