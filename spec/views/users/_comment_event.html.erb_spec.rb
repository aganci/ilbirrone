require 'rails_helper'

describe 'users/_comment_event.html.erb' do
  it "should render heading" do
    event = FactoryGirl.create(:public_event, title: 'a public event')
    comment = FactoryGirl.create(:comment, commentable: event)
    
    render_view partial: 'users/comment_event', locals: {comment: comment}

    expect(@html.css('a').text).to eq("a public event")
    expect(@html.css('a')[0]['href']).to eq(event_path(event))
  end
end