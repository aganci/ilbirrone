require 'rails_helper'

describe 'geolocation/nearest_pub.html.erb' do
  it "should render add visit link with coordinates and confirmed" do
    @pub = FactoryGirl.build(:pub, id: 1234)
    @latitude = 1.2345
    @longitude = -1.2345
    
    render_view

    expect(@html.css('a')[0]['href']).to eq(geolocated_visit_pub_visitors_path(pub_id: 1234, latitude: 1.2345, longitude: -1.2345, confirmed: true))
    expect(@html.css('a')[0]['data-method']).to eq("post")
  end
  
  it "should render not confirmed visit link" do
    @pub = FactoryGirl.build(:pub, id: 1234)
    @latitude = 1.2345
    @longitude = -1.2345
    
    render_view

    expect(@html.css('a')[1]['href']).to eq(geolocated_visit_pub_visitors_path(pub_id: 1234, latitude: 1.2345, longitude: -1.2345, confirmed: false))
    expect(@html.css('a')[1]['data-method']).to eq("post")
  end
end