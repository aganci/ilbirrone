require 'rails_helper'

describe 'home_page/_recent_events.html.erb' do
  let(:first_event) { FactoryGirl.build(:pub_event, id: 1, title: 'first event') }
  
  it "should show events" do
    render_recent_events

    expect(@html.to_html).to include(event_path(first_event))
  end

  def render_recent_events
    render_view partial: 'home_page/recent_event', locals: {recent_event: first_event}
  end
end