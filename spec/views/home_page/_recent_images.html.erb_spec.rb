require 'rails_helper'

describe 'home_page/_recent_images.html.erb' do
  before do
    first_pub = FactoryGirl.build(:pub, id: '123', name: 'first pub', description: 'first pub description')
    @first_image = FactoryGirl.build(:cover, path: 'a path')
    allow(first_pub).to receive(:images).and_return([@first_image])

    second_pub = FactoryGirl.build(:pub, id: '456', name: 'second pub', description: 'second pub description')
    @second_image = FactoryGirl.build(:cover, path: 'a path')
    allow(second_pub).to receive(:images).and_return([@second_image])

    render_view partial: 'home_page/recent_images', locals: {pubs: [first_pub, second_pub]}
  end

  it "should render indicators" do
    expect(indicator(0)['data-slide-to']).to eq("0")
    expect(indicator(0)['class']).to eq("active")

    expect(indicator(1)['data-slide-to']).to eq("1")
    expect(indicator(1)['class']).to be_nil
  end
  
  it "should render items caption" do
    expect(@html.css('.carousel-caption h4')[0].text).to eq('first pub')
    expect(@html.css('.carousel-caption h4')[1].text).to eq('second pub')
  end

  it "should render pub images" do
    expect(@html.css('.item img')[0]['src']).to eq(view.photo_url(@first_image))
    expect(@html.css('.item img')[1]['src']).to eq(view.photo_url(@second_image))
  end

  it "should render first item as active" do
    expect(@html.css('.carousel-inner > div')[0]['class']).to eq("item active")
    expect(@html.css('.carousel-inner > div')[1]['class']).to eq("item")
  end

  def indicator(index)
    @html.css('ol li')[index]
  end
end