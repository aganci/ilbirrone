require 'rails_helper'

describe 'home_page/show.html.erb' do
  before do
    define_signed_in_helper
    @notifications = []
    @recent_events = []
    @recent_pubs = []
    @popular_pubs = []
  end

  it "should render meta description" do
    render_view template: 'home_page/show', layout: 'layouts/application'

    expect(@html.css("meta[name='description']").size).to eq(1)
    expect(@html.css("meta[name='description']")[0]['content']).to eq(view.t('home_page.show.description'))
  end

  describe "jumbotron" do
    it "should render given a not logged user" do
      render_view template: 'home_page/show', layout: false
      
      expect(@html).to have_selector(".jumbotron")    
    end

    it "should not render given a logged user" do
      define_signed_in_helper true

      render_view template: 'home_page/show', layout: false
      
      expect(@html).not_to have_selector(".jumbotron")    
    end
  end
end