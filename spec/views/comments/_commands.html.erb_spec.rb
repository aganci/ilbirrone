require 'rails_helper'

describe 'comments/_commands.html.erb' do
  let(:presenter) { CommentPresenter.new(FactoryGirl.build(:comment, id: 1234), view) }

  it "should render delete share link" do
    render_commands

    expect(@html.css('a[href="#"]')[0].text).to eq("Condividi su Facebook")
  end

  it "should subscribe onClick event" do
    render_commands

    share_link_id = @html.css('a[href="#"]')[0]['id']

    expect(@html.to_html).to include("$('##{share_link_id}').click")
  end

  it "should set facebook logo to feed dialog" do
    render_commands

    expect(@html.to_html).to include("picture: 'http://test.host/assets/facebook-logo.png'")
  end

  it "should set facebook description to feed dialog" do
    render_commands

    expect(@html.to_html).to include("description: 'Clicca sul link per andare alla pagina del locale su Beerky'")
  end


  def render_commands
    render_view partial: 'comments/commands', locals: {comment: presenter}
  end
end