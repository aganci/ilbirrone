require 'rails_helper'

describe 'comments/_delete_cover.html.erb' do
  it "should render delte link" do
    comment = FactoryGirl.build(:comment, id: 1234)

    render_view partial: 'comments/delete_cover', locals: {comment: comment}

    expect(@html.css('a')[0]['href']).to eq(comment_path(comment))
    expect(@html.css('a')[0]['data-method']).to eq('delete')
  end
end