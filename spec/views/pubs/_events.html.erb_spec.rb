# encoding: utf-8
require 'rails_helper'

describe 'pubs/_events.html.erb' do
  before do
    @pub = FactoryGirl.build(:pub)
  end

  it "should render events" do
    user = FactoryGirl.build(:user, username: 'Mario Rossi', id: 123)
    event = FactoryGirl.build(:pub_event, id: 1234, user: user, 
      start_date: Date.new(2013, 4, 27), end_date: Date.new(2013, 4, 29),
      title: 'Festa della birra artigianale',
      description: 'Serata con birre di tutti i tipi venite gente')

    render_view partial: 'pubs/events', locals: {pub: @pub, events: [event]}

    expect(@html.css('.pub_event').text).to include('Segnalato da Mario Rossi')
    expect(@html.css('.pub_event a')[1]['href']).to eq(user_path(user))
    
    expect(@html.css('.pub_event').text).to include('da sabato 27 aprile 2013 a lunedì 29 aprile 2013')
    expect(@html.css('.pub_event').text).to include('Festa della birra artigianale')
    expect(@html.css('.pub_event').text).to include('Serata con birre di tutti i tipi venite gente')
    
    expect(@html.css('.pub_event').text).to include("Maggiori informazioni...")
    expect(@html.css('.pub_event a')[0]['href']).to eq(event_path(event))
  end
end