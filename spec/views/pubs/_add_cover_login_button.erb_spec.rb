require 'rails_helper'

describe 'comments/_add_cover_login_button.html.erb' do
  it "should render sign in button" do
    render_view

    @html.css('a')[0]['href'] == new_user_session_path
  end
end