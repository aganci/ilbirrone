# encoding: utf-8
require 'rails_helper'

describe 'pubs/show.html.erb' do
  before do
    @pub = FactoryGirl.build(:pub, id: 4567)
    @comment = Comment.new
    define_pub_owner_helper(false)
    define_signed_in_helper
    define_current_user_helper
  end

  it "should render events tab" do
    render_view

    expect(tabs).to eq(['Commenti', 'Recensioni', 'Eventi'])
  end
  
  it "should render events tab for brewpub" do
    @pub.pub_type = 'Brewpub'
    
    render_view

    expect(tabs).to eq(['Commenti', 'Recensioni', 'Birre', 'Eventi'])
  end
  
  it "should render events tab for microbrewery" do
    @pub.pub_type = 'Microbirrificio'
    
    render_view

    expect(tabs).to eq(['Commenti', 'Birre', 'Eventi'])
  end

  it "should render new event link" do
    render_view

    expect(@html.css('#events a').text).to eq('Segnala un evento')
  end

  it "should not show website link when not present" do
    @pub.events << FactoryGirl.build(:pub_event, id: 1234, website: '')

    render_view

    expect(@html.css('.pub_event').text).to_not include("Vai al sito dell'evento")
  end

  it "should render nothing when current user is not the owner" do
    render_view

    expect(@html.css('.pub-owner')).to be_empty
  end
  
  it "should render admin button when current user is the owner" do
    pub = FactoryGirl.build(:pub, id: 1234)
    define_pub_owner_helper(true)
    
    render_view 'pubs/owner', pub: pub

    expect(@html.css('.pub-owner a')[0]['href']).to eq(admin_pub_path(pub))
  end

  it "should render webmaster link" do
    render_view

    expect(@html).to have_link('Il logo di Beerky sul tuo sito web', href: webmaster_pub_path(@pub))    
  end  

  def tabs
    @html.css('.nav-tabs a').map {|element| element.text}
  end
end