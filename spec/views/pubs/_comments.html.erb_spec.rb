require 'rails_helper'

describe 'pubs/_comments.html.erb' do
  before do
    @pub = FactoryGirl.build(:pub)
    @comment = Comment.new
    define_signed_in_helper
    define_current_user_helper
  end

  it "should render comments" do
    @pub.comments << FactoryGirl.build(:comment, content: 'first comment')
    @pub.comments << FactoryGirl.build(:comment, content: 'second comment')

    render_comments

    expect(@html.css('.comment-content').map {|element|element.text.strip}).to eq(['first comment', 'second comment'])
  end

  it "should render author" do
    @pub.comments << FactoryGirl.build(:comment, user: FactoryGirl.build(:user, id: 1, username: 'a username'))

    render_comments
    
    expect(@html.css('.author').text).to include("a username")
  end

  it "should render user image" do
    user = FactoryGirl.build(:user, id: 1, username: 'a username')
    user.avatar = FactoryGirl.build(:comment, path: 'avatar-path')
    @pub.comments << FactoryGirl.build(:comment, user: user)
    
    render_comments

    expect(@html.css('.avatar-left img')[0]['src']).to eq('http://ilbirrone.it/avatar-path')
  end

  def render_comments
    render_view partial: 'pubs/comments', locals: {pub: @pub}
  end
end