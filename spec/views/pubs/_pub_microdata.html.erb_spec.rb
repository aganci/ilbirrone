# encoding: utf-8
require 'rails_helper'

describe 'pubs/_pub_microdata.html.erb' do
  before do
    @pub = FactoryGirl.build(:pub)
  end

  it "should render events" do
    render_view partial: 'pubs/pub_microdata', locals: {pub: @pub}

    expect(@html.css('div[itemtype="http://data-vocabulary.org/Organization"]').count).to eq(1)
    expect(@html.css('meta[itemprop="name"]').first['content']).to eq(@pub.name)
    expect(@html.css('meta[itemprop="region"]').first['content']).to eq(@pub.region)
  end
end