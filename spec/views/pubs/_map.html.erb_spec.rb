require 'rails_helper'

describe 'pubs/_map.html.erb' do
  it "should render map using pub geolocation" do
    pub = double("pub", latitude: 1.2345, longitude: -1.2345)

    render partial: 'pubs/map', locals: {pub: pub, map_id: 'an-id'}

    expect(rendered).to include("new google.maps.LatLng(1.2345, -1.2345)")
  end
end