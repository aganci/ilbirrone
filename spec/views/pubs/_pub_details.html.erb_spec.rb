require 'rails_helper'

describe 'pubs/_pub_details.html.erb' do
  before do
    @pub = FactoryGirl.build(:pub, id: 1234, pub_type: 'Birreria')
    define_pub_owner_helper false
    define_signed_in_helper
  end
  
  it "should render pub type link" do
    render_view partial: 'pubs/pub_details', locals: {pub: PubPresenter.create(@pub, view)}

    expect(@html).to have_link('Birreria', href: pubs_path)    
  end

  it "should render edit link" do
    render_view partial: 'pubs/pub_details', locals: {pub: PubPresenter.create(@pub, view)}

    expect(@html).to have_link('Proponi modifiche', href: change_pub_path(@pub))
    expect(@html.css('.improve-informations-link a')[0]['rel']).to eq('nofollow')
  end
end