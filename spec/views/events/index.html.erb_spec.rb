require 'rails_helper'

describe 'events/index.html.erb' do
  it "should render events" do
    @events = [FactoryGirl.build(:pub_event, title: 'first'), FactoryGirl.build(:pub_event, title: 'second')]

    stub_template "events/_event.html.erb" => "<%= event.title %>"

    render

    expect(rendered).to match(/first/)
    expect(rendered).to match(/second/)
  end
end