require 'rails_helper'

describe 'events/show.html.erb' do
  before do
    define_signed_in_helper
    define_current_user_helper
  end
  
  it "should render event microdata" do
    @event = FactoryGirl.build(:pub_event, title: 'event title', description: 'a description')

    render_view

    expect(@html.to_html).to include('<div itemscope itemtype="http://data-vocabulary.org/Event">')
    expect(@html.css('span[itemprop="summary"]').text).to eq("event title")
  end

  it "should render websitelink with nofollow" do
    @event = FactoryGirl.build(:pub_event, id: '123', website: 'http://www.example.org')

    render_view
    
    expect(@html.to_html).to include('<a href="http://www.example.org" rel="nofollow" target="_blank">Vai al sito dell\'evento</a>')
  end

  it "should set image url" do
    @event = FactoryGirl.build(:pub_event, id: '123')
    @event.comments << FactoryGirl.build(:cover, path: 'event-photo.jpg')

    render_view
    
    expect(view.content_for(:image_url)).to eq("http://ilbirrone.it/event-photo.jpg")
  end
end