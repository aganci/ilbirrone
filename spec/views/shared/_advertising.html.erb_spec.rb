require 'rails_helper'

describe 'shared/_advertising.html.erb' do
  
  it "should render link with target _blank for external sites" do
    render_view

    link_to_external_sites.each do |link|
      expect(link['target']).to eq('_blank')
    end
  end

  def link_to_external_sites
    @html.css('a').select {|element| element['href'].starts_with?('http:')}
  end
end
