require 'rails_helper'

describe 'layouts/_open_graph_meta.html.erb' do
  it "should render facebook application id" do
    render

    expect(rendered).to include('<meta property="fb:app_id" content="326862920753974">')
  end

  it "should render type" do
    render
    
    expect(rendered).to include('<meta property="og:type" content="website">')
  end

  it "should render title" do
    render
    
    expect(rendered).to include('<meta property="og:title" content="Recensioni su birre birrerie pub microbirrifici brewpub ed eventi dal mondo della birra">')
  end

  it "should render image url" do
    render
    
    expect(rendered).to include('<meta property="og:image" content="http://www.beerky.it/assets/facebook-logo.png">')
  end
end