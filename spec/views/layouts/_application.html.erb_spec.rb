require 'rails_helper'

describe 'layouts/application.html.erb' do
  before do
    define_signed_in_helper
    controller.singleton_class.class_eval do
      protected
        def render_where_are_you_question(request)
          "called render_where_are_you_question"
        end
        helper_method :render_where_are_you_question
    end    
  end

  it "should call helper" do
    render_view

    expect(rendered).to include("called render_where_are_you_question")
  end

  it "should render remarketing" do
    allow(Rails).to receive(:env).and_return(double(:production? => true, :test? => nil))
    
    render_view

    expect(rendered).to include("998643037")
  end
end