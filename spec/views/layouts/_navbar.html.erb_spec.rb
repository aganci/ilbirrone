require 'rails_helper'

describe 'layouts/_navbar.html.erb' do
  before do
    define_signed_in_helper(true)
  end
  
  it "should render user avatar" do
    user = "FactoryGirl.create(:user_with_cover)"
    define_current_user_helper user

    render_view     

    expect(@html.css('.navbar-avatar img')[0]['src']).to eq('http://ilbirrone.it/a-path.jpg')
  end

  it "should render users page link" do
    define_signed_in_helper false

    render_view

    expect(find_link("Birronauti")).to_not be_nil
    expect(find_link("Birronauti")['href']).to eq(users_path)
  end

  def find_link(text)
    @html.css('a').each do |element|
      return element if element.text.include?(text)
    end
    nil
  end

end