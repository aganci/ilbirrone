require 'rails_helper'

describe EventQuery do
  it "should minimize query" do
    create_event_with_image
    create_event_with_image

    count = count_queries_for do
        events = EventQuery.new.all
        events.each do |event|
          event.pub.name
          event.images.first.path
        end
    end

    expect(count).to eq(3)
  end

  def create_event_with_image
    FactoryGirl.create(:pub_event_with_cover)
  end
end