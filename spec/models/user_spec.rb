# encoding: utf-8
require 'rails_helper'

describe User do
  before do
    @user = FactoryGirl.build(:user)
  end

  subject { @user }

  it "should save password" do
    expect(@user.encrypted_password).to_not be_empty
  end

  it { should have_many(:reviews).order(date: :desc) }
  it { should have_many(:pubs).order(created_at: :desc) }

  it "should validate username" do
    @user.username = nil

    should_not be_valid

    @user.username = "ab"
    
    should_not be_valid
  end

  it "must have a valid email" do
    @user.email = nil
    should_not be_valid

    @user.email = "user"
    should_not be_valid
    expect(@user.errors.messages[:email]).to include('valore non valido')

    @user.email = "user@example.org"
    should be_valid
  end

  it { should validate_uniqueness_of(:email) }

  it "should fail save given a duplicate email" do
    @user.save!

    expect {@user.dup.save( :validate => false )}.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it "must have correct password and confirmation" do
    @user.password = "secret"
    @user.password_confirmation = "another secret"

    should_not be_valid
    expect(@user.errors.messages[:password_confirmation]).to eq(["non coincide con la conferma"])
  end

  it "should have an avatar" do
    @user.save!
    avatar = @user.build_avatar
    avatar.user = @user
    
    avatar.save!
    avatar.reload

    expect(avatar.commentable_type).to eq("User")
  end

  it "should list approved pubs" do
    approved_pub = FactoryGirl.build(:pub, status: 'approved')
    submitted_pub = FactoryGirl.build(:pub, status: 'submitted')

    user = FactoryGirl.build(:user, email: @user.email, pubs: [approved_pub, submitted_pub])

    expect(user.approved_pubs).to eq([approved_pub])
  end

  it "should have points" do
    expect(@user).to respond_to(:points)
  end

  it "should have city" do
    expect(@user).to respond_to(:city)
  end

  it "should reject admin users from rankings" do
    FactoryGirl.create(:user, username: 'normal user')
    FactoryGirl.create(:admin, username: 'admin')

    expect(User.rankings("", page: 1).map {|user| user.username}).to eq(['normal user'])
  end


  it "should get geolocated visited pubs" do
    not_geolocated_visit = Visitor.new(pub: Pub.new(name: 'pub not geolocated'))
    geolocated_visit = Visitor.new(pub: Pub.new(name: 'pub geolocated', latitude: 1.2345, longitude: 6.789))
    @user.visitors << not_geolocated_visit
    @user.visitors << geolocated_visit

    expect(@user.visited_pubs).to eq([geolocated_visit.pub])
  end

  it "should have user comments separated from avatar" do
    @user.save!
    expect(@user.avatar).to be_nil

    comment = @user.user_comments.build
    comment.content = "some content"
    comment.user = @user
    comment.save!

    expect(@user.avatar).to be_nil
  end
  
  it "should have user avatar separated from comment" do
    @user.save!
    avatar = @user.build_avatar
    avatar.image_type = "Cover"
    avatar.path = "some path"
    avatar.user = @user
    avatar.save!

    expect(@user.user_comments).to be_empty
  end

  it "should have comments separated from user comment" do
    @user.save!
    comment = @user.user_comments.build
    comment.content = "any content"
    comment.user = @user
    comment.save!

    expect(@user.comments).to be_empty
  end


end
