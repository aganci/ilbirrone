require 'rails_helper'

describe PublicEvent do

  before do
    @event = FactoryGirl.build(:public_event)
  end

  it "should validate location" do
    @event.location = ""

    expect(@event).to_not be_valid
  end
  
  it "should validate address" do
    @event.address = ""

    expect(@event).to_not be_valid
  end
  
  it "should validate city" do
    @event.city = ""

    expect(@event).to_not be_valid
  end
  
  it "should validate province" do
    @event.province = ""

    expect(@event).to_not be_valid
  end
  
  it "should validate region" do
    @event.region = ""

    expect(@event).to_not be_valid
  end
  
  it "should validate postcode" do
    @event.postcode = ""

    expect(@event).to_not be_valid
  end

  it "should be valid" do
    expect(@event).to be_valid
  end
end