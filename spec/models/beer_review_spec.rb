require 'rails_helper'

describe BeerReview do
  before do
    @review = FactoryGirl.create(:beer_review)
  end

  it "should have a valid rating" do
    @review.rating = nil
    expect(@review).to_not be_valid
    
    @review.rating = 0
    expect(@review).to_not be_valid

    @review.rating = 1
    expect(@review).to be_valid
  end

end