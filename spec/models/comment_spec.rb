require 'rails_helper'

describe Comment do
  let(:comment) { FactoryGirl.build(:comment) }

  it "should be valid" do
    expect(comment).to be_valid
  end

  it "should have a relation with pubs" do
    pub = FactoryGirl.build(:pub)
    
    expect(pub.comments.new).to be_kind_of(Comment)
  end

  it "should validate content lenght" do
    ['x' * 9].each do |value|
      comment.content = value
      expect(comment).to_not be_valid
    end

    ['x' * 10].each do |value|
      comment.content = value
      expect(comment).to be_valid
    end
  end

  it "should have a relation with users" do
    user = FactoryGirl.build(:user)
    
    expect(user).to have_many(:comments)
  end

  it "should delete image after destroy" do
    comment.path = '/a-path'
    comment.width = 100
    comment.height = 100
    comment.save!

    ftp_client = double()
    expect(FtpClient).to receive(:image_server).and_return(ftp_client)
    expect(ftp_client).to receive(:delete_file).with('/a-path')

    comment.destroy

    expect(Comment.all.size).to eq(0)
  end

  it "should know if it is an avatar" do
    expect(comment).to_not be_avatar

    comment.commentable_type = "User"
    expect(comment).to_not be_avatar
    
    comment.image_type = "Cover"
    expect(comment).to be_avatar
  end


  it "should contains at least 2000 chars" do
    comment.content = Forgery(:lorem_ipsum).characters(2000)
    comment.save!

    expect(comment.content.size).to eq(2000)
  end
end
