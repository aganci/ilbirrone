require 'models/comment_registration'

describe CommentRegistration do
  describe "#upload_comment" do
    let(:comment) { double(:comment, :content= => nil, :user= => nil) }
    let(:comments) { double(:comments, build: comment) }
    let(:pub) { double(:pub, comments: comments) }
    let(:user) { double(:user) }
    let(:visitor) { stub_const("Visitor", double(:visitor)) }
    let(:notification) { stub_const("Notification", double(:notification)) }
    
    it "should create pub visit given a successful comment save" do
      allow(comment).to receive(:save).and_return(true)
      allow(notification).to receive(:notify_pub_comment)

      expect(visitor).to receive(:find_or_create_by).with(user, pub)
      
      CommentRegistration.upload_comment(pub, "any content", nil, user)
    end

    it "should not create pub visit given a failure save" do
      allow(comment).to receive(:save).and_return(false)

      expect(visitor).to_not receive(:create)
      
      CommentRegistration.upload_comment(pub, "any content", nil, user)
    end

  end

  describe "#upload_user_comment" do
    it "should create a user comment" do
      logged_user = double(:logged_user)
      user_b = double(:user_b)
      comment = double(:comment)
      comments = double(:comments, build: comment)
      allow(user_b).to receive(:user_comments).and_return(comments)

      expect(comment).to receive(:content=).with('a message for user b')
      expect(comment).to receive(:user=).with(logged_user)
      expect(comment).to receive(:save)
      
      CommentRegistration.upload_user_comment(user_b, "a message for user b", nil, logged_user)
    end
  end
end