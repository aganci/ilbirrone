require 'rails_helper'

describe PubQuery do
  describe 'geolocalization' do
    it "should get near pubs" do
      second = FactoryGirl.create(:pub)
      third = FactoryGirl.create(:pub)
      first = FactoryGirl.create(:pub)
      not_approved = FactoryGirl.create(:pub, status: 'submitted')
      
      allow(Pub).to receive(:near).with([1.234, -1.234], 20, units: :km).and_return([first, second, third])

      expect(PubQuery.near(1.234, -1.234, 4)).to eq([first, second, third])
    end

    it "should get empty array given no near pubs" do
      allow(Pub).to receive(:near).and_return(nil)

      expect(PubQuery.near(1.234, -1.234, 4)).to eq([])
    end
  end

  it "should execute only one query for pubs and reviews" do
    create_pub_with_review('first', 'Milano')
    create_pub_with_review('second', 'Milano')
    create_pub_with_review('third', 'Milano')
    
    query = PubQuery.new

    count = count_queries_for do
        pubs = query.find_by_location('Milano')
        pubs.each do |pub|
          rating = pub.rating
        end
    end

    expect(count).to eq(2)
  end

  it "should order by rating then by name" do
    create_pub_with_review('first', 'Milano', '3')
    create_pub_with_review('second', 'Milano', '4')
    create_pub_with_review('thirda', 'Milano', '5')
    create_pub_with_review('thirdb', 'Milano', '5')
    
    query = PubQuery.new

    pubs = query.find_by_location('Milano')

    expect(pubs.map {|pub| pub.name}).to eq(['thirda', 'thirdb', 'second', 'first'])
  end

  it "should get microbreweries and brewpub ordered by most recent" do
    FactoryGirl.create(:pub, name: 'region a', pub_type: "Microbirrificio", created_at: 1.hour.ago)
    FactoryGirl.create(:pub, name: 'first region b', pub_type: "Brewpub", created_at: 2.hour.ago)
    FactoryGirl.create(:pub, name: 'second region b', pub_type: "Microbirrificio", created_at: 3.hour.ago)
    FactoryGirl.create(:pub, name: 'not a Microbirrificio', created_at: 1.hour.ago)

    query = PubQuery.new

    result = query.microbreweries_by_region

    expect(result.map { |pub| pub.name }).to eq(["region a", "first region b", "second region b"])
  end

  it "should filter microbreweries and brewpub filtered by region" do
    FactoryGirl.create(:pub, name: 'region a', pub_type: "Microbirrificio", region: "a", created_at: 1.hour.ago)
    FactoryGirl.create(:pub, name: 'first region b', pub_type: "Brewpub", region: "b", created_at: 2.hour.ago)
    FactoryGirl.create(:pub, name: 'second region b', pub_type: "Microbirrificio", region: "b", created_at: 3.hour.ago)
    FactoryGirl.create(:pub, name: 'not a Microbirrificio', created_at: 1.hour.ago)

    query = PubQuery.new

    result = query.microbreweries("b")

    expect(result.map { |pub| pub.name }).to eq(["first region b", "second region b"])
  end

  it "should minimize query count when access to beer rating" do
    pub = FactoryGirl.create(:pub, pub_type: "Microbirrificio")
    beer = FactoryGirl.create(:beer, pub: pub)
    FactoryGirl.create(:beer_review, rating: 5, beer: beer)
    FactoryGirl.create(:beer_review, rating: 4, beer: beer)
    FactoryGirl.create(:beer_review, rating: 3, beer: beer)
    another_beer = FactoryGirl.create(:beer, pub: pub)
    FactoryGirl.create(:beer_review, rating: 2, beer: another_beer)

    count = count_queries_for do
      pub = PubQuery.find pub.id
      pub.beers.each {|beer| beer.rating}
    end

    expect(count).to eq(8)
  end

  it "should minimize query count when access comments" do
    pub = FactoryGirl.create(:pub)
    FactoryGirl.create(:comment, commentable: pub, user: FactoryGirl.create(:user_with_cover))
    FactoryGirl.create(:comment, commentable: pub, user: FactoryGirl.create(:user_with_cover))
    FactoryGirl.create(:comment, commentable: pub, user: FactoryGirl.create(:user_with_cover))

    count = count_queries_for do
      pub = PubQuery.find pub.id
      pub.comments.each {|comment| comment.user.avatar}
    end

    expect(count).to eq(9)
  end

  it "should preload visitors" do
    pub = FactoryGirl.create(:pub)
    FactoryGirl.create(:visitor, pub: pub, user: FactoryGirl.create(:user))
    FactoryGirl.create(:visitor, pub: pub, user: FactoryGirl.create(:user))
    FactoryGirl.create(:visitor, pub: pub, user: FactoryGirl.create(:user))

    count = count_queries_for do
      pub = PubQuery.find pub.id
      pub.visitors.each {|visitor| visitor.user.id}
    end

    expect(count).to eq(8)
  end


  def create_pub_with_review(name, city, rating = 5)
    pub = FactoryGirl.create(:pub, name: name, city: city)
    FactoryGirl.create(:review, pub: pub, rating: rating)
  end
end