require 'rails_helper'

describe Pub do
  before { @pub = FactoryGirl.build(:pub) }

  subject { @pub }

  it "should be valid" do
    should be_valid
  end

  it "can't have a blank name" do
    @pub.name = nil

    should_not be_valid
  end
  
  it "can't have a blank address" do
    @pub.address = nil

    should_not be_valid
  end
  
  it "can't have a blank city" do
    @pub.city = nil

    should_not be_valid
  end

  it "can't have a blank province" do
    @pub.province = nil

    should_not be_valid
  end

  it { should have_many(:reviews).dependent(:destroy) }


  it "should format address" do
    @pub.address = 'via Roma, 7'
    @pub.city = 'Alba'
    @pub.province = 'Cuneo'
    @pub.postcode = '12051'

    expect(@pub.full_address).to eq('via Roma, 7 - 12051 Alba (Cuneo)')
  end

  it "should format city and province" do
    @pub.city = "Alba"
    @pub.province = "Cuneo"

    expect(@pub.city_with_province).to eq("Alba (Cuneo)")
    
    @pub.city = "Cuneo"
    @pub.province = "Cuneo"
    
    expect(@pub.city_with_province).to eq("Cuneo")
  end

  it "shoud order its reviews by most recents" do
    newer = FactoryGirl.create(:review, pub: @pub, date: 1.month.ago)
    oldest = FactoryGirl.create(:review, pub: @pub, date: 3.month.ago)
    older = FactoryGirl.create(:review, pub: @pub, date: 2.month.ago)

    expect(@pub.reviews).to eq([newer, older, oldest])
  end

  it "should calculate average" do
    FactoryGirl.create(:review, pub: @pub, rating: 3)
    FactoryGirl.create(:review, pub: @pub, rating: 5)

    expect(@pub.rating).to eq(4.0)
  end

  it "generates friendly id" do
    @pub.name = "a name"
    @pub.city = "a city"

    @pub.save

    expect(@pub.slug).to eq('a-name-a-city')
  end

  it "knows the user that creates it" do
    user = FactoryGirl.create(:user)
    pub = FactoryGirl.create(:pub, user: user)

    expect(pub.user).to eq(user)
  end

  it "is not valid when the user has not been specified" do
    @pub.user = nil

    should_not be_valid
  end

  it "should create beer" do
    @pub.save!
    beer = @pub.beers.build

    expect(beer.pub).to eq(@pub)
  end

  it { should have_many(:beers).dependent(:destroy) }


  it "should list beers in alphabetical order" do
    FactoryGirl.create(:beer, name: 'a', pub: @pub)
    FactoryGirl.create(:beer, name: 'c', pub: @pub)
    FactoryGirl.create(:beer, name: 'b', pub: @pub)

    expect(@pub.beers.map {|b| b.name}).to eq(['a', 'b', 'c'])
  end

  it "should store location" do
    pub = FactoryGirl.build(:pub)
    pub.set_location(1.2345, -1.12345)

    expect(pub.latitude).to eq(1.2345)
    expect(pub.longitude).to eq(-1.12345)

    expect(pub).to have_coordinates 
  end

  it { should have_many(:comments).dependent(:destroy) }
  it { should have_many(:visitors).dependent(:destroy) }

  it "should count confirmed visit grouped by user" do
    @pub.visitors << Visitor.new(confirmed: true)
    @pub.visitors << Visitor.new(confirmed: false)
    @pub.visitors << Visitor.new(confirmed: true)
    user = FactoryGirl.build(:user)
    @pub.visitors << Visitor.new(user: user, confirmed: true)
    @pub.visitors << Visitor.new(user: user, confirmed: true)

    expect(@pub.visitor_count).to eq(3)
  end

  it "should find confirmed visits" do
    user = FactoryGirl.build(:user, id: 1234)
    expect(@pub.visitor?(user)).to eq(false)
    
    visitor = Visitor.new(pub: @pub, user: user, confirmed: false)
    @pub.visitors << visitor
    anonymous_visitor = Visitor.new(pub: @pub)
    @pub.visitors << anonymous_visitor

    expect(@pub.visitor?(user)).to eq(false)

    confirmed_visitor = Visitor.new(pub: @pub, user: user, confirmed: true)
    @pub.visitors << confirmed_visitor

    expect(@pub.visitor?(user)).to eq(true)
    
    expect(@pub.visitor?(nil)).to eq(false)
  end

  it { should have_many(:events).dependent(:destroy) }
end
