require 'rails_helper'

describe "most recent activities" do
  before do
    @three_hours_ago = 3.hour.ago
    @two_hours_ago = 2.hour.ago 
    @one_hour_ago = 1.hour.ago
  end

  it "should get reviews and preload user and pub" do
    @three_hours_ago = 3.hour.ago
    @two_hours_ago = 2.hour.ago
    @one_hour_ago = 1.hour.ago

    FactoryGirl.create(:review, created_at: @three_hours_ago)
    FactoryGirl.create(:review, created_at: @two_hours_ago)
    FactoryGirl.create(:review, created_at: @one_hour_ago)
    FactoryGirl.create(:review, created_at: @one_hour_ago, pub: FactoryGirl.create(:pub, status: 'submitted'))

    count = count_queries_for do
      @reviews = ReviewQuery.most_recent(2).map do |review| 
        review.user.username
        review.pub.name
        review.created_at.to_s
      end
    end

    expect(@reviews).to eq([@one_hour_ago.to_s, @two_hours_ago.to_s])
    expect(count).to eq(1)
  end
end