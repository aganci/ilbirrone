require 'rails_helper'

describe 'user points update' do
  it "should udate points on save review" do
    user = FactoryGirl.create(:user)
    review = FactoryGirl.build(:review, user: user)

    review.save!

    expect(user.points).to eq(6)

    review.destroy
    
    expect(user.points).to eq(1)
  end

  it "should include update points module" do
    expect(Comment.included_modules).to include(UserPointsUpdate)
    expect(BeerReview.included_modules).to include(UserPointsUpdate)
    expect(Beer.included_modules).to include(UserPointsUpdate)
    expect(Event.included_modules).to include(UserPointsUpdate)
    expect(Pub.included_modules).to include(UserPointsUpdate)
  end

  it "should set point for user sign up" do
    user = FactoryGirl.create(:user)
    
    expect(user.points).to eq(1)    
  end
end