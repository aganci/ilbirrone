require 'models/pub_changes'

describe 'pub changes' do
  let(:pub) { double(:pub, id: 1234, name: 'a name', email: 'pub@example.com', website: 'http://www.pub.com', closing_days: '0,1',
    full_address: 'an address', phone: 'a phone number', description: 'some description', pub_type: 'a type', latitude: 1.2344, longitude: -4.1234556) }

  it "should take informations from pub" do
    changes = PubChanges.from(pub, nil)

    expect(changes.name).to eq('a name')
    expect(changes.pub_id).to eq(1234)
    expect(changes.pub_email).to eq('pub@example.com')
    expect(changes.website).to eq('http://www.pub.com')
    expect(changes.closing_days).to eq('0,1')
    expect(changes.coordinates).to eq('1.2344, -4.1234556')
  end  

  it "should take email from current user" do
    user = double(:user, email: 'user@example.org')

    changes = PubChanges.from(pub, user)

    expect(changes.user_email).to eq('user@example.org')
  end

end