require 'rails_helper'

describe Visitor do
  it { should belong_to(:user) }
  it { should belong_to(:pub) }
  it { should respond_to(:longitude) }
  it { should respond_to(:confirmed) }
  
  it "should be confirmed by default" do
    visit = Visitor.create(pub: FactoryGirl.create(:pub))

    expect(visit).to be_confirmed
  end
end
