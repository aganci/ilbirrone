require 'rails_helper'

RSpec.describe Notification, :type => :model do
  describe "pub approval" do
    let(:pub) { FactoryGirl.create(:pub, id: 1234) }
    
    it "should save notification" do
      expect {Notification.notify_pub_approval(pub)}.to change(Notification, :count).by(1)
    end

    it "should send email" do 
      expect(NotificationsMailer).to receive(:pub_approved).and_return(double(:mailer, deliver: true))

      Notification.notify_pub_approval(pub)
    end

    it "should set notification info" do
      notification = Notification.notify_pub_approval(pub)

      expect(notification.pub).to eq(pub)
    end
  end

  describe "pub review" do
    it "should save notification" do
      review = FactoryGirl.create(:review)

      expect {Notification.notify_pub_review(review)}.to change(Notification, :count).by(1)
    end
  end

  describe "pub comment" do
    it "should save notification" do
      comment = FactoryGirl.create(:comment)

      expect { Notification.notify_pub_comment(comment) }.to change(Notification, :count).by(1)
    end

    it "should set notification info" do
      comment = FactoryGirl.create(:comment)

      notification = Notification.notify_pub_comment(comment)

      expect(notification.comment).to eq(comment)
    end
  end

  describe "user registration" do
    it "should save notification" do
      user = FactoryGirl.create(:user)

      expect { Notification.notify_user_registration(user) }.to change(Notification, :count).by(1)
    end
  end

  describe "event creation" do
    it "should set notification info" do
      event = FactoryGirl.create(:public_event)

      notification = Notification.notify_event(event)

      expect(notification.event).to eq(event)
      expect(notification.notification_type).to eq('event')
    end
  end

  describe "beer notification" do
    it "should set notification info" do
      beer = FactoryGirl.create(:beer)

      notification = Notification.notify_beer(beer)

      expect(notification.beer).to eq(beer)
      expect(notification.notification_type).to eq('beer')
    end
  end

  describe "beer review notification" do
    it "should set notification info" do
      review = FactoryGirl.create(:beer_review)

      notification = Notification.notify_beer_review(review)

      expect(notification.beer_review).to eq(review)
      expect(notification.notification_type).to eq('beer_review')
    end
  end

  describe "comment delete" do
    it "should delete related notification" do
      pub = FactoryGirl.create(:pub)

      CommentRegistration.upload_comment(pub, "any content", nil, FactoryGirl.create(:user))

      expect { Comment.last.destroy }.to change(Notification, :count).by(-1)
    end
  end

  describe "delete review" do
    it "should delete related notification" do
      review = FactoryGirl.create(:review)

      Notification.notify_pub_review(review)

      expect { Review.last.destroy }.to change(Notification, :count).by(-1)
    end
  end

end
