require 'models/user_points'

describe UserPoints do
  let(:user) { double('user', approved_pubs: [], reviews: [], beer_reviews: [], comments_with_image: [], text_only_comments: [], beers: [], events: []) }
  
  it "should give points for subscription" do
    should_set_points 1

    UserPoints.calculate_on(user)
  end

  it "should give points for pub approvation" do
    allow(user).to receive(:approved_pubs).and_return([double('pub')])

    should_set_points 1 + 10

    UserPoints.calculate_on(user)
  end

  it "should give points for event" do
    allow(user).to receive(:events).and_return([double('event')])

    should_set_points 1 + 10

    UserPoints.calculate_on(user)
  end

  it "should give points for pub review" do
    allow(user).to receive(:reviews).and_return([double('review')])

    should_set_points 1 + 5

    UserPoints.calculate_on(user)
  end
  
  it "should give points for beer insertion" do
    allow(user).to receive(:beers).and_return([double('beer')])

    should_set_points 1 + 10

    UserPoints.calculate_on(user)
  end

  it "should give points for beer reviews" do
    allow(user).to receive(:beer_reviews).and_return([double('beer review')])

    should_set_points 1 + 5

    UserPoints.calculate_on(user)
  end

  it "should give points for comment with image" do
    allow(user).to receive(:comments_with_image).and_return([double('comment with image')])

    should_set_points 1 + 5

    UserPoints.calculate_on(user)
  end

  it "should give points for comment without image" do
    allow(user).to receive(:text_only_comments).and_return([double('comment')])

    should_set_points 1 + 1

    UserPoints.calculate_on(user)
  end

  def should_set_points(value)
    expect(user).to receive(:points=).with(value)
  end
end