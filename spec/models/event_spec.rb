require 'rails_helper'

describe Event do
  before do
    @event = FactoryGirl.build(:pub_event)
  end

  it "should have a title" do
    @event.title = ''

    expect(@event).to_not be_valid 
  end

  it "should be related to pub and user" do
    pub = FactoryGirl.build(:pub)
    user = FactoryGirl.build(:user)

    expect(user.events.build(pub: pub)).to_not be_nil
    expect(pub.events.build(user: user)).to_not be_nil
  end

  it "should have description" do
    @event.description = ''

    expect(@event).to_not be_valid
  end

  it "should have a start date" do
    @event.start_date = 1.day.ago

    expect(@event).to be_valid
  end
  
  it "should not have end date in the past" do
    @event.end_date = 1.day.ago

    expect(@event).to_not be_valid
  end
  
  it "should not have end date before start date" do
    @event.start_date = Date.tomorrow
    @event.end_date = Date.today

    expect(@event).to_not be_valid
  end

  it "should have friendly id" do
    event = FactoryGirl.build(:pub_event, title: 'An event title')
    event.save!

    expect(event.friendly_id).to eq('an-event-title')
  end

  it "should have comments" do
    event = FactoryGirl.build(:pub_event)

    expect(event.comments.build.commentable_type).to eq('Event')
  end

  it "should validate website" do
    @event.website = "invalid-url"

    expect(@event).to_not be_valid
    
    @event.website = "http://valid-url"

    expect(@event).to be_valid
    
    @event.website = "https://valid-url"

    expect(@event).to be_valid
  end

end
