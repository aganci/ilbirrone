require 'rails_helper'

describe 'search presenter' do
  before do
    @view = double('view')
  end
  
  it "should render pubs and beers" do
    beers = [FactoryGirl.build(:beer)]
    pubs = [FactoryGirl.build(:pub)]
    view_should_render_beers(beers)
    view_should_render_pubs(pubs)

    presenter = SearchPresenter.new(beers, pubs)

    presenter.render(@view)
  end

  it "should render only pubs when beers are not found" do
    pubs = [FactoryGirl.build(:pub)]
    view_should_render_pubs(pubs)

    presenter = SearchPresenter.new([], pubs)

    presenter.render(@view)
  end

  it "should render only beers when pubs are not found" do
    beers = [FactoryGirl.build(:beer)]
    view_should_render_beers(beers)

    presenter = SearchPresenter.new(beers, [])

    presenter.render(@view)
  end

  it "should render message for no results" do
    presenter = SearchPresenter.new([], [])
    expect(@view).to receive(:render).with(partial: 'search/no_results')

    presenter.render_add_pub_invitation(@view)
  end

  it "should render message when results are present" do
    beers = [FactoryGirl.build(:beer)]
    pubs = [FactoryGirl.build(:pub)]
    presenter = SearchPresenter.new(beers, pubs)
    expect(@view).to receive(:render).with(partial: 'search/with_results')

    presenter.render_add_pub_invitation(@view)
  end


  def view_should_render_pubs(pubs)
    expect(@view).to receive(:render).with(partial: 'search/pubs', locals: {pubs: pubs}).and_return("some html")
  end

  def view_should_render_beers(beers)
    expect(@view).to receive(:render).with(partial: 'search/beers', locals: {beers: beers}).and_return("some html")
  end
end