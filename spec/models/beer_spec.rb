require 'rails_helper'

describe Beer do
  it "should know its pub" do
    beer = Beer.new

    beer.pub = FactoryGirl.build(:pub)
  end

  it "should know its user" do
    beer = Beer.new

    beer.user = FactoryGirl.build(:user)
  end

  it "should validate alcohol by volume" do
    beer = FactoryGirl.build(:beer)
    expect(beer).to be_valid

    beer.alcohol_by_volume = nil
    expect(beer).to_not be_valid

    beer.alcohol_by_volume = 'a'
    expect(beer).to_not be_valid

    beer.alcohol_by_volume = '12'
    expect(beer).to be_valid
    
    beer.alcohol_by_volume = '1,2'
    expect(beer).to be_valid

    beer.alcohol_by_volume = '1.2'
    expect(beer).to_not be_valid

    beer.alcohol_by_volume = '1,21'
    expect(beer).to_not be_valid
  end

  it "should validate color" do
    beer = FactoryGirl.build(:beer)
    
    beer.color = nil

    expect(beer).to_not be_valid
  end

  it "should have reviews" do
    expect(Beer.new).to respond_to(:reviews)    
  end

  it "should calculate rating" do
    beer = Beer.new
    beer.reviews << BeerReview.new(rating: 3)
    beer.reviews << BeerReview.new(rating: 5)

    expect(beer.rating).to eq(4.0)
  end

  it "should be zero the rating without reviews" do
    beer = Beer.new

    expect(beer.rating).to eq(0.0)
  end

  it "should delete its comments" do
    beer = FactoryGirl.create(:beer, comments: [FactoryGirl.create(:comment)])

    beer.destroy

    expect(Comment.all.count).to eq(0)
  end

  it "should delete its reviews" do
    beer = FactoryGirl.create(:beer)
    FactoryGirl.create(:beer_review, beer: beer)

    beer.destroy

    expect(BeerReview.all.count).to eq(0)
  end

end