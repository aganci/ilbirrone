require 'rails_helper'

describe ModelRemoteImage do
  it "should format remote path" do
    pub = FactoryGirl.create(:pub)

    image = ModelRemoteImage.new(pub, "Pub")

    expect(image.path('local.jpg')).to eq('pubs/pub-name-a-city.jpg')
  end
end