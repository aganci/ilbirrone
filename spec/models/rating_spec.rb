require 'rails_helper'

describe Rating do
  
  it "should return zero given no reviews" do
    expect(Rating.calculate([])).to eq(0.0)
  end

  it "should calculate average" do
    expect(Rating.calculate([a_review(3.0), a_review(5.0)])).to eq(4.0)
  end
  
  it "should round to 0.5 precision" do
    expect(Rating.calculate([a_review(3.0), a_review(3.0), a_review(5.0)])).to eq(3.5)
  end

  def a_review(rating)
    FactoryGirl.build(:review, rating: rating)
  end
end