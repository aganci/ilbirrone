require 'rails_helper'

describe "review" do
  before do
    @review = FactoryGirl.create(:review)
  end

  it "should create a review" do
    expect(@review.pub).to_not be_nil
  end

  it "must have a user" do
    @review.user = nil

    expect(@review).to_not be_valid
  end

  it "must have a valid rating" do
    @review.rating = nil
    expect(@review).to_not be_valid
    
    @review.rating = 0
    expect(@review).to_not be_valid
  end

  it "must have a valid date" do
    @review.date = nil
    
    expect(@review).to_not be_valid
  end

  it "must have a valid review" do
    @review.description = nil

    expect(@review).to_not be_valid

    @review.description = "x" * 99
    
    expect(@review).to_not be_valid
  end

  it "can't have a date in the future" do
    @review.date = DateTime.now + 1.day
    
    expect(@review).to_not be_valid
  end
end
