require 'rails_helper'
require 'RMagick'

include Magick

describe GeolocatedPub do
  before do
    @view = double('view')
    allow(@view).to receive(:pub_url).and_return('http://pub-url.com')
    allow(@view).to receive(:request).and_return(OpenStruct.new(protocol: 'http://', host_with_port: 'example.com'))
    @pub = FactoryGirl.build(:pub)
    @geolocated = GeolocatedPub.new(@pub, 100, 200)
  end

  it "should format pub url" do
    hash = @geolocated.to_hash(@view)

    expect(hash[:path]).to eq('http://pub-url.com')
  end

  it "should get no photo url" do
    hash = @geolocated.to_hash(@view)

    expect(hash[:image_url]).to eq("http://example.com/assets/no-photo.jpg")
  end
  
  it "should get photo url" do
    @pub.comments << FactoryGirl.build(:cover, path: 'pubs/a-pub.jpg')

    hash = @geolocated.to_hash(@view)

    expect(hash[:image_url]).to eq("http://ilbirrone.it/pubs/a-pub.jpg")
  end
  
  it "should get description" do
    @pub.description = 'a description'

    hash = @geolocated.to_hash(@view)

    expect(hash[:description]).to eq("a description")
  end

  it "should get description given a nil pub description" do
    @pub.description = nil

    hash = @geolocated.to_hash(@view)

    expect(hash[:description]).to be_nil
  end

  it "should get pub type" do
    hash = @geolocated.to_hash(@view)

    expect(hash[:pub_type]).to eq("Birreria")
  end

  it "should get phone" do
    @pub.phone = "333-12345678"

    hash = @geolocated.to_hash(@view)

    expect(hash[:phone]).to eq("333-12345678")
  end

  it "should get phone" do
    @pub.closing_days = "0,1"

    hash = @geolocated.to_hash(@view)

    expect(hash[:closing_days]).to eq("0,1")
  end

  it "should get image size" do
    @pub.comments << FactoryGirl.build(:cover, width: 100, height: 200)

    hash = @geolocated.to_hash(@view)

    expect(hash[:image_width]).to eq(100)
    expect(hash[:image_height]).to eq(200)
  end

  it "should get no photo image given a pub without an image" do
    asset = Rails.application.assets.find_asset("no-photo.jpg")
    image = Image::read(asset.pathname).first


    hash = @geolocated.to_hash(@view)

    expect(hash[:image_width]).to eq(image.columns)
    expect(hash[:image_height]).to eq(image.rows)
  end
end