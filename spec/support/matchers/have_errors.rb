RSpec::Matchers.define :have_errors do |tag_id, score|
  match do |page|
    expect(page).to have_selector('.alert.alert-danger')
  end
end