module ViewTestHelpers
  def render_view(*args)
    render *args
    @html = Nokogiri::HTML::fragment(rendered)
  end

  def define_signed_in_helper(return_value = false)
    controller.singleton_class.class_eval %Q(
      protected
        def signed_in?
          #{return_value}
        end
        helper_method :signed_in?
    )
  end
  
  def define_current_user_helper(user = nil)
    controller.singleton_class.class_eval %Q(
      protected
        def current_user
          #{user}
        end
        helper_method :current_user
    )
  end

  def define_pub_owner_helper(return_value)
    controller.singleton_class.class_eval %Q(
      protected
      def pub_owner?
        #{return_value}
      end
      helper_method :pub_owner?
    )
  end  
end