def sign_in(user = FactoryGirl.create(:user)) 
  visit new_user_session_path
  fill_in "Email",    with: user.email
  fill_in "Password", with: user.password
  click_button "Accedi"
end

def sign_up
  visit new_user_registration_path
  fill_in("Nome utente", with: "john smith")
  fill_in("Email", with: "john@example.com")
  fill_in("Password", with: 'secret')
  fill_in("Conferma password", with: 'secret')
  check("Accetto tutti i termini e le condizioni del regolamento e della disciplina sulla privacy")
  click_button "Iscriviti"
end

def last_email
  ActionMailer::Base.deliveries.last
end

def count_queries_for(&block)
  count = 0
  callback = lambda {|name, start, finish, message_id, values|
      return if values[:sql].include?("PRAGMA")
      return if values[:sql].include?('sqlite_sequence')
      return if values[:sql].include?('FROM pg_')
      count += 1
  }
  ActiveSupport::Notifications.subscribed(callback, "sql.active_record") do
    yield
  end
  count
end
