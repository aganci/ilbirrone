require_relative 'page'

class NewBeerReviewPage < Page
  def initialize(beer)
    visit new_beer_review_path(beer_id: beer.id)
  end

  def rating=(value)
    find("#beer_review_rating").set value
  end

  def content=(value)
    fill_in("beer_review_content", with: value)
  end

  def activate_aroma
    click_link "Aroma"
  end

  def aroma=(value)
    find("#beer_review_aroma").set value
  end

  def aroma
    find("#beer_review_aroma", visible: false).value
  end

  def first_aroma
    first('#aroma a')
  end

  def activate_appearance
    click_link "Aspetto"
  end

  def appearance=(value)
    find("#beer_review_appearance").set value
  end
  
  def appearance
    find("#beer_review_appearance", visible: false).value
  end

  def first_appearance
    first('#appearance a')
  end

  def activate_taste
    click_link "Gusto"
  end

  def taste
    find("#beer_review_taste", visible: false).value
  end

  def taste=(value)
    find("#beer_review_taste").set value
  end
  
  def first_taste
    first('#taste a')
  end

  def activate_palate
    click_link "Sensazioni in bocca"
  end

  def palate
    find("#beer_review_palate", visible: false).value
  end

  def palate=(value)
    find("#beer_review_palate").set value
  end
  
  def first_palate
    first('#palate a')
  end

  def click_save
    click_button "Salva"
  end

end