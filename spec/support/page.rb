class Page
  include Rails.application.routes.url_helpers
  include Capybara::DSL

  def heading
    find('h1').text
  end

  def title
    find('title').text
  end
end