require_relative 'page'

class NewReviewPage < Page
  
  def initialize(pub)
    @pub = pub
    visit new_review_path(pub_id: @pub.id)
  end

  def title
    find('h1').text
  end

  def full_address
    first('p').text
  end

  def select_date(date)
    fill_in('review_date', with: date)
  end
  
  def review=(value)
    fill_in('Recensione', with: value)
  end

  def rating=(value)
    find("input[@id='review_rating']").set value
  end 

  def food=(value)
    find("input[@id='review_food']").set value
  end
  
  def beers=(value)
    find("input[@id='review_beers']").set value
  end

  def value=(value)
    find("input[@id='review_value']").set value
  end

  def atmosphere=(value)
    find("input[@id='review_atmosphere']").set value
  end

  def check_frequenter
    choose('review_frequenter_true')
  end

  def save
    click_button 'Salva'
  end

  def has_errors?
    find('.alert.alert-error')
  end
end