module PageTestHelpers
  def breadcrumb_labels
    all('.breadcrumb li').map {|element| element.text.gsub('>', '').strip}
  end
  
  def breadcrumb_links
    all('.breadcrumb a').map {|element| element[:href]}
  end
end