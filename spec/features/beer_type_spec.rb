require 'rails_helper'

describe 'beer type page' do
  it "should set title and heading" do
    visit beers_path(tipologia: 'a beer type')

    expect(page).to have_title("Birre a beer type - Beerky")
    expect(find('h1').text).to eq('Birre a beer type')
  end

  it "should list beers" do
    first_a = FactoryGirl.create(:beer, name: 'first type a', beer_type: 'type a')
    FactoryGirl.create(:beer, name: 'type b', beer_type: 'type b')
    second_a = FactoryGirl.create(:beer, name: 'second type a', beer_type: 'type a')

    visit beers_path(tipologia: 'type a')

    expect(page).to have_link('first type a', href: beer_path(first_a))
    expect(page).to have_link('second type a', href: beer_path(second_a))
    expect(page).to_not have_link('type b')
  end
end