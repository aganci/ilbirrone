require 'rails_helper'

describe 'new pub event page' do
  before do
    sign_in(FactoryGirl.create(:user))
    pub = FactoryGirl.create(:pub, name: 'Birreria del corso', city: 'Milano')

    visit new_pub_event_path(pub_id: pub.id)
  end

  it "should format title" do
    expect(find('h1').text).to eq('Segnalazione evento Birreria del corso Milano')
    expect(page).to have_title('Segnalazione evento Birreria del corso Milano - Beerky')
  end

  it "should add event" do
    fill_in "Inizia il giorno", with: Date.tomorrow
    fill_in "Finisce il giorno", with: Date.tomorrow
    fill_in "Titolo", with: 'a title'
    fill_in "Descrizione", with: 'a description'
    fill_in "Sito Web", with: 'http://www.example.com'

    expect { click_button 'Salva' }.to change(PubEvent, :count).by(1)
  end

  it "should show errors" do
    click_button 'Salva'

    expect(page).to have_errors
  end

  def active_tab
    find('.nav-tabs .active').text
  end
end