require 'rails_helper'

describe "microbrewery page"  do
  before do
    sign_in FactoryGirl.create(:user)
    @pub = FactoryGirl.create(:pub, pub_type: "Microbirrificio")
    @beer = FactoryGirl.create(:beer, pub: @pub)
    @review = FactoryGirl.create(:beer_review, beer: @beer, rating: 3)
    visit pub_path(@pub)
  end

  subject {page}

  it "should show beer links" do
    should have_link(@beer.name, href: beer_path(@beer))
  end

  it "should show beer ratings" do
    expect(find('.small-rating')[:alt]).to eq('3 su 5 stelle')
  end
end