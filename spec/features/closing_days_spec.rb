require 'rails_helper'

describe "closing days", js: true do
  before do
    @user = FactoryGirl.create(:admin)
    visit new_user_session_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'Accedi'
  end

  it "should set closing days" do
    visit new_pub_path

    click_button "Lun"
    click_button "Mar"

    expect(page).to have_selector("input#pub_closing_days[value=\"1,2\"]", visible: false)
  end

  it "should initialize closing days" do
    pub = FactoryGirl.create(:pub, closing_days: "0,1")
    visit edit_admin_pub_path(pub)

    active_days = []
    all('.active').each do |element|
      active_days << element.text
    end

    expect(active_days).to eq(["Dom","Lun"])
  end
end
