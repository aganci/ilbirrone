require 'rails_helper'

describe 'sign in redirection' do
  it "should redirect to current page after sign in" do
    visit a_path

    sign_in(FactoryGirl.create(:user))

    expect(page.current_path).to eq(a_path)
  end

  it "should redirect to current page after sign in of a page with ajax request", js: true do
    visit_page_with_ajax_request

    sign_in(FactoryGirl.create(:user))

    expect(current_path_with_query).to eq(nearby_pubs_path)
  end


  def visit_page_with_ajax_request
    visit nearby_pubs_path
    page.execute_script "latitude = '100'; longitude = '100';"
    page.execute_script "state = 'ok';"
    sleep 0.1
  end

  def current_path_with_query
    uri = URI::parse(current_url)
    result = uri.path
    result << "?#{uri.query}" if uri.query.present?
    result 
  end

  def a_path
    new_contact_us_path
  end
end