require 'rails_helper'

describe 'open graph meta' do
  it "should set image_url for pub page" do
    pub = FactoryGirl.create(:pub_with_cover)
    
    visit pub_path(pub)

    expect(find('meta[property="og:image"]', visible: false)[:content]).to eq("http://ilbirrone.it/a-path.jpg")
  end

  it "should set image_url for beer page" do
    beer = FactoryGirl.create(:beer)
    FactoryGirl.create(:cover, 
      commentable: beer, 
      path: 'beers/an-image-path.jpg', 
      user: FactoryGirl.create(:user))
    
    visit beer_path(beer)

    expect(find('meta[property="og:image"]', visible: false)[:content]).to eq("http://ilbirrone.it/beers/an-image-path.jpg")
  end
end

