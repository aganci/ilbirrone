require 'rails_helper'

describe "statistics page" do
  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should count users" do
    FactoryGirl.create(:user)
    FactoryGirl.create(:user)

    visit admin_statistics_path

    expect(page).to have_selector('td', text: '3')
    expect(page).to have_selector('td', text: 'Birronauti')
  end

  it "should count reviews" do
    FactoryGirl.create(:review)
    
    visit admin_statistics_path

    expect(page).to have_selector('td', text: '1')
    expect(page).to have_selector('td', text: 'Recensioni')
  end

  it "should count beers" do
    FactoryGirl.create(:beer)
    FactoryGirl.create(:beer)
    FactoryGirl.create(:beer)

    visit admin_statistics_path

    expect(page).to have_selector('td', text: '3')
    expect(page).to have_selector('td', text: 'Birre')
  end

  it "should count not approved pubs" do
    FactoryGirl.create(:pub, status: "submitted")
    FactoryGirl.create(:pub, status: "approved")

    visit admin_statistics_path

    expect(page).to have_selector('td', text: '2')
    expect(page).to have_selector('td', text: 'Locali')
  end

  it "should count beer reviews" do
    FactoryGirl.create(:beer_review)
    
    visit admin_statistics_path

    expect(page).to have_selector('td', text: '1')
    expect(page).to have_selector('td', text: 'Recensioni birre')
  end

  it "should count events" do
    FactoryGirl.create(:pub_event)
    FactoryGirl.create(:pub_event)
    
    visit admin_statistics_path

    expect(page).to have_selector('td', text: '2')
    expect(page).to have_selector('td', text: 'Eventi')
  end

  it "should count comments" do
    FactoryGirl.create(:comment)
    
    visit admin_statistics_path

    expect(page).to have_selector('td', text: '1')
    expect(page).to have_selector('td', text: 'Commenti')
  end

  it "should show admin comments link" do
    visit admin_statistics_path
    
    expect(page).to have_link("Commenti", href: admin_comments_path)
  end

  it "should show admin pre registrations link" do
    visit admin_statistics_path

    expect(page).to have_link("Preiscritti", href: admin_pre_registrations_path)
  end

  it "should show pre registration count" do
    FactoryGirl.create(:pre_registration)

    visit admin_statistics_path

    expect(page).to have_selector('td', text: '1')
  end

  it "should show visitor count" do
    allow(Visitor).to receive(:count).and_return(20)

    visit admin_statistics_path

    expect(page).to have_selector('td', text: '20')
  end

end