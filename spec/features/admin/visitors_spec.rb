require 'rails_helper'

describe 'visitors admin page' do
  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should render visitors" do
    pub = FactoryGirl.create(:pub, name: 'a pub')
    user = FactoryGirl.create(:user, username: 'a username')
    FactoryGirl.create(:visitor, pub: pub, user: user, confirmed: false)

    visit admin_visitors_path

    expect(page).to have_link('a pub', href: pub_path(pub))
    expect(page).to have_link('a username', href: user_path(user))
  end
end