# encoding: UTF-8"
require 'rails_helper'

describe "new pub page" do
  before do
    sign_in(FactoryGirl.create(:admin))
    
    visit new_admin_pub_path
  end

  it "should save the pub into database" do
    fill_in 'Nome', with: 'pub name'
    fill_in 'Indirizzo', with: 'indirizzo del pub'
    fill_in 'Città', with: 'città del pub'
    fill_in 'Provincia', with: 'provincia del pub'
    fill_in 'Cap', with: '12345'
    fill_in 'Telefono', with: '0123-1234567890'
    fill_in 'E-mail', with: 'user@example.com'
    fill_in 'Sito Web', with: 'www.example.org'

    expect { click_button 'Salva' }.to change(Pub, :count).by(1)
  end

  it "should show errors" do
    click_button 'Salva'

    expect(page).to have_errors
  end
end
