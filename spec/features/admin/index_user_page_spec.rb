require 'rails_helper'

describe "Users page" do
  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should list users" do
    FactoryGirl.create(:user, username: 'a username', current_sign_in_at: 1.hour.ago)

    visit admin_users_path

    expect(page).to have_selector('td', text: 'a username')
  end
end