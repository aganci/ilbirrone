require 'rails_helper'

describe "admin beers page" do
  before do
    sign_in(FactoryGirl.create(:admin))
    @beer = FactoryGirl.create(:beer)

    visit admin_beers_path
  end

  subject { page }

  it "should show beers table" do
    should have_selector('table tr td', text: @beer.name)
  end

  it "should count inserted beers" do
    should have_selector('h1', text: '1 Birre inserite')
  end

  it "should delete beer" do
    expect { click_link 'Elimina' }.to change(Beer, :count).by(-1)
  end

  it "should modify beer" do
    click_link 'Modifica'

    fill_in 'Nome della birra', with: 'Modified name'
    click_button 'Salva'

    should have_selector('table tr td', text: 'Modified name')
  end
end