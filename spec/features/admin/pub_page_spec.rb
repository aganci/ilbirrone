require 'rails_helper'

describe 'spec_helper' do
  before do
    sign_in(FactoryGirl.create(:admin))
  end


  it "should render admin pub page" do
    pub = FactoryGirl.create(:pub, name: 'a pub name')

    visit admin_pub_path(pub)

    expect(find('h1').text).to eq("Pannello di controllo a pub name")
  end

  it "should render beers" do
    pub = FactoryGirl.create(:pub, name: 'a pub name')
    FactoryGirl.create(:beer, name: 'a beer name', pub: pub)

    visit admin_pub_path(pub)
    
    expect(page).to have_content('a beer name')
  end
end