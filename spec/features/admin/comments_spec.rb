require 'rails_helper'

describe 'admin comments page' do
  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should render comments" do
    FactoryGirl.create(:comment, commentable: FactoryGirl.create(:pub, name: 'a pub name'))

    visit admin_comments_path

    expect(page).to have_content('a pub name')
  end

  it "should delete comment" do
    FactoryGirl.create(:comment)

    visit admin_comments_path

    expect { click_link 'Elimina' }.to change(Comment, :count).by(-1)
  end

end