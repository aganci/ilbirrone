require 'rails_helper'

describe "pub list page" do
  before do
    sign_in(FactoryGirl.create(:admin))
    @pub = FactoryGirl.create(:pub)

    visit admin_pubs_path
  end

  subject { page }

  it "should show pub list" do
    should have_selector('table tr td', text: @pub.name)
    should have_selector('table tr td', text: @pub.address)
    should have_selector('table tr td', text: @pub.email)
  end

  it "should show pub count" do
    should have_selector('h1', text: '1 Birrerie inserite')
  end
  
  it "should show pub to approve count" do
    should have_selector('h1', text: '0 da approvare')
    
    FactoryGirl.create(:pub, status: 'submitted')
    visit admin_pubs_path
    
    should have_selector('h1', text: '1 da approvare')
  end

  it "should show new pub link" do
    should have_link('Nuovo', href: new_admin_pub_path)
  end

  it "should delete pub" do
    expect { click_link 'Elimina' }.to change(Pub, :count).by(-1)
  end

  it "should show edit pub link" do
    should have_link('Modifica', href: edit_admin_pub_path(@pub))
  end
end