require 'rails_helper'

describe 'pre registrations' do
  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should list pre registrations" do
    FactoryGirl.create(:pre_registration, user: FactoryGirl.create(:user, username: 'an homebrewer'))

    visit admin_pre_registrations_path

    expect(page).to have_content('an homebrewer')
  end

  it "should delete pre registration" do
    FactoryGirl.create(:pre_registration)

    visit admin_pre_registrations_path

    expect { click_link 'Elimina' }.to change(PreRegistration, :count).by(-1)
  end  
end