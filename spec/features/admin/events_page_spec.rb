require 'rails_helper'

describe "event list page" do
  before do
    sign_in(FactoryGirl.create(:admin))
    @event = FactoryGirl.create(:pub_event, title: 'Titolo evento')

    visit admin_events_path
  end

  it "should show event" do
    expect(page).to have_content('Titolo evento')
  end

  it "should delete event" do
    click_link 'Elimina'

    expect(Event.all.count).to eq(0)
  end

  it "should modify the event" do
    click_link 'Modifica'

    fill_in 'Titolo', with: 'a new title'

    click_button 'Salva'

    @event.reload
    expect(@event.title).to eq('a new title')
  end
end