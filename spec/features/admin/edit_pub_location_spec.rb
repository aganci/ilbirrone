require 'rails_helper'

describe "update pub location page" do

  let(:pub) { FactoryGirl.create(:pub) }

  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should update the pub location" do
    visit edit_location_admin_pub_path(pub)

    fill_in "Latitudine", with: '45.678113'
    fill_in "Longitudine", with: '9.235216'
    click_button "Salva"
    pub.reload

    expect(pub.latitude).to eq(45.678113)
    expect(pub.longitude).to eq(9.235216)
  end

  it "should show edit location on admin pub page" do
    visit admin_pub_path(pub)

    expect(page).to have_link("Modifica posizione", href: edit_location_admin_pub_path(pub))
  end
end