require 'rails_helper'

describe 'edit public event' do
  before do
    sign_in(FactoryGirl.create(:admin))
    @event = FactoryGirl.create(:public_event)

    visit admin_events_path
  end
  
  it "should update public event" do
    click_link 'Modifica'

    fill_in 'Nome del luogo', with: 'una nuova location'
    click_button 'Salva'
    @event.reload

    expect(@event.location).to eq('una nuova location')
  end
end