require 'rails_helper'

describe 'admin reviews page' do
  before do
    sign_in(FactoryGirl.create(:admin))
  end

  it "should render title and heading" do
    visit admin_reviews_path

    expect(page).to have_title("Amministrazione Recensioni - Beerky")
    expect(find('h1').text).to eq("Amministrazione Recensioni - nessuna recensione")
  end

  it "should render reviews" do
    review = FactoryGirl.create(:review, date: Date.new(2013, 4, 28))
    
    visit admin_reviews_path

    expect(page).to have_content('domenica 28 aprile 2013')
    expect(page).to have_content(review.description)
  end

  it "should delete review" do
    review = FactoryGirl.create(:review)
    
    visit admin_reviews_path
    click_link 'Elimina'

    expect(Review.all.count).to eq(0)
  end

  it "should modify review" do
    review = FactoryGirl.create(:review)
    
    visit admin_reviews_path

    click_link 'Modifica'

    new_description = Forgery(:lorem_ipsum).characters(200)
    fill_in 'review_description', with: new_description

    click_button 'Salva'

    expect(Review.all.first.description).to eq(new_description)
  end

  it "should set frequenter" do
    review = FactoryGirl.create(:review, frequenter: true)

    visit edit_admin_review_path(review)
    click_button 'Salva'
    
    expect(Review.all.first.frequenter).to eq(true)
  end
end