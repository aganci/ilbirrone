# encoding: UTF-8"
require 'rails_helper'

describe "edit pub page" do
  before do
    sign_in(FactoryGirl.create(:admin))
    @pub = FactoryGirl.create(:pub, pub_type: 'Brewpub')
    
    visit edit_admin_pub_path(@pub)
  end

  it "should set pub type" do
    expect(find('#pub_pub_type_brewpub')).to be_checked
  end

  it "should update the pub" do
    fill_in 'Nome', with: 'new name'

    click_button 'Salva'

    @pub.reload
    expect(@pub.name).to eq('new name')
  end

  it "should show errors" do
    fill_in 'Nome', with: ''

    click_button 'Salva'

    expect(page).to have_errors
  end
end
