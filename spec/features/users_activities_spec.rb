require 'rails_helper'

describe 'users activities page' do
  it "should render pub approval" do
    pub = FactoryGirl.create(:pub, name: 'a pub name', user: FactoryGirl.create(:user, username: 'a username'))
    Notification.notify_pub_approval(pub)

    visit notifications_path

    expect(find('.notification-message').text).to eq('a username ha segnalato a pub name a city (a province)')
  end

  it "should render pub review" do
    pub = FactoryGirl.create(:pub, name: 'a pub name')
    review = FactoryGirl.create(:review, pub: pub, user: FactoryGirl.create(:user, username: 'a reviewer'))
    Notification.notify_pub_review(review)

    visit notifications_path
    
    expect(find('.notification-message').text).to include('a reviewer ha recensito a pub name')
  end

  it "should paginate" do
    pub = FactoryGirl.create(:pub, name: 'a pub name', user: FactoryGirl.create(:user, username: 'a username'))
    notification = Notification.notify_pub_approval(pub)
    allow(Notification).to receive(:paginated_timeline).and_return(paginated([notification] * 21))

    visit notifications_path

    expect(page).to have_selector('.pagination')
  end

  def paginated models
    WillPaginate::Collection.create(1, 20, models.length) do |pager|
      pager.replace models
    end    
  end
end