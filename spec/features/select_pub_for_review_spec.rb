require 'rails_helper'

describe do
  before do
    sign_in FactoryGirl.create(:user)
  end
    
  it "should show add pub button" do
    visit select_for_review_pubs_path

    first(:link, 'Segnala un locale').click

    expect(page).to have_button('Salva')
  end

  it "should autocomplete pub name", js: true do
    FactoryGirl.create(:pub, name: 'Baladin')
    visit select_for_review_pubs_path
    
    fill_in "search_pub", with: 'Baladin'    
    
    first_autocomplete_item.click
    
    expect(page).to have_button('Salva')
    expect(find('h1').text).to eq("Baladin")
  end

  def first_autocomplete_item
    page.find(:css, ".typeahead.dropdown-menu li")
  end
end