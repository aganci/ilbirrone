require 'rails_helper'

describe 'pubs map page' do
  before do
    FactoryGirl.create(:pub, province: 'Milano', latitude: 1.23, longitude: -1.23)
    pub = FactoryGirl.create(:pub, province: 'Milano', latitude: 2.34, longitude: -2.34)
    FactoryGirl.create(:review, pub: pub)
    
    visit pubs_path(provincia: 'Milano', show: 'map')
  end

  it "should create markers with pub positions" do
    expect(page.html).to include('position: new google.maps.LatLng(1.23, -1.23)')
    expect(page.html).to include('position: new google.maps.LatLng(2.34, -2.34)')
  end

  it "should display marker for reviews" do
    expect(page.html).to include('marker-con-recensioni.png')
  end
  
end