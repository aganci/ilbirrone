require 'rails_helper'

describe "who we are" do
  it "should render the page" do
    visit who_we_are_informations_path

    expect(page).to have_link("Iscriviti subito!", href: new_user_registration_path)
  end

  it "should show link in home page" do
    visit root_path

    expect(page).to have_link("Chi siamo", href: who_we_are_informations_path)
  end
end