require 'rails_helper'

describe 'beer photo', js: true do
  it "should zoom photo when clicked" do
    beer = FactoryGirl.create(:beer_with_cover)

    visit beer_path(beer)
    page.find('.beer-photo img').click

    expect(page).to have_selector('#photoDialog', visible: true)
  end
end