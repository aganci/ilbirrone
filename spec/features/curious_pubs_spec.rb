# encoding: utf-8
require 'rails_helper'

describe 'curious pubs page' do
  before do
    pub = FactoryGirl.create(:pub, name: 'a curious pub', description: 'any description')
    allow(PubQuery).to receive(:curious).and_return([pub])

    visit curious_pubs_path
  end
  
  it "should list curious pubs" do
    expect(all('.thumbnail h4').map {|element| element.text}).to eq(['a curious pub'])
  end

  it "should show links in subnavbar" do
    expect(all('.sub-navbar li').map {|element| element.text}).to eq(['Nuove segnalazioni', "|", 'Nuove recensioni', "|", 'I più curiosi'])
    expect(all('.sub-navbar a').map {|element| element[:href]}).to eq([pubs_path, recent_reviews_pubs_path])
  end
end