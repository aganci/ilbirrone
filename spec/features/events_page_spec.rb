# encoding: utf-8
require 'rails_helper'

describe 'events page' do
  it "should render title and heading" do
    visit events_path

    expect(page).to have_title("Scopri dov'è il divertimento! I prossimi eventi e feste della birra da tutta Italia - Beerky")
    expect(find('h1').text).to eq('Eventi')
  end

  it "should show add new event button" do
    visit events_path

    expect(page).to have_link('Segnala evento', href: select_type_events_path)
  end

  it "should show events from today ordered by date" do
    create_event title: 'in the past', start_date: 1.day.ago, end_date: 1.day.ago
    create_event title: 'tomorrow', start_date: 1.day.ago, end_date: (Date.today + 1)
    create_event title: 'today', start_date: 1.day.ago, end_date: Date.today
    expect(Event.all.count).to eq(3)

    visit events_path

    expect(all('.thumbnail h4').map { |element| element.text.strip }).to eq(["today", "tomorrow"])
  end

  it "should show link in navbar" do
    visit events_path

    expect(all('.navbar li a').map {|link| link.text}).to include('Eventi')    
    expect(all('.navbar li a').map {|link| link['href']}).to include(events_path)    
  end

  def create_event(options)
    FactoryGirl.build(:pub_event, options).save!(validate: false)
  end
end