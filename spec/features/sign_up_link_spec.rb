require 'rails_helper'

describe 'sign up link visibility', js: true do
  
  it "should hide sign up link when window is too narrow" do
    visit root_path

    page.current_window.resize_to(1299, 500)

    expect(page).to have_selector('.sign-up', visible: false)
    
  end

  it "should show sign up link" do
    visit root_path
    
    page.current_window.resize_to(1400, 500)
    
    expect(page).to have_selector('.sign-up', visible: true)
  end

end

describe 'sign up link' do
  it "should show link registration page" do
    visit root_path

    expect(find('.sign-up a').text).to eq("Contattaci")
    expect(find('.sign-up a')['href']).to eq(new_contact_us_path)
  end
end