require 'rails_helper'

describe "microbreweries page" do
  it "should set title" do
    visit microbreweries_path

    expect(page).to have_title("Microbirrifici d'Italia - Beerky")
  end

  it "should show most recent microbreweries of all regions" do
    FactoryGirl.create(:pub, name: 'newer', pub_type: "Microbirrificio", created_at: 1.hour.ago, region: "a")
    FactoryGirl.create(:pub, name: 'oldest', pub_type: "Microbirrificio", created_at: 2.hour.ago, region: "b")

    visit microbreweries_path

    expect(microbreweries).to eq(["newer", "a", "oldest", "b"])
  end

  it "should render first microbrewery of each region" do
    lombardia_a = FactoryGirl.create(:pub, pub_type: "Microbirrificio", name: 'Lombardia a', region: 'Lombardia', created_at: 1.hour.ago)
    lombardia_b = FactoryGirl.create(:pub, pub_type: "Microbirrificio", region: 'Lombardia b', region: 'Lombardia', created_at: 2.hour.ago)
    piemonte = FactoryGirl.create(:pub, pub_type: "Microbirrificio", name: 'Piemonte', region: 'Piemonte')

    visit microbreweries_path
    
    expect(microbreweries).to eq(["Piemonte", "Piemonte", "Lombardia a", "Lombardia"])
  end

  
  it "should show microbreweries of a region" do
    FactoryGirl.create(:pub, name: 'region a', pub_type: "Microbirrificio", created_at: 1.hour.ago, region: "a")
    FactoryGirl.create(:pub, name: 'region b', pub_type: "Microbirrificio", created_at: 2.hour.ago, region: "b")

    visit microbreweries_path(region: 'a')

    expect(microbreweries_of_region).to eq(["region a"])
  end

  it "should have all cloud link working" do
    visit microbreweries_path

    get_cloud_links.each do |link|
      visit link.href
      
      expect(page).to have_selector('h2', text: "#{link.text}")
    end
  end

  it "should invite to add a new microbreweries" do
    visit microbreweries_path(region: 'a region')

    expect(page).to have_link("Segnala un microbirrificio", href: new_pub_path(pub_type: "Microbirrificio")) 
  end

  it "should show add microbrewery in all region" do
    visit microbreweries_path

    expect(page).to have_link("Segnala un microbirrificio", href: new_pub_path(pub_type: "Microbirrificio")) 
  end

  it "should show link to page" do
    visit root_path

    expect(page).to have_link("Microbirrifici", href: microbreweries_path)
  end


  def microbreweries_of_region
    all('table tbody td a').map {|link| link.text }
  end
  
  def microbreweries
    all('.thumbnail a').map {|link| link.text }.reject {|text| text.blank?}
  end

  def get_cloud_links
    links = []
    page.all('.regions a').each do |link|
      links << OpenStruct.new(href: link[:href], text: link.text)
    end
    links
  end

end