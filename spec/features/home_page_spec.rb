# encoding: utf-8
require 'rails_helper'

describe "home page" do
  before do
    visit root_path
  end

  it "should have title" do
    expect(page).to have_title('Recensioni su birre birrerie pub microbirrifici brewpub ed eventi dal mondo della birra - Beerky')
  end

  it "should show sign up link" do
    expect(page).to have_link('Iscriviti!', href: new_user_registration_path)
    
    sign_in(FactoryGirl.create(:user))
    visit root_path

    expect(page).to_not have_link('Registrati')
  end

  it "should show contact uslink" do
    expect(page).to have_link('Contattaci', href: new_contact_us_path)    
  end

  it "should show pubs nearby link in navbar" do
    expect(all('.navbar li a').map {|link| link.text}).to include('Intorno a Te')    
    expect(all('.navbar li a').map {|link| link['href']}).to include(nearby_pubs_path)        
  end
end

