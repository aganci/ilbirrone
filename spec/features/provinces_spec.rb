require 'rails_helper'

describe 'pronvices' do
  before do
    FactoryGirl.create(:pub, province: 'Milano', region: 'Lombardia')    
    FactoryGirl.create(:pub, province: 'Torino', region: 'Piemonte')    
    FactoryGirl.create(:pub, province: 'Torino', region: 'Piemonte')    
  end

  it "should show regions" do
    visit provinces_pubs_path

    expect(all('.thumbnail h3').map {|element| element.text}).to eq(['Lombardia', 'Piemonte'])
  end
  
  it "should show provinces with pubs count" do
    visit provinces_pubs_path

    expect(page).to have_link(Nokogiri::HTML('Milano&nbsp;(1)').text, href: pubs_path(provincia: 'Milano'))
    expect(page).to have_link(Nokogiri::HTML('Torino&nbsp;(2)').text, href: pubs_path(provincia: 'Torino'))
  end
end