require 'rails_helper'

describe 'beer page similar beers' do
  it "should show similar beers" do
    beer = FactoryGirl.create(:beer, name: 'first', beer_type: 'a beer type')
    pub = FactoryGirl.create(:pub, name: 'a pub')
    similar = FactoryGirl.create(:beer, name: 'similar to first', beer_type: 'a beer type', pub: pub)
    FactoryGirl.create(:beer, name: 'another type', beer_type: 'another type')

    visit beer_path(beer)

    expect(similar_beers_links_text).to eq(['similar to first', 'a pub'])
    expect(similar_beers_links).to eq([beer_path(similar), beer_path(similar), pub_path(pub)])
  end

  def similar_beers_links_text
    all('.similar_beers a').map { |element| element.text }.reject {|element| element.blank? }
  end

  def similar_beers_links
    all('.similar_beers a').map { |element| element[:href] }
  end

end