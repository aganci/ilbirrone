require 'rails_helper'

describe 'pub photo', js: true do
  it "should zoom photo when clicked" do
    pub = FactoryGirl.create(:pub_with_cover)

    visit pub_path(pub)

    page.find('.pub-photo img').click

    expect(page).to have_selector('#photoDialog', visible: true)
  end
end