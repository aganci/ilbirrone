require 'rails_helper'

describe 'homebrewer pre registration page' do
  it "should render title" do
    visit pre_registration_homebrewers_path

    expect(page).to have_title('Preiscriviti al primo social network italiano degli homebrewer - Beerky')
    expect(find('h1').text).to eq("Homebrewer")
  end

  it "should render sign in button given a not signed in user" do
    visit pre_registration_homebrewers_path
    
    expect(find(".pre-register")).to have_link('Accedi', href: new_user_session_path)
    expect(find(".invite-friend")).to have_link('Accedi', href: new_user_session_path)
  end

  it "should pre-register user given a signed in user" do
    user = FactoryGirl.create(:user)
    sign_in user
    visit pre_registration_homebrewers_path

    click_button "Preiscriviti"

    expect(PreRegistration.first.user).to eq(user)
  end

  it "should not pre-register a user two times" do
    user = FactoryGirl.create(:user)
    sign_in user
    visit pre_registration_homebrewers_path

    click_button "Preiscriviti"
    click_button "Preiscriviti"

    expect(PreRegistration.count).to eq(1)
  end

  it "should send invitaion email" do
    user = FactoryGirl.create(:user, username: 'a user')
    sign_in user
    visit pre_registration_homebrewers_path

    fill_in "Email", with: "friend@example.org"

    click_button "Invita"

    set_url_host
    expect(last_email.body).to include(pre_registration_homebrewers_url)
    expect(last_email.body).to include('a user')
    expect(last_email.to).to eq(["friend@example.org"])
  end

  it "should validate email form" do
    user = FactoryGirl.create(:user, username: 'a user')
    sign_in user
    visit pre_registration_homebrewers_path

    click_button "Invita"

    expect(page).to have_errors
  end

  def set_url_host
    default_url_options[:host] = Rails.application.config.action_mailer.default_url_options[:host]
  end
end