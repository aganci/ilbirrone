require 'rails_helper'

describe "beers in pub page" do
  before do
    sign_in FactoryGirl.create(:user)
    @pub = FactoryGirl.create(:pub, pub_type: 'Microbirrificio')
  end

  it "should show beers" do
    FactoryGirl.create(:beer, name: 'first', pub: @pub)

    visit pub_path(@pub)

    expect(page).to have_selector('td', text: 'first')
    expect(page).to have_selector('th', text: 'Nome della birra')
    expect(page).to have_selector('#beers')
  end
  
  it "should show add beer button" do
    visit pub_path(@pub)

    expect(page).to have_link('Segnala una birra', href: new_beer_path(pub_id: @pub.id))
  end  

  it "should not show table when no beers" do
    visit pub_path(@pub)

    expect(page).to_not have_selector('th', text: 'Nome della birra')
    expect(page).to have_selector('p', text: 'Al momento non sono ancora state segnalate birre.')
  end

  it "should not show beers tab when not produce beers" do
    @pub = FactoryGirl.create(:pub, pub_type: 'Birreria')

    visit pub_path(@pub)

    expect(page).to_not have_selector('#beers')
  end

  it "should not show reviews when is a microbrewery" do
    visit pub_path(@pub)

    expect(page).to_not have_selector('#reviews')
  end
end