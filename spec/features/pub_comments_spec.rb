require 'rails_helper'

describe 'pub comments' do
  before do
    @pub = FactoryGirl.create(:pub)
  end

  it "should show comments tab ordered by most recent" do
    FactoryGirl.create(:comment, content: 'second comment', created_at: 2.day.ago, commentable: @pub)
    FactoryGirl.create(:comment, content: 'first comment', created_at: 1.day.ago, commentable: @pub)

    visit pub_path(@pub)

    expect(comments_content).to eq(['first comment', 'second comment'])
  end

  it "should insert comment" do
    sign_in FactoryGirl.create(:user)

    visit pub_path(@pub)
    
    fill_in 'comment_content', with: 'a comment content'

    click_button 'Pubblica'    

    expect(comments_content).to eq(['a comment content'])
  end

  it "should delete comment" do
    user = FactoryGirl.create(:user)
    FactoryGirl.create(:comment, commentable: @pub, user: user)
    sign_in user

    visit pub_path(@pub)

    expect { click_link 'Elimina' }.to change(Comment, :count).by(-1)
  end

  it "should render errors" do
    sign_in FactoryGirl.create(:user)

    visit pub_path(@pub)

    click_button 'Pubblica'

    expect(page).to have_errors
  end

  it "should render errors given a too short comment" do
    sign_in FactoryGirl.create(:user)
    visit pub_path(@pub)

    fill_in 'comment_content', with: '123456789'
    click_button 'Pubblica'

    expect(page).to have_errors
  end

  def comments_content
    all('.comment-content').map {|element| element.text}
  end
end