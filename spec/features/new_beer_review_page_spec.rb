require 'rails_helper'

describe "new beer review page" do
  before do
    sign_in FactoryGirl.create(:user)
    @beer = FactoryGirl.create(:beer)
    @page = NewBeerReviewPage.new(@beer)
  end

  it "should show beer name and producer name" do
    expect(@page.heading).to include(@beer.name)
  end

  it "should create a new review" do
    @page.rating = 4
    @page.content = "a review content"
    @page.aroma = "caramello"
    @page.appearance = "colore pallido"
    @page.taste = "lievemente dolce"
    @page.palate = "oleosa"

    expect { @page.click_save }.to change(BeerReview, :count).by(1)
  end

  it "should render errors" do
    @page.click_save

    expect(page).to have_errors
  end
end