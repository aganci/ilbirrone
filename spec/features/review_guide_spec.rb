require 'rails_helper'

describe "review guide page" do
  it "should show guide" do
    visit review_guide_path

    expect(page).to have_content('recensione efficace')
  end
end