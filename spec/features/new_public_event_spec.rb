require 'rails_helper'

describe 'new public event page' do
  before do
    sign_in FactoryGirl.create(:user)
  end

  it "should create a public event" do
    visit new_public_event_path
    
    fill_in "Inizia il giorno", with: Date.tomorrow
    fill_in "Finisce il giorno", with: Date.tomorrow
    fill_in "Titolo", with: 'a title'
    fill_in "Descrizione", with: 'a description'
    fill_in "Nome del luogo", with: 'nome del luogo'
    fill_in "Indirizzo", with: 'indirizzo evento'
    fill_in "Città", with: 'città evento'
    fill_in "Provincia", with: 'Provincia evento'
    fill_in "Regione", with: 'regione evento'
    fill_in "Cap", with: 'Cap evento'

    expect { click_button 'Salva' }.to change(PublicEvent, :count).by(1)
  end
end