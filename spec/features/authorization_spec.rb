require 'rails_helper'

describe "authorization" do
  before do 
    @user = FactoryGirl.create(:user)
  end
  
  it "should show message for sign in if not authorized" do
      visit new_pub_path

      expect(page).to have_selector('div.alert', text: 'Devi prima accedere o registrati per proseguire')      
  end
end
