require 'rails_helper'

describe 'breadcrumb' do

  it "should show breadcrumb in brewpub beer page" do
    pub = FactoryGirl.create(:pub, pub_type: 'Brewpub')
    beer = FactoryGirl.create(:beer, pub: pub)
    
    visit beer_path(beer)

    expect(breadcrumb_labels).to eq(['Home', 'Brewpub', pub.name, beer.name])
    expect(breadcrumb_links).to eq([root_path, brewpub_path, pub_path(beer.pub)])
  end
  
  it "should show breadcrumb in microbrewery beer page" do
    beer = FactoryGirl.create(:beer)
    
    visit beer_path(beer)

    expect(breadcrumb_labels).to eq(['Home', 'Microbirrifici', beer.pub.region, beer.pub.name, beer.name])
    expect(breadcrumb_links).to eq([root_path, microbreweries_path, microbreweries_path(region: beer.pub.region), pub_path(beer.pub)])
  end

  it "should show breadcrumb in microbrewery page" do
    microbrewery = FactoryGirl.create(:pub, pub_type: 'Microbirrificio')
    
    visit pub_path(microbrewery)

    expect(breadcrumb_labels).to eq(['Home', 'Microbirrifici', microbrewery.region, microbrewery.name])
    expect(breadcrumb_links).to eq([root_path, microbreweries_path, microbreweries_path(region: microbrewery.region)])
  end

  it "should show breadcrumb in region page" do
    visit microbreweries_path(region: 'a region')

    expect(breadcrumb_labels).to eq(['Home', 'Microbirrifici', 'a region'])
    expect(breadcrumb_links).to eq([root_path, microbreweries_path])
  end
  
  it "should show breadcrumb in microbreweries page" do
    visit microbreweries_path

    expect(breadcrumb_labels).to eq(['Home', 'Microbirrifici'])
    expect(breadcrumb_links).to eq([root_path])
  end

  it "should show breadcrumb in pub page" do
    pub = FactoryGirl.create(:pub, pub_type: 'Birreria', province: 'Milano')

    visit pub_path(pub)

    expect(breadcrumb_labels).to eq(['Home', 'Birrerie e Pub', 'Milano', pub.name])
    expect(breadcrumb_links).to eq([root_path, pubs_path, pubs_path(provincia: 'Milano')])
  end
  
  it "should show breadcrumb in brewpub page" do
    pub = FactoryGirl.create(:pub, pub_type: 'Brewpub', province: 'Milano')

    visit pub_path(pub)

    expect(breadcrumb_labels).to eq(['Home', 'Brewpub', pub.province, pub.name])
    expect(breadcrumb_links).to eq([root_path, brewpub_path, brewpub_path(provincia: pub.province)])
  end

  it "should show breadcrumb in brewpub page" do
    visit brewpub_path

    expect(breadcrumb_labels).to eq(['Home', 'Brewpub'])
    expect(breadcrumb_links).to eq([root_path])
  end

  it "should show breadcrumb in achievements page" do
    visit achievements_path

    expect(breadcrumb_labels).to eq(['Home', 'Sfide'])
  end

  it "should show breadcrumb in beers page" do
    visit beers_path

    expect(breadcrumb_labels).to eq(['Home', 'Birre'])
  end

  it "should show breadcrumb in pubs province page" do
    visit pubs_path(provincia: 'Milano')

    expect(breadcrumb_labels).to eq(['Home', 'Birrerie e Pub', 'Milano'])
    expect(breadcrumb_links).to eq([root_path, pubs_path])
  end

  it "should show breadcrumb in beer type page" do
    visit beers_path(tipologia: 'Pale Ale')
    
    expect(breadcrumb_labels).to eq(['Home', 'Birre', 'Pale Ale'])
    expect(breadcrumb_links).to eq([root_path, beers_path])
  end
end