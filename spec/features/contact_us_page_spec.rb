require 'rails_helper'

describe 'contact us' do
  before do
    visit new_contact_us_path
  end

  it "should send email" do
    fill_in('Nome e cognome', with: 'Mario Rossi')
    fill_in('Email', with: 'mariorossi@example.org')
    fill_in('Oggetto', with: 'a subject')
    fill_in('Messaggio', with: 'a message')

    click_button('Invia')

    expect(last_email.body).to include('a message')
    expect(last_email.body).to include('Mario Rossi')
    expect(last_email.body).to include('mariorossi@example.org')
    expect(last_email.to).to be_eql(['ilbirrone@gmail.com'])

    expect(page).to have_selector('div.alert', text: 'Messaggio inviato. Grazie di averci contattato')
  end

  it "should render errors" do
    click_button('Invia')

    expect(page).to have_errors
  end
end