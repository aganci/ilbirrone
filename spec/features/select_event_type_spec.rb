require 'rails_helper'

describe 'select event type page' do
  before do
    sign_in FactoryGirl.create(:user)
  end

  it "should show new public event button" do
    visit select_type_events_path

    click_link 'Segnala evento in luogo pubblico'

    expect(page).to have_button('Salva')
  end

  it "should autocomplete pub name", js: true do
    FactoryGirl.create(:pub, name: 'Baladin', city: 'Milano')
    visit select_type_events_path
    
    fill_in "search_pub", with: 'Baladin'    
    
    first_autocomplete_item.click
    
    expect(page).to have_button('Salva')
  end

  it "should show new pub link" do
    visit select_type_events_path
    
    expect(page).to have_link('Segnala un locale', href: new_pub_path)    
  end

  def first_autocomplete_item
    page.find(:css, ".typeahead.dropdown-menu li")
  end
end