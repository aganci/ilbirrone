require 'rails_helper'

describe 'pubs of province page' do
  it "should render title and heading" do
    visit pubs_path(provincia: 'Milano')

    expect(find('h1').text).to eq("Birrerie e Pub in Milano e provincia")
    expect(page).to have_title("Tutte le birrerie e pub in provincia Milano")
  end
  
  it "should render add pub link" do
    visit pubs_path(provincia: 'Milano')

    expect(page).to have_link('Segnala una Birreria', href: new_pub_path(pub_type: 'Birreria'))
  end

  it "should list pubs and brewpubs" do
    milano_city = FactoryGirl.create(:pub, name: 'pub in milano city', city: 'Milano')
    milano_province = FactoryGirl.create(:pub, name: 'pub in milano province', province: 'Milano')
    FactoryGirl.create(:pub, name: 'pub in rome province', province: 'Roma')
    FactoryGirl.create(:pub, name: 'brewpub in milano city', city: 'Milano', pub_type: 'Brewpub')
    FactoryGirl.create(:pub, name: 'microbirrificio in milano city', city: 'Milano', pub_type: 'Microbirrificio')

    visit pubs_path(provincia: 'Milano')

    expect(page).to have_link('pub in milano city', href: pub_path(milano_city))
    expect(page).to have_link('pub in milano province', href: pub_path(milano_province))
    expect(page).to have_link('brewpub in milano city')
    expect(page).to_not have_link('pub in rome province')
    expect(page).to_not have_link('microbirrificio in milano city')
  end
  
  it "should render title and heading" do
    visit brewpub_path(provincia: 'Milano')

    expect(find('h1').text).to eq("Brewpub in Milano e provincia")
    expect(page).to have_title("Tutti i brewpub in provincia Milano")
  end
  
  it "should render add brewpub link" do
    visit brewpub_path(provincia: 'Milano')

    expect(page).to have_link('Segnala un Brewpub', href: new_pub_path(pub_type: 'Brewpub'))
  end


  it "should list brewpub" do
    milano_city = FactoryGirl.create(:pub, name: 'pub in milano city', city: 'Milano', pub_type: 'Brewpub')
    milano_province = FactoryGirl.create(:pub, name: 'pub in milano province', province: 'Milano', pub_type: 'Brewpub')
    FactoryGirl.create(:pub, name: 'pub in rome province', province: 'Roma', pub_type: 'Brewpub')
    FactoryGirl.create(:pub, name: 'brewpub in milano city', city: 'Milano', pub_type: 'Birreria')

    visit brewpub_path(provincia: 'Milano')

    expect(page).to have_link('pub in milano city', href: pub_path(milano_city))
    expect(page).to have_link('pub in milano province', href: pub_path(milano_province))
    expect(page).to_not have_link('pub in rome province')
    expect(page).to_not have_link('brewpub in milano city')
  end

end