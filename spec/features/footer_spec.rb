require 'rails_helper'

describe 'footer links' do
    it "should have all footer links working" do
    visit root_path

    get_links.each do |link|
      visit link.href
      expect(page).to have_selector('h1'), "#{link.href} h1 not found"
      expect(find('h1').text).to include(link.text), link.href
    end
  end
  
  def get_links
    paths = []
    page.all('footer a').each do |link|
      next if link[:href].starts_with?('http')
      next if links_to_skip.include?(link[:href])

      paths << OpenStruct.new(href: link[:href], text: link.text)
    end
    paths
  end

  def links_to_skip
    [provinces_pubs_path, new_user_registration_path, new_pub_path, select_type_events_path]
  end

end