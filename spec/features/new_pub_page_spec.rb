# encoding: UTF-8
require 'rails_helper'

describe "new pub page" do
  before do
    sign_in(FactoryGirl.create(:user))
    
    visit new_pub_path
  end

  it "should save the pub into database" do
    fill_in 'Nome', with: 'pub name'
    fill_in 'Indirizzo', with: 'indirizzo del pub'
    fill_in 'Città', with: 'città del pub'
    fill_in 'Provincia', with: 'provincia del pub'
    fill_in 'Regione', with: 'una regione'
    fill_in 'Cap', with: '12345'
    fill_in 'Telefono', with: '0123-1234567890'
    fill_in 'E-mail', with: 'user@example.com'
    fill_in 'Sito Web', with: 'www.example.org'
    fill_in 'Descrizione', with: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    choose('pub_pub_type_microbirrificio')

    expect { click_button 'Salva' }.to change(Pub, :count).by(1)
  end
  
  it "should select italy by default" do
    expect(nations).to_not include("")
    expect(nations.first).to eq("Italia")
  end

  def nations
    find_field('Nazione').all('option').map { |option| option.text }
  end
end