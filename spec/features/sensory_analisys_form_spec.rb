require 'rails_helper'

describe "sensory analysis form", js: true do
  let(:page) { NewBeerReviewPage.new(FactoryGirl.create(:beer)) }

  before do
    @user = FactoryGirl.create(:user)
    visit new_user_session_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'Accedi'
  end
  
  it "should add appearance to form field" do
    page.activate_appearance
    page.first_appearance.click
    expect(page.appearance).to eq(page.first_appearance["data-value"])
  end
  
  it "should add taste to form field" do
    page.activate_taste
    page.first_taste.click
    expect(page.taste).to eq(page.first_taste["data-value"])
  end
  
  it "should add palate to form field" do
    page.activate_palate
    page.first_palate.click
    expect(page.palate).to eq(page.first_palate["data-value"])
  end

  it "should add aroma to form field" do
    page.activate_aroma
    page.first_aroma.click
    expect(page.aroma).to eq(page.first_aroma["data-value"])
  end
end
