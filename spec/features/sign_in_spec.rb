require 'rails_helper'

describe "sign in page" do
  before do 
    @user = FactoryGirl.create(:user)
    visit new_user_session_path
  end

  subject { page }
  
  it "should not authenticate" do
    click_button 'Accedi'

    should have_selector('div.alert', text: "L'email o la password inserite non sono valide")
  end

  it "should not show flash in homepage" do
    click_button 'Accedi'

    should have_selector('div.alert')
    visit root_path
    should_not have_selector('div.alert')
  end

  it "should authenticate" do
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password

    click_button 'Accedi'

    should have_link('Esci')
    should have_selector('div.alert', text: 'Bentornato su Beerky!')
    should have_link('Profilo Personale', href: user_path(@user))
  end

  it "should sign out" do
    sign_in

    click_link 'Esci'
    
    should have_link('Accedi')
    should have_selector('div.alert', text: 'Arrivederci a presto su Beerky!')
  end
end
