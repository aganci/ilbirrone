# encoding: utf-8
require 'rails_helper'

describe "show pub page" do
  before do
    @pub = FactoryGirl.create(:pub, 
      name: 'a name', city: 'a city', closing_days: "1,2", 
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      latitude: 1.2345,
      longitude: -1.2345)
    @review = FactoryGirl.create(:review, pub: @pub, date: 1.day.ago, rating: 5, beers: 4, food: 3, value: 2, atmosphere: 1)
    visit pub_path(@pub)
  end

  subject { page }

  it "should show pub name" do
    should have_selector("h1", text: @pub.name)
    should have_link('Scrivi la tua recensione')
  end

  it "should show reviews text" do
    should have_selector('p', text: @review.description)
  end

  it "should format title" do
    should have_title("a name, a city - Recensioni sulle migliori birrerie - Beerky")
  end

  it "should show review rating" do
    expect(all('.small-rating')[0][:alt]).to eq("5 su 5 stelle")
    expect(all('.small-rating')[1][:alt]).to eq("4 su 5 stelle")
    expect(all('.small-rating')[2][:alt]).to eq("3 su 5 stelle")
    expect(all('.small-rating')[3][:alt]).to eq("2 su 5 stelle")
    expect(all('.small-rating')[4][:alt]).to eq("1 su 5 stelle")
  end

  it "should show author" do
    should have_selector("p", text: "#{@review.user.username}")
  end

  it "should show invitation message" do
    should have_selector('p', text: I18n.t('pubs.show.add_review_invitation'))
  end

  it "should show rating" do
    expect(find('.big-rating')[:alt]).to eq('5 su 5 stelle')
  end

  it "should show closing days" do
    should have_selector('td', text: "lunedì, martedì")
  end

  it "should show link to map" do
    should have_link("Visualizza su Google maps", href:"http://maps.google.it?q=1.2345%2C-1.2345")
  end
  
  it "should show link nearby pubs" do
    should have_link("Visualizza locali intorno a te", href: nearby_pubs_path(show: 'map'))
  end

  it "should show its description" do
    should have_content(@pub.description)
  end
  
  it "should show frequenter icon" do
    should_not have_selector('i.icon-home')
  end

  it "should show frequenter icon" do
    FactoryGirl.create(:review, pub: @pub, frequenter: true)

    visit pub_path(@pub)

    should have_selector('i.fa-home')
  end

  it "should show user profile page link" do
    should have_link(@review.user.username, href: user_path(@review.user))
  end

  it "should show search page when click search" do
    visit pub_path(@pub)

    fill_in 'small_search_text', with: 'any'
    click_button 'search_button'

    expect(page).to have_link('Segnala')
  end
end