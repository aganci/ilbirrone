require 'rails_helper'

describe "achievements page" do
  it "should have heading and title" do
    visit achievements_path

    expect(page).to have_title("Acquisisci punti completando le Sfide - Beerky")
    expect(find("h1").text).to eq("Sfide")
  end
end