require 'rails_helper'

describe 'brewpub page' do
  it "should render title" do
    visit brewpub_path

    expect(page).to have_title('Brewpub segnalati dai birronauti - Beerky')
  end

  it "should show only brewpub" do
    brewpub('a brewpub', 'Brewpub')
    brewpub('a pub', 'Birreria')

    visit brewpub_path

    expect(all('.thumbnail h4').map {|link| link.text}.reject {|link| link.blank?}).to eq(['a brewpub'])
  end

  it "should show page link in navbar" do
    visit brewpub_path
    
    expect(all('.navbar li a').map {|link| link.text}).to include('Brewpub')    
  end

  def brewpub(name, pub_type)
    pub = FactoryGirl.create(:pub_with_cover, 
      pub_type: pub_type,
      name: name, 
      description: Forgery(:lorem_ipsum).characters(100))
  end
end