require 'rails_helper'

describe 'event page' do
  it "should set page title and heading" do
    event = FactoryGirl.create(:pub_event, title: 'Event title')

    visit event_path(event)

    expect(page).to have_title('Event title - Beerky')
    find('h1').text == "Event title"
  end

  it "should have same path given a pub event" do
    event = FactoryGirl.create(:pub_event)

    expect(url_for(event)).to eq(event_url(event))
  end

  it "should have same path given a public event" do
    event = FactoryGirl.create(:public_event)
    
    expect(url_for(event)).to eq(event_url(event))
  end
end 