require 'rails_helper'

describe "beer pages" do
  it "should render what is page" do
    visit what_is_beer_path

    expect(page).to have_selector("h1", text: I18n.t('helpers.links.what_is_beer'))
    expect(page).to have_link(I18n.t('helpers.links.home_page'), href: root_path)
  end

  it "should render craft beer page" do
    visit craft_beer_path

    expect(page).to have_selector("h1", text: I18n.t('helpers.links.craft_beer'))
    expect(page).to have_link("birra", href: what_is_beer_path)
    expect(page).to have_link("contattandoci", href: new_contact_us_path)
    expect(page).to have_link(I18n.t('helpers.links.home_page'), href: root_path)
  end

  it "should render history page" do
    visit history_path

    expect(page).to have_selector("h1", text: I18n.t('helpers.links.history'))
    expect(page).to have_link("birra", href: what_is_beer_path)
    expect(page).to have_link(I18n.t('helpers.links.home_page'), href: root_path)
  end

  it "should render oktoberfest page" do
    FactoryGirl.create(:pub, name: 'Hofbrauhaus', city: 'Genova')

    visit oktoberfest_path

    expect(page).to have_selector('h1', text: 'Oktoberfest')
    expect(page).to have_link(I18n.t('helpers.links.home_page'), href: root_path)
  end
end