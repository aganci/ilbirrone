require 'rails_helper'

describe 'nearby pubs page' do
  
  it "should show pubs in map", js: true do
    visit nearby_pubs_path(show: 'map')
    geolocate_at(1.2345, -1.2345)

    expect(page).to have_selector('#pubs_map')
  end

  it "should render meta for apple app" do
    visit nearby_pubs_path

    expect(page).to have_css 'meta[name="apple-itunes-app"]', :visible => false

    visit root_path
    
    expect(page).to_not have_css 'meta[name="apple-itunes-app"]', :visible => false
  end

  def geolocate_at(latitude, longitude)
    page.execute_script "latitude = '#{latitude}'; longitude = '#{longitude}';"
    page.execute_script "state = 'ok';"
  end
end