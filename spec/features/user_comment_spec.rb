require 'rails_helper'

describe 'user comment' do
  let(:user_a) { FactoryGirl.create(:user) }
  let(:user_b) { FactoryGirl.create(:user) }

  it "should show user comment" do
    sign_in user_a

    visit user_path(user_b)

    fill_in 'user_comment_content', with: 'a message for user b'
    click_button 'Pubblica'

    expect(page).to have_content('a message for user b')
  end

  it "should delete user comment" do
    FactoryGirl.create(:user_comment, user: user_a, commentable: user_b)
    expect(Comment.count).to eq(1)
    sign_in user_a

    visit user_path(user_b)

    click_link 'Elimina'

    expect(Comment.count).to eq(0)
  end

  it "should render errors" do
    sign_in user_a

    visit user_path(user_b)

    click_button 'Pubblica'

    expect(page).to have_errors
  end
end