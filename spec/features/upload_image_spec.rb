require 'rails_helper'

describe 'upload image' do
  before(:all) do
    @server = FakeFtp::Server.new(21212, 21213)
    @server.start
  end

  before do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end

  after(:all) do
    @server.stop
  end

  it "should add a comment with a photo" do
    pub = FactoryGirl.create(:pub)

    visit pub_path(pub)
    
    fill_in 'comment_content', with: 'some interesting comment'
    attach_file('comment_image_file', a_picture_file)

    click_button 'Pubblica'

    expect(@server.files.size).to eq(1)
    expect(Comment.first).to have_image
  end

  it "should set pub cover" do
    pub = FactoryGirl.create(:pub)

    visit pub_path(pub)
    
    attach_file('cover_file', a_picture_file)

    click_button 'Carica la foto'
    
    should_have_cover pub    
  end

  it "should set event cover" do
    event = FactoryGirl.create(:public_event)

    visit event_path(event)

    attach_file('cover_file', a_picture_file)

    click_button 'Carica la foto'

    should_have_cover event
  end

  it "should set beer cover" do
    beer = FactoryGirl.create(:beer)

    visit beer_path(beer)

    attach_file('cover_file', a_picture_file)

    click_button 'Carica la foto'

    should_have_cover beer
  end

  it "should upload avatar" do
    visit edit_user_registration_path(@user)
    
    attach_file('cover_file', a_picture_file)
    click_button 'Carica la foto'

    expect(@server.files).to include("#{@user.id}.jpg")
    expect(Comment.count).to eq(1)
    comment = Comment.first
    expect(comment.path).to eq("users/#{@user.id}.jpg")
    expect(comment.height).to eq(200)
    expect(comment.width).to eq(200)
  end

  def a_picture_file
    Rails.root.join('spec', 'fixturesfiles', 'avatar.jpg')
  end

  def should_have_cover(model)
    expect(@server.files).to include("#{model.friendly_id}.jpg")
    expect(Comment.first.image_type).to eq("Cover")
  end
end