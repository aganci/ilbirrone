require 'rails_helper'

describe "user profile page" do
  before do
    @user = FactoryGirl.create(:user, username: "a user")
    @pub = FactoryGirl.create(:pub, user: @user)
    @review = FactoryGirl.create(:review, user: @user, pub: @pub)
    @comment = FactoryGirl.create(:comment, content: 'some comment content', commentable: @pub, user: @user)
  end

  it "should show user name" do
    visit user_path(@user)

    expect(page).to have_selector('h1', text: "a user")
    expect(page).to have_title("Profilo di a user - Beerky")
  end

  it "should show avatar with points" do
    visit user_path(@user)

    expect(page).to have_selector("img[src='/assets/avatar.jpg']")
    expect(page).to have_selector(".score", text: @user.points.to_s)
  end 

  it "should show his reviews" do
    visit user_path(@user)
    
    expect(page).to have_link(@pub.name, href: pub_path(@pub))    
  end

  it "should show profile page link" do
    sign_in(@user)

    visit root_path

    expect(page).to have_link("Profilo Personale", href: user_path(@user))
  end

  it "should show its comments" do
    visit user_path(@user)
    
    expect(page).to have_content('some comment content')
  end

  it "should show its events" do
    event = FactoryGirl.create(:public_event, user: @user, title: 'an event')
    
    visit user_path(@user)

    expect(page).to have_link('an event', href: event_path(event))
  end
end