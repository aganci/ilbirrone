require 'rails_helper'

describe "new beer page" do
  before do
    user = FactoryGirl.create(:user)
    sign_in user
    pub = FactoryGirl.create(:pub, user: user)

    visit new_beer_path(pub_id: pub.id)
  end

  it "should render errors" do
    click_button 'Salva'

    expect(page).to have_errors
  end

  it "should insert beer" do
    fill_in "Nome", with: 'a beer name'
    fill_in "Gradi alcolici", with: '10,2'
    choose('beer_color_bianca')
    choose('beer_fermentation_alta')
    choose('beer_family_ale')
    select(Beer.styles.first, from: "beer_style")
    select(Beer.types.first, from: "beer_beer_type")
    fill_in "Temperatura di servizio", with: "12-12,5"
    fill_in "Descrizione commerciale", with: "bla bla bla bla"

    expect { click_button 'Salva' }.to change(Beer, :count).by(1)
  end
end