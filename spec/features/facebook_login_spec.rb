require 'rails_helper'

describe "facebook login" do
  before do
    OmniAuth.config.test_mode = true

    OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new({
        :provider => 'facebook',
        :uid => '123545',
        :extra => {
          :raw_info => {
            name: "username"
          }
        },
        :info => {
          email: 'user@example.org'
        }
    })
  end

  it "should create new user with facebook information" do
    visit "/utenti/auth/facebook"

    user = User.find_by_username('username')
    expect(user).to_not be_nil
    expect(user.email).to eq('user@example.org')
    expect(user).to be_valid
  end

  it "should access to the site" do
    visit "/utenti/auth/facebook"
    
    expect(page).to have_link("username")    
  end
end