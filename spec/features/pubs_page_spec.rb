require 'rails_helper'

describe 'pubs page' do
  it "should set title" do
    visit pubs_path

    expect(page).to have_title('Birrerie e Pub segnalati dai birronauti - Beerky')
  end

  it "should show breadcrumb in pubs page" do
    visit pubs_path

    expect(breadcrumb_labels).to eq(['Home', 'Birrerie e Pub'])
    expect(breadcrumb_links).to eq([root_path])
  end

  it "should show sub navbar links" do
    visit pubs_path
    
    expect(all('.sub-navbar li').map {|element| element.text}).to eq(['Nuove segnalazioni', '|', 'Nuove recensioni', '|', 'I più curiosi'])
    expect(all('.sub-navbar a').map {|element| element[:href]}).to eq([recent_reviews_pubs_path, curious_pubs_path])
  end

  it "should show recent reviews link" do
    visit pubs_path

    expect(page).to have_link('Nuove recensioni', href: recent_reviews_pubs_path)
  end


  it "should have back to home title" do
    visit pubs_path
    
    expect(page).to have_link(I18n.t('helpers.links.home_page'), href: root_path)    
  end

  it "should order pubs by creation date" do
    pub('second', 2.day.ago)
    pub('first', 1.day.ago)
    
    visit pubs_path

    expect(all('.thumbnail h4').map {|link| link.text}).to eq(['first', 'second'])
  end

  it "should show page link in navbar" do
    visit pubs_path
    
    expect(all('.navbar li a').map {|link| link.text}).to include('Birrerie & Pub')    
  end

  it "should show review instructions link" do
    visit pubs_path
    
    expect(page).to have_link('Scrivi la tua recensione!', href: select_for_review_pubs_path)    
  end

  def pub(name, created_at)
    pub = FactoryGirl.create(:pub_with_cover, 
      name: name, 
      created_at: created_at, 
      description: Forgery(:lorem_ipsum).characters(100))
  end
end