require 'rails_helper'

describe 'users page' do
  it "should set page title" do
    visit users_path

    expect(page).to have_title('Classifica dei Birronauti - Beerky')
    expect(find('h1').text).to eq("Birronauti")    
  end

  it "should list users ordered by earned points" do
    allow(User).to receive(:rankings).and_return(paginated_users([user_with_points('first', 10), user_with_points('second', 9)]))

    visit users_path

    expect(usernames).to eq(['first', 'second'])
    expect(all('.points').map {|element| element.text}).to eq(['10', '9'])
  end

  it "should paginate" do
    allow(User).to receive(:rankings).and_return(paginated_users([user_with_points('a user', 10)] * 33))

    visit users_path

    expect(page).to have_selector('.pagination')
  end

  it "should search", js: true do
    FactoryGirl.create(:user, username: "antonio")
    FactoryGirl.create(:user, username: "Ganci Antonio")
    FactoryGirl.create(:user, username: "Antonino")
    FactoryGirl.create(:user, username: "Andrea")

    visit users_path
    fill_in "user-search", with: 'antoni'

    page.document.synchronize do
      usernames == ['antonio', 'Ganci Antonio', 'Antonino']
    end
  end

  def paginated_users users
    WillPaginate::Collection.create(1, 32, users.length) do |pager|
      pager.replace users
    end    
  end

  def user_with_points(username, points)
    FactoryGirl.build(:user, id: 1, username: username, points: points)
  end

  def usernames
    all('.username').map {|element| element.text}
  end
end