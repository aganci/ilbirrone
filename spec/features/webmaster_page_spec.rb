require 'rails_helper'

describe 'Webmaster page' do
  before do
    @pub = FactoryGirl.create(:pub, name: 'pub name')
  end

  it "should render page" do
    visit webmaster_pub_path(@pub)

    expect(page).to have_title("Risorse per i webmaster pub name - Beerky")
  end

  it "should not delete old images given some external website linking them" do
    expect(Rails.application.assets.find_asset("segnalato-su-ilbirrone.png")).to_not be_nil
    expect(Rails.application.assets.find_asset("segnalato-su-ilbirrone-medio.png")).to_not be_nil
  end
end