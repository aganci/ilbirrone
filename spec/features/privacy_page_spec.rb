require 'rails_helper'

describe "privacy page" do
  it "should render content" do
    visit privacy_informations_path

    expect(page).to have_content('Misure che adottiamo per proteggere le informazioni personali degli utenti') 
  end

  it "should show link in homepage" do
    visit root_path

    expect(page).to have_link('Dichiarazione sulla privacy', href: privacy_informations_path)
  end  

  it "should set title" do
    visit privacy_informations_path
    
    expect(page).to have_title('Dichiarazione sulla privacy - Beerky')    
  end

  it "must accepts privacy policy" do
    user = FactoryGirl.build(:user, privacy_policy_accepted: false)
    expect(user).to_not be_valid

    user.privacy_policy_accepted = nil
    expect(user).to_not be_valid
  end
end