require 'rails_helper'

describe "social links" do
  before do
    visit root_path
  end

  it "should render facebook link" do
    expect(page).to have_link('', href: "https://www.facebook.com/pages/Beerky/446032622104944")
  end

  it "should render twitter link" do
    expect(page).to have_link('', href: "https://twitter.com/BeerkyIt")
  end

end