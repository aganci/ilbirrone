require 'rails_helper'

describe "beer page" do
  before do
    @pub = FactoryGirl.create(:pub, name: "A pub name")
    @beer = FactoryGirl.create(:beer, name: "A beer name", description: "The beer description", pub: @pub)
  end

  subject { page }

  it "should use url friendly id" do
    visit "/birre/a-beer-name-a-pub-name"
    
    should have_selector("h1", text: "A beer name")
    should have_title("Birra A beer name A pub name - Recensioni sulle migliori birre artigianali - Beerky")
  end

  it "should show beer producer" do
    visit beer_path(@beer)

    should have_link("A pub name", href: pub_path(@pub))
  end

  it "should show description" do
    visit beer_path(@beer)

    should have_selector("p", text: "Descrizione commerciale")
  end

  it "should show new review link" do
    visit beer_path(@beer)
    
    should have_link("Scrivi la tua recensione", href: new_beer_review_path(beer_id: @beer.id))    
  end

  it "should show rating" do
    FactoryGirl.create(:beer_review, rating: 5, beer: @beer)
    
    visit beer_path(@beer)

    expect(find('.big-rating')[:alt]).to eq("5 su 5 stelle")
  end

  it "should show reviews" do
    review = FactoryGirl.create(:beer_review, rating: 5, content: "beer review", beer: @beer)

    visit beer_path(@beer)
    should have_link(review.user.username, href: user_path(review.user))
    expect(find('.small-rating')[:alt]).to eq("5 su 5 stelle")

    should have_selector("p", text: "meno di un minuto fa")
    should have_selector("p", text: "beer review")
  end

  it "should show oktoberfest banner" do
    visit beer_path(@beer)

    expect(page).to have_selector('h5 a', text: 'Speciale Oktoberfest')
  end
end