require 'rails_helper'

describe 'where are you question', js: true do

  it "should show where are you question given a mobile request" do
    page.driver.browser.header('User-Agent', 'Mobile')
    pub = FactoryGirl.create(:pub, name: 'a pub', city: 'a city', latitude: 1, longitude: 2)

    visit root_path
    geolocate_at(1, 2)
    expect(page).to have_selector('#current-position h3')

    click_link('Si')
    expect(page.current_path).to eq(pub_path(pub))

    expect(Visitor.count).to eq(1)
  end

  def geolocate_at(latitude, longitude)
    page.execute_script "latitude = '#{latitude}'; longitude = '#{longitude}';"
    page.execute_script "state = 'ok';"
  end

end