require 'rails_helper'

describe "new review page" do
  let(:pub) { FactoryGirl.create(:pub) }
  let(:page) { NewReviewPage.new(pub) }
  
  before do
    sign_in(FactoryGirl.create(:user))
  end

  subject { page }

  its(:title) { should == pub.name }
  its(:full_address) { should == pub.full_address }

  it "should add review" do
    page.select_date '17/06/2012'
    page.beers = "3"
    page.food = "4"
    page.value = "2"
    page.atmosphere = "1"
    page.review = Forgery(:lorem_ipsum).characters(100)
    page.rating = "5"
    page.check_frequenter

    expect { page.save }.to change(Review, :count).by(1)
  end

  it "should show errors" do
    page.save

    should have_errors
  end
end