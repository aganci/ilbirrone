require 'rails_helper'

describe "autocomplete", js: true do
  it "should show autocomplete in small search box" do
    visit pub_path(FactoryGirl.create(:pub))

    fill_in "small_search_text", with: 'milano'    
    
    expect(page).to have_autocomplete
  end

  it "should update province and region after city has been selected" do
    sign_in FactoryGirl.create(:user)

    visit new_pub_path

    fill_in "pub_city", with: 'roma'

    expect(page).to have_autocomplete
    first_autocomplete_item.click
    
    expect(page.find_field("pub_province").value).to eq("Roma")
    expect(page.find_field("pub_region").value).to eq("Lazio")
  end

  def have_autocomplete
    have_selector(".typeahead.dropdown-menu")
  end

  def first_autocomplete_item
    all(:css, ".typeahead.dropdown-menu li").first
  end
end
