require 'rails_helper'

describe 'user can propose changes' do
  before do
    allow(Captcha).to receive(:valid?).and_return(true)
  end
  
  it "should show current pub informations" do
    pub = FactoryGirl.build(:pub, id: 1234, name: 'current name', 
      address: 'via Santa Maria del Pozzo', postcode: '80049', city: 'Somma Vesuviana', province: 'Napoli',
      phone: '02 12345678', description: 'the pub full description', pub_type: 'Brewpub', latitude: 1.2344, longitude: -4.1234556)
    allow(Pub).to receive(:find).with("1234").and_return(pub)

    visit change_pub_path(pub)

    expect(find('#pub_changes_name').value).to eq('current name')
    expect(find('#pub_changes_full_address').value).to eq('via Santa Maria del Pozzo - 80049 Somma Vesuviana (Napoli)')
    expect(find('#pub_changes_phone').value).to eq('02 12345678')
    expect(find('#pub_changes_description').value).to eq('the pub full description')
    expect(find("#pub_changes_pub_type_brewpub")['checked']).to eq('checked')
    expect(find('#pub_changes_duplicated')['type']).to eq('checkbox')
    expect(find('#pub_changes_note').value).to be_empty
    expect(find('#pub_changes_coordinates').value).to eq('1.2344, -4.1234556')
  end

  it "should have cancel link" do
    pub = FactoryGirl.build(:pub, id: 1234)
    allow(Pub).to receive(:find).with("1234").and_return(pub)

    visit change_pub_path(pub)

    expect(page).to have_link('Annulla', href: pub_path(pub))
  end

  it "should send email with new pub informations" do
    pub = FactoryGirl.create(:pub, name: 'current name', email: 'pub@example.com', website: 'http://example.org', closing_days: 0, phone: '123456789', 
      description: 'a pub description')

    visit change_pub_path(pub)
    fill_in('Nome', with: 'new name')
    click_button 'Invia le Modifiche'

    expect(body.css('.difference-label').map {|e| e.text}).to eq(["Nome"])
    expect(body.css('.difference-value').map {|e| e.text}).to eq(["new name"])
  end

  it "should send email with logged user link" do
    user = FactoryGirl.create(:user)
    sign_in user
    pub = FactoryGirl.create(:pub, name: 'current name')

    visit change_pub_path(pub)
    click_button 'Invia le Modifiche'

    expect(last_email.body).to include(user_path(user))
  end

  def body
    Nokogiri::HTML::fragment(last_email.body.to_s)
  end
end