# encoding: utf-8
require 'rails_helper'

describe "beers page" do
  before do
    @pub = FactoryGirl.create(:pub)
    @four_stars_beer = FactoryGirl.create(:beer_with_cover, name: 'four stars beer', pub: @pub)
    FactoryGirl.create(:beer_review, rating: 4, beer: @four_stars_beer)
    @five_stars_beer = FactoryGirl.create(:beer_with_cover, name: 'five stars beer', pub: @pub)
    FactoryGirl.create(:beer_review, rating: 5, beer: @five_stars_beer)

    visit beers_path
  end

  it "should set title" do
    expect(page).to have_title('Le Birre più popolari')
  end

  it "should show beers ordered by rating" do
    expect(all('.thumbnail a').map {|element| element[:href] }).to eq([
      beer_path(@five_stars_beer), beer_path(@five_stars_beer), pub_path(@pub), #first thumbnail links
      beer_path(@four_stars_beer), beer_path(@four_stars_beer), pub_path(@pub), #second thumbnail links
    ])
  end

  it "should show beer pages links" do
    expect(page).to have_link I18n.t('helpers.links.what_is_beer'), href: what_is_beer_path
    expect(page).to have_link I18n.t('helpers.links.craft_beer'), href: craft_beer_path
    expect(page).to have_link I18n.t('helpers.links.history'), href: history_path
  end

  it "should render oktoberfest banner" do
    expect(page).to have_selector('.oktoberfest-banner')
  end

  it "should show homepage link" do
    expect(page).to have_link("Torna all'home page", href: root_path)
  end

  it "should show page link in navbar" do
    expect(all('.navbar li a').map {|link| link.text}).to include('Birre')    
  end

end