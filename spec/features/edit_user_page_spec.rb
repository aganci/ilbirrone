require 'rails_helper'

describe 'Edit user profile' do
  let(:user) {FactoryGirl.create(:user, username: 'a username', city: 'my city')}
  
  before do
    sign_in user
    visit edit_user_registration_path(user)
  end

  it "should edit user" do
    fill_in 'Nome utente', with: 'a new username'
    fill_in 'Dove vivi?', with: 'a new town'

    click_button 'Salva'
    user.reload

    expect(user.username).to eq('a new username')
    expect(user.city).to eq('a new town')
  end  

  it "should autocomplete city", js: true do
    fill_in "user_city", with: 'millesimo'

    expect(page).to have_autocomplete
    first_autocomplete_item.click
    
    expect(page.find_field("user_city").value).to eq("Millesimo, Savona, Liguria")
  end

  def have_autocomplete
    have_selector(".typeahead.dropdown-menu")
  end

  def first_autocomplete_item
    all(:css, ".typeahead.dropdown-menu li").first
  end
end