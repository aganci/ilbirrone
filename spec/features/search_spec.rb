require 'rails_helper'

describe "search results page" do
  it "should find pubs by name part" do
    befed_milano = FactoryGirl.create(:pub, name: 'BEFED - Milano')
    befed = FactoryGirl.create(:pub, name: 'BEFED')
    FactoryGirl.create(:pub, name: 'some name')

    visit search_path(text: 'befed')

    expect(page).to have_link('BEFED - Milano', href: pub_path(befed_milano))
    expect(page).to have_link('BEFED', href: pub_path(befed))
    expect(page).to_not have_link('some name')
  end

  it "should find pubs by city and province" do
    milano_city = FactoryGirl.create(:pub, name: 'a', city: 'Milano')    
    milano_marittima_city = FactoryGirl.create(:pub, name: 'b', city: 'Milano marittima')    
    milano_province = FactoryGirl.create(:pub, name: 'c', province: 'Milano')

    visit search_path(text: 'milano')

    expect(page).to have_link(milano_city.name, href: pub_path(milano_city))
    expect(page).to have_link(milano_marittima_city.name, href: pub_path(milano_marittima_city))
    expect(page).to have_link(milano_province.name, href: pub_path(milano_province))
  end

  it "should find beers by name part" do
    la_bionda = FactoryGirl.create(:beer, name: 'la bionda')
    bionda = FactoryGirl.create(:beer, name: 'Bionda')
    FactoryGirl.create(:beer, name: 'some name')

    visit search_path(text: 'bionda')

    expect(page).to have_link('la bionda', href: beer_path(la_bionda))
    expect(page).to have_link('Bionda', href: beer_path(bionda))
    expect(page).to_not have_link('some name')
  end

  it "should find beers of approved pubs" do
    not_approved_pub = FactoryGirl.create(:pub, status: 'submitted')
    FactoryGirl.create(:beer, name: 'not approved', pub: not_approved_pub)

    visit search_path(text: 'not approved')

    expect(page).to_not have_link('not approved')
  end

  it "should render new pub button" do
    visit search_path(text: 'search text')

    expect(page).to have_link('Segnala', href: new_pub_path)
  end

  it "should show results for message" do
    visit search_path(text: 'search text')

    expect(page).to have_selector('h1', text: 'Risultati per "search text"')
  end
end