require 'rails_helper'

describe 'sign up redirection' do
  it "should redirect to current page after sign up" do
    visit a_path

    sign_up

    expect(page.current_path).to eq(a_path)
  end
  
  def a_path
    new_contact_us_path
  end
end