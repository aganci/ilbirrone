require 'rails_helper'

describe 'recent pub reviews page' do
  it "should set title" do
    visit recent_reviews_pubs_path

    expect(page).to have_title('Recensioni recenti Birrerie e Pub - Beerky')
  end

  it "should show sub navbar links" do
    visit recent_reviews_pubs_path
    
    expect(all('.sub-navbar li').map {|element| element.text}).to eq(['Nuove segnalazioni', '|', 'Nuove recensioni', '|', 'I più curiosi'])
    expect(all('.sub-navbar a').map {|element| element[:href]}).to eq([pubs_path, curious_pubs_path])
  end

  it "should show reviewed pubs ordered by review creation date" do
    a_review_for(a_pub('second'), 2.day.ago)
    a_review_for(a_pub('first'), 1.day.ago)
    a_pub('without review')

    visit recent_reviews_pubs_path

    expect(all('.thumbnail h4').map {|title| title.text}).to eq(['first', 'second'])
  end

  def a_review_for(pub, created_at)
    FactoryGirl.create(:review, created_at: created_at, pub: pub)
  end

  def a_pub(name)
    FactoryGirl.create(:pub, name: name)
  end
end