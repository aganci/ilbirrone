require 'rails_helper'

describe "sign up page" do
  before do
    visit new_user_registration_path
  end
  
  describe 'filled in form' do  
    before do
      fill_in("Nome utente", with: "john smith")
      fill_in("Email", with: "john@example.com")
      fill_in("Password", with: 'secret')
      fill_in("Conferma password", with: 'secret')
      fill_in("Dove vivi?", with: 'Alba, Cuneo, Piemonte')
      check("Accetto tutti i termini e le condizioni del regolamento e della disciplina sulla privacy")
    end
    
    it "create new user" do
      expect { click_button "Iscriviti" }.to change(User, :count).by(1)
    end

    it "show welcome flash message" do
      click_button "Iscriviti"

      expect(page).to have_selector('div.alert', text: 'Benvenuto su Beerky! Il sito degli appassionati della birra')
    end

    it "should send welcome email" do
      click_button "Iscriviti"
      
      expect(last_email.subject).to eq("Benvenuto su Beerky!")
      expect(mail_body).to include('john smith')
    end

    it "should save notification" do
      expect { click_button "Iscriviti" }.to change(Notification, :count).by(1)
    end

    def mail_body
      last_email.body.parts.find {|p| p.content_type.match /html/}.body.raw_source
    end
  end

 it "should autocomplete city", js: true do
    fill_in "user_city", with: 'millesimo'

    expect(page).to have_autocomplete
    first_autocomplete_item.click
    
    expect(page.find_field("user_city").value).to eq("Millesimo, Savona, Liguria")
  end

  def have_autocomplete
    have_selector(".typeahead.dropdown-menu")
  end

  def first_autocomplete_item
    all(:css, ".typeahead.dropdown-menu li").first
  end

 it "renders errors" do
    click_button "Iscriviti!"

    expect(page).to have_errors
 end
end