require 'rails_helper'

describe 'pub form', js: true do
  before do
    sign_in(FactoryGirl.create(:user))
    
    visit new_pub_path
  end

  it "should update hints when change nation", js: true do
    select('Regno Unito', from: 'Nazione')

    expect(find('.pub_name .help-block').text).to eq('es: London Pub, Aufsturz')
    expect(find('.pub_province .help-block').text).to eq('es: Greater London, Ohio')
    expect(find('.pub_postcode .help-block').text).to eq('es: WC2N 4HS')
    expect(find('.pub_phone .help-block').text).to eq('es: 020 7379 9883')
  end

  it "should change province label for a foreign nation", js: true do
    select('Regno Unito', from: 'Nazione')

    expect(find('.pub_province label').text).to eq("Stato")
  end

  it "should disable city autocomplete when foreign nation" do
    select('Regno Unito', from: 'Nazione')
    fill_in 'Città', with: 'mil'

    expect(page).to_not have_selector(".typeahead.dropdown-menu")
    
    select('Italia', from: 'Nazione')
    fill_in 'Città', with: 'mil'

    expect(page).to have_selector(".typeahead.dropdown-menu")
  end

  it "should hide region when foreign nation", js: true do
    fill_in 'Regione', with: 'something'
    select('Regno Unito', from: 'Nazione')

    expect(page).to have_selector('.pub_region', visible: false)
    
    select('Italia', from: 'Nazione')
    
    expect(page).to have_selector('.pub_region', visible: true)
    expect(find_field('pub_region').value).to eq('')
  end

  it "should show errors" do
    click_button 'Salva'

    expect(page).to have_errors
  end

end