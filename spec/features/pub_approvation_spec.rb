# encoding: utf-8
require 'rails_helper'

describe "Pub approvation" do
  describe PubsController, type: :controller do
    before do
      sign_in(FactoryGirl.create(:user))
      post :create, pub: {name: "a pub", address: 'an address', city: "a city", province: "a province", country: 'a country'}
    end

    it "changes pub status to submitted" do
      expect(assigns["pub"][:status]).to eq('submitted')
    end

    it "show message after submission" do
      expect(flash[:success]).to eq(I18n.t('pubs.create.submitted'))
    end
  end

  describe Admin::PubsController, type: :controller do
    before do
      sign_in(FactoryGirl.create(:admin))
      post :create, pub: {name: "a pub", address: 'an address', city: "a city", province: "a province", country: 'a country'}
    end

    it "should be approved after creation for administrators" do
      expect(assigns["pub"][:status]).to eq('approved')
    end
  end

  describe "administration" do
    before do
      sign_in(FactoryGirl.create(:admin))
      @user = FactoryGirl.create(:user, email: 'user@example.com')
      @pub = FactoryGirl.create(:pub, name: 'Terzotempo', status: 'submitted', user: @user)
    end
    
    it "should show approve button" do
      visit admin_pubs_path

      expect(page).to have_link('Approva')

      click_link('Approva')

      @pub.reload
      expect(@pub).to be_approved
      end

    it "should not show approve button when already approved" do
      @pub.approve!
      
      visit admin_pubs_path

      expect(page).to_not have_link('Approva')
    end

    it "sends email of approvation" do
      visit admin_pubs_path
      click_link('Approva')

      expect(last_email.subject).to have_content("Terzotempo")
      expect(last_email.to).to eq(['user@example.com'])
      expect(last_email.body).to include(pub_path(@pub))
      expect(last_email.from).to eq(['info@ilbirrone.it'])
    end
  end
end 

