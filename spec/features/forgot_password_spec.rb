# encoding: utf-8

require 'rails_helper'

describe "forgot password" do
  before do
    @user = FactoryGirl.create(:user)

    visit new_user_password_path
  end

  it "should show error when not found" do
    fill_in('Email', with: 'nonexistent')
    
    click_send_email
    
    expect(page).to have_content('email non trovata')
  end

  it "should send email with istructions" do
    fill_in('Email', with: @user.email)
    
    click_send_email

    expect(last_email.body).to include("Buongiorno #{@user.username}")
    expect(last_email.subject).to eq("Richiesta di modifica della password")
  end

  it "change password" do
    fill_in('Email', with: @user.email)
    click_send_email
    
    click_link_change_password

    fill_in 'Nuova password', with: 'newsecret'
    fill_in 'Conferma password', with: 'newsecret'

    click_button 'Cambia password'

    expect(page).to have_selector('div.alert', text: 'La tua password è stata cambiata. Bentornato su Beerky!')
  end

  def click_send_email
    click_button 'Spediscimi le istruzioni per reimpostare la password'
  end

  def click_link_change_password
    html = Nokogiri::HTML::fragment(last_email.body.to_s)
    link = html.css('a').first[:href]
    visit link
  end
end