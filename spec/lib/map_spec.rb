require 'map'
require 'ostruct'

describe Map do
  let(:http) { double("http") }
  let(:pub) {double("pub", full_address: 'an address', set_location: nil)}

  before do
    stub_const('Net::HTTP', http)
  end

  it "should update pub location" do
    allow(http).to receive(:get_response).and_return(google_api_response("OK"))
    
    expect(pub).to receive(:set_location).with(37.42291810, -122.08542120)
    
    Map.geocode(pub)    
  end

  it "should geocode address" do
    expect(http).to receive(:get_response).with(google_api_url("an+address")).and_return(google_api_response("OK"))

    expect(Map.geocode(pub)).to eq(OpenStruct.new(latitude: 37.42291810, longitude: -122.08542120, success: true, address: "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA"))
  end

  it "should not geocode given a response without results" do
    allow(http).to receive(:get_response).and_return(google_api_response("ZERO_RESULTS"))

    expect(Map.geocode(pub)).to eq(OpenStruct.new(success: false, status: "ZERO_RESULTS"))
  end

  def google_api_response(status)
    response = double('response')
    allow(response).to receive(:body).and_return(
<<-EOF
{
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "1600",
               "short_name" : "1600",
               "types" : [ "street_number" ]
            },
            {
               "long_name" : "Amphitheatre Pkwy",
               "short_name" : "Amphitheatre Pkwy",
               "types" : [ "route" ]
            },
            {
               "long_name" : "Mountain View",
               "short_name" : "Mountain View",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Santa Clara",
               "short_name" : "Santa Clara",
               "types" : [ "administrative_area_level_2", "political" ]
            },
            {
               "long_name" : "California",
               "short_name" : "CA",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "United States",
               "short_name" : "US",
               "types" : [ "country", "political" ]
            },
            {
               "long_name" : "94043",
               "short_name" : "94043",
               "types" : [ "postal_code" ]
            }
         ],
         "formatted_address" : "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",
         "geometry" : {
            "location" : {
               "lat" : 37.42291810,
               "lng" : -122.08542120
            },
            "location_type" : "ROOFTOP",
            "viewport" : {
               "northeast" : {
                  "lat" : 37.42426708029149,
                  "lng" : -122.0840722197085
               },
               "southwest" : {
                  "lat" : 37.42156911970850,
                  "lng" : -122.0867701802915
               }
            }
         },
         "types" : [ "street_address" ]
      }
   ],
   "status" : "#{status}"
}
EOF
)
response
  end

  def google_api_url(address)
    URI.parse("http://maps.googleapis.com/maps/api/geocode/json?address=#{address}&sensor=false&language=it")
  end
end