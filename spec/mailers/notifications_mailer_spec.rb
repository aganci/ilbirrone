# encoding: utf-8

require 'rails_helper'

describe NotificationsMailer do
  
  describe 'pub changes' do
    let(:pub) { FactoryGirl.build(:pub, id: 12345) }
    let(:changes) { PubChanges.from(pub, nil) }
    
    it "should include pub url" do
      mail = NotificationsMailer.pub_changes(pub, changes, nil)

      expect(mail.body).to include(pub_url(pub))
    end
    
    it "should include user url" do
      user = FactoryGirl.build(:user, id: 123)
      mail = NotificationsMailer.pub_changes(pub, changes, user)

      expect(mail.body).to include(user_url(user))
    end

    it "should include formatted closing days" do
      changes.closing_days = "0,1"
      
      mail = NotificationsMailer.pub_changes(pub, changes, nil)

      expect(body(mail).css('.difference-label').map {|e| e.text}).to eq(['Giorni di chiusura'])
      expect(body(mail).css('.difference-value').map {|e| e.text}).to eq(['domenica, lunedì'])
    end 
  end

  def body(mail)
    Nokogiri::HTML::fragment(mail.body.to_s)
  end
end