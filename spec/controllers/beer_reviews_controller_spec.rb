require 'rails_helper'

describe BeerReviewsController do
  let(:user) { FactoryGirl.create(:user) }
  let(:beer) { FactoryGirl.create(:beer) }

  it "should redirect to sign_in page" do
    get :new, beer_id: beer.id

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to beer page" do
    sign_in user
    post :create, beer_review_attributes

    expect(response).to redirect_to(beer_path(beer))
  end

  it "should save notification" do
    sign_in user
    
    expect { post :create, beer_review_attributes }.to change(Notification, :count).by(1)
  end

  def beer_review_attributes
    { beer_review: {rating: 5, beer_id: beer.id, user_id: user.id} }
  end
end