require 'rails_helper'

describe PublicEventsController do
  let(:pub) { FactoryGirl.create(:pub) }

  it "should redirect to login page when the user is not signed in" do
    get :new, pub_id: pub.id

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should save creation notification" do
    user = FactoryGirl.create(:user)
    sign_in user

    expect { post :create, public_event: public_event_attributes(user) }.to change(Notification, :count).by(1)
  end

  def public_event_attributes(user)
    FactoryGirl.attributes_for(:public_event).merge(user_id: user.id)
  end
end