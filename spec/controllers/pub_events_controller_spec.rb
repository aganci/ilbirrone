require 'rails_helper'

describe PubEventsController do
  before do
    @pub = FactoryGirl.create(:pub)
  end

  it "should redirect to login page when the user is not signed in" do
    get :new, pub_id: @pub.id

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to event page" do
    user = FactoryGirl.create(:user)
    sign_in user
    post :create, pub_event: {title: 'a title', description: 'a description', start_date: Date.today, end_date: Date.today, pub_id: @pub.id, user_id: user.id }

    event = Event.all.first
    expect(response).to redirect_to(event_path(event))
  end

  it "should save creation notification" do
    user = FactoryGirl.create(:user)
    sign_in user

    expect { post :create, pub_event: pub_event_attributes(user) }.to change(Notification, :count).by(1)
  end

  def pub_event_attributes(user)
    FactoryGirl.attributes_for(:pub_event).merge(user_id: user.id)
  end
end