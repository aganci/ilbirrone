require 'rails_helper'

describe VisitorsController do
  it "should redirect to sign in page given a user not signed in" do
    get :visit_pub, pub_id: 1234

    expect(response).to redirect_to(new_user_session_path)    
  end

  it "should not save visitor given a user that alread visit the pub" do
    user = FactoryGirl.create(:user)
    pub = FactoryGirl.create(:pub)
    FactoryGirl.create(:visitor, user: user, pub: pub)
    sign_in user

    get :visit_pub, pub_id: pub.id

    expect(Visitor.count).to eq(1)
  end

  it "should save coordinates" do
    pub = FactoryGirl.create(:pub)

    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: true

    expect(Visitor.count).to eq(1)
    expect(Visitor.first.latitude).to eq(-1.234)
    expect(Visitor.first.longitude).to eq(1.234)
  end

  it "should save current user given a logged user" do
    pub = FactoryGirl.create(:pub)
    user = FactoryGirl.create(:user)
    sign_in user

    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: true

    expect(Visitor.count).to eq(1)
    expect(Visitor.first.user).to eq(user)
  end

  it "should save more visits of the same user" do
    pub = FactoryGirl.create(:pub)
    user = FactoryGirl.create(:user)
    sign_in user

    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: false
    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: true
    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: true

    expect(Visitor.count).to eq(3)
  end

  it "should save an unconfirmed visit" do
    pub = FactoryGirl.create(:pub)

    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: false

    expect(Visitor.count).to eq(1)
  end

  it "should redirect to session url given a not confirmed visit" do
    pub = FactoryGirl.create(:pub)
    session[:return_to] = "/redirect-path"

    post :geolocated_visit_pub, pub_id: pub.id, longitude: 1.234, latitude: -1.234, confirmed: false

    expect(response).to redirect_to('/redirect-path')
  end
end