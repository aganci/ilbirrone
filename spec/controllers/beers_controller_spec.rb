require 'rails_helper'

describe BeersController do
  render_views
  let(:user) { FactoryGirl.create(:user) }
  let(:pub) { FactoryGirl.create(:pub) }
  
  it "should redirect to sign_in page" do
    get :new, pub_id: pub.id

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to pub page" do
    sign_in user
    post :create, beer_attributes

    expect(response).to redirect_to(pub_path(pub))
  end

  it "should save notification" do
    sign_in user

    expect { post :create, beer_attributes }.to change(Notification, :count).by(1)
  end

  def beer_attributes
    { beer: {name: "a beer name", alcohol_by_volume: '4', color: 'a color', pub_id: pub.id, user_id: user.id} }
  end


  it "should render delete cover button" do
    sign_in user
    beer = FactoryGirl.build(:beer, id: 1)
    beer.comments << FactoryGirl.build(:cover, id: 1, user: user)
    allow(BeerQuery).to receive(:find).with("1").and_return(beer)

    get :show, id: "1"

    expect(response).to render_template('_delete_cover')
  end
end