require 'rails_helper'

describe Admin::PubImagesController, type: :controller do
  before do
    @pub = FactoryGirl.create(:pub)
  end

  it "should redirect to login" do
    get :index, pub_id: @pub.id

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to home page for non admin user" do
    sign_in FactoryGirl.create(:user)

    get :index, pub_id: @pub.id

    expect(response).to redirect_to(root_path)
  end
end
