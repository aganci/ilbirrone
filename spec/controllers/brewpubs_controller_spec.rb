require 'rails_helper'

describe BrewpubsController do
  render_views
  
  before do
    sign_in(FactoryGirl.create(:user))
  end

  it "should assign presenter for brewpub action" do
    get :index, provincia: 'Milano', show: 'map'

    expect(controller).to render_template('pubs/map')
  end
end