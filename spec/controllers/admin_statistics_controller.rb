require 'rails_helper'

describe Admin::StatisticsController do
  describe "should limit access to registered user" do
    before do
      get :index
    end

    specify { expect(response).to redirect_to(new_user_session_path)  }
  end
end