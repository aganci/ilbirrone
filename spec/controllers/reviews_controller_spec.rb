require 'rails_helper'

describe ReviewsController do
  let(:pub)  { FactoryGirl.create(:pub) }

  it "should redirect to sign in page" do
    get :new, pub_id: pub.id

    expect(response).to redirect_to(new_user_session_path) 
  end

  it "should create a visit" do
    user = FactoryGirl.create(:user)
    sign_in user

    post :create, review: FactoryGirl.attributes_for(:review, pub_id: pub.id, user_id: user.id)

    expect(pub.visitor?(user)).to eq(true)
  end
  
  it "should create a notification" do
    user = FactoryGirl.create(:user)
    sign_in user

    expect(Notification).to receive(:notify_pub_review)
    
    post :create, review: FactoryGirl.attributes_for(:review, pub_id: pub.id, user_id: user.id)
  end

  it "should add visit only once" do
    user = FactoryGirl.create(:user)
    sign_in user

    post :create, review: FactoryGirl.attributes_for(:review, pub_id: pub.id, user_id: user.id)
    post :create, review: FactoryGirl.attributes_for(:review, pub_id: pub.id, user_id: user.id)

    expect(Visitor.count).to eq(1)
  end
end
