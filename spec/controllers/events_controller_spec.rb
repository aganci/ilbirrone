require 'rails_helper'

describe EventsController do
  render_views

  it "should render social links" do
    event = FactoryGirl.build(:public_event, id: 1234)
    allow(Event).to receive(:find).with("1234").and_return(event)

    get :show, id: 1234

    expect(response).to render_template(partial: 'shared/_social')
  end

  it "should render delete cover button" do
    user = FactoryGirl.create(:user)
    sign_in user

    event = FactoryGirl.build(:public_event, id: 1)
    event.comments << FactoryGirl.build(:cover, id: 1, user: user)
    allow(Event).to receive(:find).with("1").and_return(event)

    get :show, id: "1"

    expect(response).to render_template('_delete_cover')
  end
end