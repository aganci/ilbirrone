require 'rails_helper'

describe GeolocationController do
  it "should render near pubs template" do
    pub = FactoryGirl.build(:pub, id: 1234)
    stub_near_pubs([pub])

    get :nearby, latitude: 1.234, longitude: -1.234

    expect(response).to render_template('geolocation/_pubs_with_directions')
  end
  
  it "should render near pubs in map" do
    get :nearby, show: 'map', latitude: 1.234, longitude: -1.234

    expect(response).to render_template('geolocation/_nearby_map')
  end

  it "should render near pubs" do
    get :nearby

    expect(response).to render_template('nearby')
  end

  it "should render in json format" do
    stub_near_pubs([])
    
    get :nearby, :format => 'json', latitude: 1.234, longitude: -1.234

    expect(ActiveSupport::JSON.decode(response.body)).to eq([])
  end

  it "should include pub informations in json" do
    first = FactoryGirl.build(:pub, id: 1, name: 'first')
    second = FactoryGirl.build(:pub, id: 2, name: 'second')

    stub_near_pubs([first, second])
    
    get :nearby, :format => 'json', latitude: 1.234, longitude: -1.234

    expect(ActiveSupport::JSON.decode(response.body).map {|p| p['name']}).to eq(['first', 'second'])
    expect(ActiveSupport::JSON.decode(response.body).map {|p| p['path']}).to eq(['http://www.beerky.it/birrerie/1', 'http://www.beerky.it/birrerie/2'])
  end
  
  it "should include pub coordinates in json" do
    pub = FactoryGirl.build(:pub, id: 1, latitude: 100, longitude: 200)

    stub_near_pubs([pub])
    
    get :nearby, :format => 'json', latitude: 1.234, longitude: -1.234

    pub_json = ActiveSupport::JSON.decode(response.body).first
    expect(pub_json['latitude']).to eq(100)
    expect(pub_json['longitude']).to eq(200)
  end

  describe "#nearest_pub" do

    it "should render nearest pub" do
      pub = FactoryGirl.build(:pub, name: 'Baladin', city: 'Milano')
      allow(PubQuery).to receive(:nearest).with(1.234, -1.234).and_return(pub)

      get :nearest_pub, latitude: 1.234, longitude: -1.234

      expect(assigns(:pub)).to eq(pub)
      expect(assigns(:latitude)).to eq(1.234)
      expect(assigns(:longitude)).to eq(-1.234)
    end

    it "should render nothing without nearest pub" do
      allow(PubQuery).to receive(:nearest).and_return(nil)

      get :nearest_pub, latitude: 1.234, longitude: -1.234

      expect(response.body).to eq(" ")
    end

  end

  def stub_near_pubs(value)
    allow(PubQuery).to receive(:near).with(1.234, -1.234, 50, 50).and_return(value)
  end

end