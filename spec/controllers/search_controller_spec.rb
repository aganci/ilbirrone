require 'rails_helper'

describe SearchController do
  it "should redirect to home page for too short search" do
    get :index, text: 'ab'

    expect(response).to redirect_to(root_path)
  end
  
  it "should redirect to home page when no search text is specified" do
    get :index

    expect(response).to redirect_to(root_path)
  end

  it "should list pubs with new review" do
    pub = FactoryGirl.create(:pub, name: 'Baladin', city: 'Milano')

    post :autocomplete_pub_new_review, query: 'baladin'

    expect(YAML::load(response.body)).to eq([["Birreria - Baladin - Milano", new_review_path(pub_id: pub.id)]])
  end

  it "should list cities with province url" do
    post :autocomplete, query: 'milano'

    expect(JSON.parse(response.body)).to eq([["Milano, Lombardia", pubs_path(provincia: "Milano")]])
  end
end