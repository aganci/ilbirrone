require 'rails_helper'

describe Admin::BeerImagesController, type: :controller do
  before do
    @beer = FactoryGirl.create(:beer)
  end

  it "should redirect to login" do
    get :index, beer_id: @beer.id

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to home page for non admin user" do
    sign_in FactoryGirl.create(:user)

    get :index, beer_id: @beer.id

    expect(response).to redirect_to(root_path)
  end
end
