require 'rails_helper'

describe HomePageController do
  render_views

  it "should render recent events" do
    FactoryGirl.create(:public_event)

    get :show

    expect(response).to render_template(partial: '_recent_event')
  end

  it "should render recent images" do
    get :show

    expect(response).to render_template(partial: '_recent_images')
  end

  it "should render notifications" do
    get :show

    expect(response).to render_template(partial: "notifications/_notifications")    
  end

  it "should render analytics on all pages" do
    allow(Rails).to receive(:env).and_return(double(:production? => true, :test? => nil))

    get :show

    expect(response).to render_template('shared/_analytics')
  end  

  it "should render remarketing on all pages" do
    allow(Rails).to receive(:env).and_return(double(:production? => true, :test? => nil))

    get :show

    expect(response).to render_template('shared/_remarketing')
  end

  it "should assign recent pubs" do
    pubs = [FactoryGirl.build(:pub, id: 1234)]
    allow(PubQuery).to receive(:recents_with_cover).with(6).and_return(pubs)

    get :show

    expect(assigns(:recent_pubs)).to eq(pubs)
  end

  it "should render recent pubs" do
    pubs = [FactoryGirl.build(:pub, id: 1234), FactoryGirl.build(:pub, id: 4567)]
    allow(PubQuery).to receive(:recents_with_cover).with(6).and_return(pubs)

    get :show

    expect(response).to render_template(partial: '_pub', count: 2)
  end

  it "should render cloud" do
    get :show

    expect(response).to render_template(partial: 'pubs/_cloud', count: 1)
  end

  it "should render world cloud" do
    get :show

    expect(response).to render_template(partial: 'pubs/_world_cloud', count: 1)
  end
end