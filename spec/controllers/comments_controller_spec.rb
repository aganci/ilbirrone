require 'rails_helper'

describe CommentsController do

  it "should redirect to sign in" do
    post :destroy, id: 1234

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should raise error for destroy of comment of another user" do
    comment = FactoryGirl.create(:comment, user: FactoryGirl.create(:user))
    sign_in FactoryGirl.create(:user)

    expect { post :destroy, id: comment.id }.to raise_error
  end

  it "should delete comment given the user that creates it" do
    user = FactoryGirl.create(:user)
    sign_in user
    event = FactoryGirl.create(:public_event)
    comment = FactoryGirl.create(:comment, user: user, commentable: event)

    expect { post :destroy, id: comment.id }.to change(Comment, :count).by(-1)

    expect(response).to redirect_to(event_path(event))
  end

  it "should delete comment given an admin user" do
    comment = FactoryGirl.create(:comment, user: FactoryGirl.create(:user))
    sign_in FactoryGirl.create(:admin)

    expect { post :destroy, id: comment.id }.to change(Comment, :count).by(-1)
  end

  it "should create a comment" do
    user_a = FactoryGirl.create(:user)
    sign_in user_a
    user_b = FactoryGirl.create(:user)

    post :upload_user_comment, id: user_b.id, user_comment_content: 'a comment for user_b'

    expect(Comment.last.commentable_type).to eq("User")
  end

  it "should notify pub comment" do
    sign_in FactoryGirl.create(:user)
    pub = FactoryGirl.create(:pub)

    expect { post :upload_comment, id: pub.id, comment_content: "a pub comment" }.to change(Notification, :count).by(1)
  end


  it "should notify a user comment" do
    user_a = FactoryGirl.create(:user)
    sign_in user_a
    user_b = FactoryGirl.create(:user)

    expect { post :upload_user_comment, id: user_b.id, user_comment_content: 'a comment for user_b' }.to change(Notification, :count).by(1)

    expect(Comment.last.commentable_type).to eq("User")
  end

end