require 'rails_helper'

describe PubsController do
  render_views
  
  it "should redirect to login page for not logged user" do
    get :new

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should render add cover button" do
    allow(PubQuery).to receive(:find).with("1234").and_return(FactoryGirl.build(:pub, id: 1234))

    get :show, id: "1234"

    expect(response).to render_template('comments/_add_cover_login_button')
  end


  describe "signed in user" do

    before do
      @user = FactoryGirl.create(:user)
      sign_in(@user)
    end

    it "should redirect to pub page" do
      post :create, pub: {name: "a pub", address: 'an address', city: "a city", province: "a province", country: "a country"}
      expect(Pub.count).to eq(1)
      pub = Pub.all.first

      expect(response).to redirect_to(pub_path(pub))
    end

    it "should geocode pub" do
      post :create, pub: {name: "a pub", address: 'viale monza, 15', city: "milano", province: "milano", country: "italia"}
      
      pub = Pub.all.first

      expect(pub.latitude).to_not be_nil
      expect(pub.longitude).to_not be_nil
    end

    it "should redirect to pubs in province" do
      get :search, pub: {location: 'Milano'}

      expect(response).to redirect_to(pubs_path(provincia: 'Milano'))
      expect(response.code).to eq("301")
    end

    it "should render microdata" do
      pub = FactoryGirl.create(:pub)
      
      get :show, id: pub.id

      expect(response).to render_template(partial: '_pub_microdata')
    end

    it "should assign presenter for index action" do
      get :index, provincia: 'Milano', show: 'map'

      expect(controller).to render_template('pubs/map')
    end

    it "should render delete cover button" do
      pub = FactoryGirl.build(:pub, id: 1)
      pub.comments << FactoryGirl.build(:cover, id: 1, user: @user)
      allow(PubQuery).to receive(:find).with("1").and_return(pub)

      get :show, id: "1"

      expect(response).to render_template('_delete_cover')
    end
  end
end