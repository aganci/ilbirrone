require 'rails_helper'

describe SearchController do 
  it "should list cities" do
    post :autocomplete, query: "milano"

    result = JSON.parse(response.body)

    expect(result).to eq([["Milano, Lombardia", pubs_path(provincia: "Milano")]])
  end 

  it "should list beers" do
    pub = FactoryGirl.create(:pub, name: 'pub name')
    beer = FactoryGirl.create(:beer, name: 'La bionda', pub: pub)

    post :autocomplete, query: "bionda"

    result = JSON.parse(response.body)

    expect(result).to eq([["Birra - La bionda - pub name", beer_path(beer)]])
  end

  it "should list pubs" do
    pub = FactoryGirl.create(:pub, name: 'pub name', pub_type: 'Birreria', city: 'Milano')

    post :autocomplete, query: "pub"

    result = JSON.parse(response.body)

    expect(result).to eq([["Birreria - pub name - Milano", pub_path(pub)]])
  end
end