require 'rails_helper'

describe ChangesController do
  before do
    allow(Captcha).to receive(:valid?).and_return(true)
  end

  it "should redirect to pub page" do
    pub = FactoryGirl.create(:pub)

    post :send_pub_changes, pub_changes: { pub_id: pub.id, note: '' }

    expect(response).to redirect_to(pub_path(pub))
  end
end
