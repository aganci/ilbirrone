require 'rails_helper'

describe Admin::PubsController, type: :controller do
  it "should redirect to login" do
    get :index

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to home page for non admin user" do
    sign_in FactoryGirl.create(:user)

    get :index

    expect(response).to redirect_to(root_path)
  end

  describe "geocoding address" do
    let(:map) { double }
    before do
      sign_in FactoryGirl.create(:admin)
      stub_const("Map", map)
    end

    describe "on create" do
      let(:pub) {mock_model(Pub, { :approve! => true, :user= => nil}) }
      
      before do
        allow(Pub).to receive(:new) {pub}
        allow(Notification).to receive(:notify_pub_approval)
      end

      it "should call map geocode" do
        expect(map).to receive(:geocode).with(pub)

        put :create, pub: {}
      end
    end
  end

  describe 'approval notification' do
    before do
      sign_in FactoryGirl.create(:admin)
    end

    it "should notify the approval" do
      pub = FactoryGirl.build(:pub, id: '1234')
      allow(Pub).to receive(:find).with('1234').and_return(pub)

      expect(Notification).to receive(:notify_pub_approval).with(pub)

      post :approve, {id: '1234'}
    end

    it "should notify approval after saving" do
      expect(Notification).to receive(:notify_pub_approval)
      
      post :create, pub: {name: "a pub", address: 'an address', city: "a city", province: "a province", country: "a country"}
    end
  end  
end
