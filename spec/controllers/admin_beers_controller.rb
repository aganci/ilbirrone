require 'rails_helper'

describe Admin::BeersController, type: :controller do
  it "should redirect to login" do
    get :index

    expect(response).to redirect_to(new_user_session_path)
  end

  it "should redirect to home page for non admin user" do
    sign_in FactoryGirl.create(:user)

    get :index

    expect(response).to redirect_to(root_path)
  end
end
