class NotificationsMailer < ActionMailer::Base
  include ApplicationHelper

  
  def contact_us(message)
    @message = message
    mail(from: "Form contattaci <ilbirrone@gmail.com>", to: "ilbirrone@gmail.com", subject: message.subject)
  end

  def pub_approved(pub)
    @pub = pub
    mail(from: 'info@ilbirrone.it', subject: t('pubs.approve.email_subject', name: pub.name), to: pub.user.email)
  end

  def invite_homebrewer(invitation)
    @user = invitation.friend
    email = mail(from: 'info@ilbirrone.it', 
      subject: "#{invitation.friend.username} ti ha invitato al nuovo social network degli homebrewer italiani", 
      to: invitation.email)
  end

  def welcome_new_user(user)
    @user = user
    attachments.inline['logo-email.png'] = File.read(Rails.root.join('app', 'assets', 'images', 'logo-email.png'))
    mail(from: 'info@ilbirrone.it', subject: "Benvenuto su #{t('site_name')}!", to: user.email)
  end

  def pub_changes(pub, pub_changes, user)
    @pub = pub
    @pub_changes = PubChangesPresenter.new(pub_changes, self)
    @user = user
    @differences = @pub_changes.differences_from(pub, user)
    mail(from: "Form modifica #{pub.name} <ilbirrone@gmail.com>", to: "ilbirrone@gmail.com", 
      subject: "Correzione informazioni per #{pub.name}")
  end
end
