class HomebrewersController < ApplicationController
  def pre_registration
    @pre_registration = PreRegistration.new
    @invitation = Invitation.new
  end

  def invite
    @invitation = Invitation.new(params[:invitation])
    @invitation.friend = current_user

    if @invitation.valid?
      NotificationsMailer.invite_homebrewer(@invitation).deliver
      redirect_to pre_registration_homebrewers_path, flash: {success: "Abbiamo inviato una mail di invito all'indirizzo #{@invitation.email}"}
    else
      @pre_registration = PreRegistration.new
      render 'pre_registration'
    end
  end
end