class VisitorsController < ApplicationController
  before_filter :authenticate_user!, only: [:visit_pub]
  
  def visit_pub
    pub = Pub.find(params[:pub_id])
    pub.visitors.create(user: current_user) if !pub.visitor?(current_user)

    redirect_to pub
  end

  def geolocated_visit_pub
    pub = Pub.find(params[:pub_id])
    pub.visitors.create(latitude: params[:latitude], longitude: params[:longitude], user: current_user, confirmed: params[:confirmed])
    if (params[:confirmed] == 'true')
      redirect_to pub
    else
      redirect_to (session.delete(:return_to) || root_path)
    end
  end
end