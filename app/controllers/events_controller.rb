class EventsController < ApplicationController
  def index
    @events = EventQuery.new.all
  end

  def show
    @event = Event.find(params[:id])
  end

  def select_type
  end
end