require 'map'

class PublicEventsController < EventsController
  before_filter :authenticate_user!
  
  def new
    @event = PublicEvent.new(user: current_user)
  end

  def create
    @event = PublicEvent.new(params[:public_event])
    Map.geocode(@event)
    if @event.save
      Notification.notify_event(@event)
      redirect_to event_path(@event), :notice => "Complimenti! Hai inserito l'evento #{@event.title}"
    else
      render 'new'
    end
  end
end