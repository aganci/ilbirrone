class GeolocationController < ApplicationController
  def nearby
    if (params[:latitude].nil? || params[:longitude].nil?)
      return render 'nearby'
    end

    pubs = PubQuery.near(latitude, longitude, 50, 50)     
    
    respond_to do |format|
      format.html do 
        render(partial: nearby_template, 
          locals: {pubs: pubs, latitude: latitude, longitude: longitude}, 
          layout:false)
      end

      format.json do
        render json: pubs.map {|p| GeolocatedPub.new(p, latitude, longitude).to_hash(self)}
      end

    end
  end

  def nearest_pub
    @pub = PubQuery.nearest(latitude, longitude)
    @latitude = latitude
    @longitude = longitude

    if @pub.present?
      render 'nearest_pub', layout: false
    else
      render nothing: true
    end
  end

  private
    def latitude
      params[:latitude].to_f
    end

    def longitude
      params[:longitude].to_f
    end

    def nearby_template
      return 'nearby_map' if params[:show] == 'map'
      'pubs_with_directions'
    end
end