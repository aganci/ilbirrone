class RegistrationsController < Devise::RegistrationsController
  
  def create
    super
    if (@user.valid?)
      Notification.notify_user_registration(@user)
    end
  end


  def update
    account_update_params = params[:user]

    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end

  protected
  def after_update_path_for(resource)
    user_path(resource)
  end
end