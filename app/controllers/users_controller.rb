class UsersController < ApplicationController
  def show
    @user = User.preload_data(params[:id])
  end

  def index
    @users = load_users
  end

  def search
    render(partial: 'users', locals: {users: load_users}, layout: false)
  end

  private
  def load_users
    User.rankings(params[:search], :page => params[:page], :per_page => 30)
  end
end