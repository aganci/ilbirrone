class NotificationsController < ApplicationController
  def index
    @notifications = Notification.paginated_timeline(params[:page])
  end
end