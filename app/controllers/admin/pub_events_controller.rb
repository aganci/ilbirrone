module Admin
  class PubEventsController < AdminController
    def edit
      @event = Event.find(params[:id])
    end
    
    def update
      @event = Event.find(params[:id])
      if @event.update_attributes(params[:pub_event])
        flash[:success] = "Evento #{@event.title} aggiornata"
        redirect_to admin_events_path
      else
        render 'edit'
      end      
    end

  end
end