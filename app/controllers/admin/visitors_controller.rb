module Admin
  class VisitorsController < AdminController
    def index
      @visitors = Visitor.paginate(:page => params[:page], :per_page => 50).includes(:pub).includes(:user).order(created_at: :desc)
    end
  end
end