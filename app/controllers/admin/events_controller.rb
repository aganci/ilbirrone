module Admin
  class EventsController < AdminController
    def index
      @events = Event.includes(:pub).order(created_at: :desc)
    end

    def destroy
      event = Event.find(params[:id])
      event.destroy
      redirect_to admin_events_path, flash: {success: "Hai eliminato l'evento #{event.title}"}
    end
  end
end