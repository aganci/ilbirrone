module Admin
  class UsersController < AdminController
    def index
      @users = User.order(current_sign_in_at: :desc)
    end
  end
end