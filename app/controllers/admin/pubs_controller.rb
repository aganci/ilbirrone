require 'map'

module Admin
  class PubsController < AdminController
    
    def index
      @pubs = Pub.paginate(:page => params[:page], :per_page => 50).order(created_at: :desc)
    end

    def show
      @pub = Pub.find(params[:id])
    end
    
    def new
      @pub = Pub.new
    end

    def edit
      @pub = Pub.find(params[:id])
    end

    def edit_location
      @pub = Pub.find(params[:id])
    end

    def update
      @pub = Pub.find(params[:id])
      if @pub.update_attributes(params[:pub])
        flash[:success] = "Birreria #{@pub.name} aggiornata"
        redirect_to admin_pub_path(@pub)
      else
        render 'edit'
      end      
    end

    def create
      @pub = Pub.new(params[:pub])
      @pub.user = current_user 
      Map.geocode(@pub)
      if @pub.approve!
        Notification.notify_pub_approval(@pub)
        redirect_to admin_pubs_path
      else
        render 'new'
      end
    end 


    def destroy
      @pub = Pub.find(params[:id])
      @pub.destroy
      redirect_to admin_pubs_path, flash: {success: "Hai eliminato #{@pub.name}"}
    end

    def approve
      @pub = Pub.find(params[:id])
      @pub.approve!
      Notification.notify_pub_approval(@pub)
      redirect_to admin_pubs_path, flash: {success: "Hai approvato #{@pub.name}"}
    end
  end
end