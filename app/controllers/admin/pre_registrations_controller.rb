module Admin
  class PreRegistrationsController < AdminController
    def index
      @pre_registrations = PreRegistration.order(created_at: :desc)
    end

    def destroy
      pre_registration = PreRegistration.find(params[:id])
      pre_registration.destroy
      redirect_to admin_pre_registrations_path, flash: {success: "Hai eliminato la preiscrizione di #{pre_registration.user.username}"}
    end
  end
end