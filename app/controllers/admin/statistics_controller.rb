module Admin
  class StatisticsController < AdminController
    def index
      @user_count = User.count
      @review_count = Review.count
      @beer_count = Beer.count
      @pub_count = Pub.count
      @beer_review_count = BeerReview.count
      @event_count = Event.count
      @comment_count = Comment.count
      @pre_registration_count = PreRegistration.count
      @visitor_count = Visitor.count
    end
  end
end