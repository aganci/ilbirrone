module Admin
  class CommentsController < AdminController
    def index
      @comments = Comment.paginate(:page => params[:page], :per_page => 50).order(created_at: :desc)
    end

    def destroy
      comment = Comment.find(params[:id])
      content = comment.content
      comment.destroy
      redirect_to admin_comments_path, flash: {success: "Hai eliminato #{content}"}
    end
  end
end