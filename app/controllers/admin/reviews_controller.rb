module Admin
  class ReviewsController < AdminController
    def index
      @reviews = Review.includes(:user).includes(:pub).order(created_at: :desc)
    end

    def destroy
      review = Review.find(params[:id])
      review.destroy
      redirect_to admin_reviews_path, flash: {success: "Hai eliminato la recensione di #{review.user.username}"}
    end

    def edit
      @review = Review.find(params[:id])
    end

    def update
      @review = Review.find(params[:id])
      if @review.update_attributes(params[:review])
        flash[:success] = "Recensione per #{@review.pub.name} aggiornata"
        redirect_to admin_reviews_path
      else
        render 'edit'
      end      
    end

  end
end