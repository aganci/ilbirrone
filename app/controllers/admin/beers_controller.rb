module Admin
  class BeersController < AdminController
    def index
      @beers = Beer.paginate(:page => params[:page], :per_page => 50).order(created_at: :desc)
    end

    def destroy
      @beer = Beer.find(params[:id])
      @beer.destroy
      redirect_to admin_pub_path(@beer.pub), flash: {success: "Hai eliminato #{@beer.name}"}
    end

    def edit
      @beer = Beer.find(params[:id])
    end

    def update
      @beer = Beer.find(params[:id])
      if @beer.update_attributes(params[:beer])
        flash[:success] = "Birra #{@beer.name} aggiornata"
        redirect_to admin_pub_path(@beer.pub)
      else
        render 'edit'
      end      
    end
  end
end