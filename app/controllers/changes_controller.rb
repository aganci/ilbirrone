class ChangesController < ApplicationController
  def pub
    @pub = Pub.find(params[:id])
    @changes = PubChanges.from(@pub, current_user)
  end

  def send_pub_changes
    @changes = PubChanges.new(params[:pub_changes])
    @pub = Pub.find(@changes.pub_id)
    
    if Captcha.valid?(params)
      NotificationsMailer.pub_changes(@pub, @changes, current_user).deliver
      redirect_to pub_path(@pub), 
        flash: { success: t('helpers.flashes.send_pub_changes.success') } 
    else
      flash[:error] = "Clicca su Non sono un robot"
      render :pub
    end
  end 
end