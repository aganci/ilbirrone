class BeerPagesController < ApplicationController
  def whatis
  end

  def craft_beer
  end

  def history
  end

  def oktoberfest
    @pub = Pub.find('hofbrauhaus-genova')
  end
end