class PubEventsController < EventsController
  before_filter :authenticate_user!
  
  def new
    pub = Pub.find(params[:pub_id])
    @event = pub.events.build(user: current_user)
  end

  def create
    @event = PubEvent.new(params[:pub_event])
    if @event.save
      Notification.notify_event(@event)
      redirect_to event_path(@event), :notice => "Complimenti! Hai inserito l'evento #{@event.title}"
    else
      render 'new'
    end
  end
end