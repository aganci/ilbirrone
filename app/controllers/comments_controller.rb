class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def destroy
    comment = Comment.find(params[:id])
    raise "Non hai i permessi per cancellare il commento" if !can_delete?(comment)
    comment.destroy
    redirect_to comment.commentable, flash: {success: "Il commento è stato eliminato"}
  end

  def upload
    commentable = params[:type].constantize.find(params[:id])
    CommentRegistration.upload_cover(commentable, params[:cover_file], current_user)
    redirect_to commentable 
  end

  def upload_avatar
    commentable = User.find(params[:id])
    CommentRegistration.upload_avatar(commentable, params[:cover_file], current_user)
    redirect_to edit_user_registration_path(current_user), notice: "L'immagine del profilo è stata aggiornata correttamente." 
  end

  def upload_comment
    pub = PubQuery.find(params[:id])
    
    if CommentRegistration.upload_comment(pub, params[:comment_content], params[:comment_image_file], current_user)
      redirect_to pub 
    else
      redirect_to pub, alert: "Errore nell'inserimento del commento. Scrivi il contenuto o carica un'immagine" 
    end
  end

  def upload_user_comment
    user = User.find(params[:id])

    if CommentRegistration.upload_user_comment(user, params[:user_comment_content], params[:user_comment_image_file], current_user)
      redirect_to user
    else
      redirect_to user, alert: "Errore nell'inserimento del commento. Scrivi il contenuto o carica un'immagine" 
    end
  end

  private
  def can_delete?(comment)
    CommentPermissions.new(comment).can_delete?(current_user)
  end
end
