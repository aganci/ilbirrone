class HomePageController < ApplicationController
  def show
    @recent_events = EventQuery.new.first(3)
    @popular_pubs = Ranking.popular_pubs
    @recent_pubs = PubQuery.recents_with_cover(6)
    @notifications = Notification.timeline(10)
  end
end
