class SearchController < ApplicationController

  def index
    if (search_text.nil? || search_text.size < 3)
      redirect_to root_path, flash: { error: t('search.index.invalid_search') }
      return
    end

    @pubs = PubQuery.new.search(search_text)
    @beers = BeerQuery.new.search(search_text)
    @search_text = search_text 
  end

  def autocomplete
    render json: ( cities(params[:query]) + beers(params[:query]) + pubs(params[:query]) ).to_json
  end
  
  def autocomplete_pub_new_event
    render json: (pubs_with_new_event_path(params[:query])).to_json
  end

  def autocomplete_pub_new_review
    render json: (pubs_with_new_review_path(params[:query])).to_json
  end

  def cities(query)
    Place.where_city(query).map {|city| [ view_context.format_city(city), pubs_path(provincia: city[0])]}
  end

  def beers(query)
    BeerQuery.new.search_first(query, 20).map {|beer| ["Birra - #{beer.name} - #{beer.pub.name}", beer_path(beer)]}
  end

  def pubs(query)
    PubQuery.new.search_first(query, 20).map {|pub| ["#{pub.pub_type} - #{pub.name} - #{pub.city}", pub_path(pub)]}
  end

  def pubs_with_new_event_path(query)
    PubQuery.new.search_first(query, 20).map {|pub| ["#{pub.pub_type} - #{pub.name} - #{pub.city}", new_pub_event_path(pub_id: pub.id)]}
  end
  
  def pubs_with_new_review_path(query)
    PubQuery.new.search_first(query, 20).map {|pub| ["#{pub.pub_type} - #{pub.name} - #{pub.city}", new_review_path(pub_id: pub.id)]}
  end

  def search_text
    params[:text]
  end
end