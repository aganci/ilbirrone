class BrewpubsController < ApplicationController
  def index
    if (params[:provincia].present?)
      @province = params[:provincia]
      if (params[:show].present?)
        pubs = PubQuery.in_province_of_with_coordinates(@province, ['Brewpub'])
        presenter = BrewpubsMapPresenter.new(@province)
        render 'pubs/map', locals: { presenter: presenter, pubs: pubs }
      else
        @pubs = PubQuery.in_province_of(@province, ['Brewpub'])
        render 'province'
      end
    else
      @pubs = PubQuery.recents_with_cover(15, ['Brewpub'])
    end
  end

  def recent_reviews
    @pubs = PubQuery.new.recents_with_review(15, ['Brewpub'])
  end
end