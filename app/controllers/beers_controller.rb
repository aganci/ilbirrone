class BeersController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create]

  def index
    if (params[:tipologia].present?)
      @beer_type = params[:tipologia]
      @beers = BeerQuery.new.find_by_type(@beer_type)
      render 'beers_by_type'
    else
      @beers = BeerQuery.most_recent_with_photo(20)
    end
  end

  def new
    @beer = Pub.find(params[:pub_id]).beers.build(user: current_user)
  end

  def create
    @beer = Beer.new(params[:beer])
    if @beer.save
      Notification.notify_beer(@beer)
      redirect_to pub_path(@beer.pub)
    else
      render 'new'
    end
  end

  def show
    @beer = BeerQuery.find(params[:id])
  end
end 