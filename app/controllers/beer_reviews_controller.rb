class BeerReviewsController < ApplicationController
  before_filter :authenticate_user!
  
  def new
    @review = Beer.find(params[:beer_id]).reviews.build(user: current_user)
    @items = SensoryAnalysis.items(self)
  end

  def create
    @review = BeerReview.new(params[:beer_review])
    if @review.save
      Notification.notify_beer_review(@review)
      redirect_to @review.beer
    else
      render 'new'
    end
  end
end