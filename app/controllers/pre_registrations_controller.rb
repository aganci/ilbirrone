class PreRegistrationsController < ApplicationController
  def create
    @pre_registration = PreRegistration.new(params[:pre_registration])
    @pre_registration.save
    redirect_to pre_registration_homebrewers_path, 
      flash: {success: "Grazie per esserti preiscritto! Ti invieremo un'email all'indirizzo #{current_user.email} appena attiveremo il servizio dedicato agli homebrewers!"}
  end
end