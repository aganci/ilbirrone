class ApplicationController < ActionController::Base
  protect_from_forgery
  
  include RedirectToCurrentPageAfterSignInHelper

  before_action :set_locale, :store_location
 
  def set_locale
    I18n.locale = 'it'
  end

  def authenticate_admin!
    authenticate_user!
    unless current_user.admin?
      redirect_to root_path, flash: { error: t('errors.user_is_not_admin') }
    end
  end

 before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me, :city, :privacy_policy_accepted) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password, :city) }
  end  
end
