require 'pub_query'
require 'map'

class PubsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create]

  def new
    @pub = Pub.new
    @pub.pub_type = params[:pub_type] || 'Birreria'
  end

  def create
    @pub = Pub.new(params[:pub])
    @pub.user = current_user
    Map.geocode(@pub)

    if @pub.submit!
      redirect_to pub_path(@pub), flash: {success: t('pubs.create.submitted')}
    else
      render 'new'
    end
  end 

  def show
    @pub = PubQuery.find(params[:id])
  end

  def search
    redirect_to pubs_path(provincia: params[:pub][:location]), status: 301
  end

  def index
    IndexPubsHandler.execute(self, params[:provincia], params[:show])
  end

  def microbreweries
    if params[:region].present?
      @microbreweries = PubQuery.new.microbreweries(params[:region])
    else
      @microbreweries = PubQuery.new.microbreweries_by_region
    end
  end

  def curious
    @pubs = PubQuery.curious
  end

  def recent_reviews
    @pubs = PubQuery.new.recents_with_review(15, ['Brewpub', 'Birreria'])
  end

  def webmaster
    @pub = Pub.find(params[:id])
  end

  def select_for_review
  end

  def provinces
    @provinces = PubQuery.new.provinces_with_count(['Brewpub', 'Birreria'])
  end
end
