class ReviewsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create]

  def new
    @review = Pub.find(params[:pub_id]).reviews.build(user: current_user, frequenter: false)
  end

  def create
    @review = Review.new(params[:review])
    if @review.save
      Notification.notify_pub_review(@review)
      Visitor.find_or_create_by(current_user, @review.pub)
      redirect_to @review.pub
    else
      render 'new'
    end
  end

  def guide
  end
end