require 'rating_view'

class RatingInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    rating_value_field = Nokogiri::HTML::fragment(@builder.hidden_field(attribute_name))
    target_id = rating_value_field.children.first[:id]
    value = rating_value_field.children.first[:value]
    rating_id = target_id + '_rating'

    result = ''
    result << rating_stars_field(rating_id)
    result << rating_value_field.to_html
    result << RatingView.new(template).render_script(rating_id, target_id, value)
    result.html_safe
  end

  private
  def rating_stars_field(id)
    "<br><span id='#{id}'></span>"
  end
end