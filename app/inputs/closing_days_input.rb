class ClosingDaysInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    value_field = Nokogiri::HTML::fragment(@builder.hidden_field(attribute_name))
    target_id = value_field.children.first[:id]
    days_group_id = target_id + "_options"
    
    result = value_field.to_html + "<br>"
    result << "<div id=\"#{days_group_id}\" class=\"btn-group\" data-toggle=\"buttons-checkbox\">\n"
    index = 0
    I18n.t('date.abbr_day_names').each do |day|
      result << "  <button type=\"button\" class=\"btn btn-default\" value=\"#{index}\">#{day}</button>\n"
      index += 1
    end
    result << "</div>"
    result << javascript(target_id, days_group_id)
    result
  end

  private
  def javascript(target_id, options_id)
    template.javascript_tag do
      template.raw(set_current_value(target_id) + click_event(target_id, options_id))
    end
  end

  def set_current_value(target_id)
<<-SCRIPT
days = $('##{target_id}').val().split(',');
$.each(days, function(index, value) {
  $('button[value=\\'' + value + '\\']').button('toggle');
  });
SCRIPT
  end

  def click_event(target_id, options_id)
<<-SCRIPT
$('##{options_id}').click(function() {
  that = $(this);
  var actives = [];
  var wait = setTimeout(function() {
    that.find('.active').each( function() {
      actives.push($(this).attr('value'));     
      });
      $('##{target_id}').val(actives);
    }, 10);
  });
SCRIPT
  end
end