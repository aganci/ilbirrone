function parsePlace(place) {
  var place = place.split(", ");

  if (place.length == 3) {
    return {
      city: place[0],
      province: place[1],
      region: place[2]
    };
  }

  if (place.length == 2) {
    return {
      city: place[0],
      province: place[0],
      region: place[1]
    };
  }
}

function autocompletePlace(selector, values, updaterFunction) {
  $(selector).typeahead({
    source: function (query, process) {
      return values;
    },
    minLength: 2,
    items: 20,
    updater: updaterFunction
  });
}


function autocompletePubPlace(values) {
  $('#pub_city').typeahead({
    source: function (query, process) {
        if ($('#pub_country').val() == 'Italia') {
            return values;
          };
        return [];
    },
    minLength: 2,
    items: 20,
    updater: function (item) {
      place = parsePlace(item);

      $("#pub_province").val(place.province);
      $("#pub_region").val(place.region);
      $("#pub_postcode").focus();
      return place.city;
    }
  });
}
