function geolocate(succeededGeolocation, failedGeolocation, unsupportedGeolocation) {
  var geolocation_provider = window.geolocation_provider || navigator.geolocation
  
  if(geolocation_provider) {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    geolocation_provider.getCurrentPosition(succeededGeolocation, failedGeolocation, options);
  } else {
    unsupportedGeolocation();
  }
}