class UserCommentPresenter
  def initialize(user, view)
    @user = user
    @view = view
  end

  def render_new_form
    return @view.render(partial: 'comments/new_user_comment', locals: {user: @user, message: 'A cosa stai pensando?'}) if @view.current_user == @user
    return @view.render(partial: 'comments/new_user_comment', locals: {user: @user, message: "Lascia un messaggio per #{@user.username}"}) if @view.signed_in?
    
    @view.render partial: 'comments/invite_to_sign_in_for_new_message', locals: {user: @user}
  end
end
