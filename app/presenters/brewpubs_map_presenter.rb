class BrewpubsMapPresenter
  def initialize(province)
    @province = province
  end

  def view=(value)
    @view = value
  end

  def title
    "Mappa dei brewpub in provincia di #{@province}"
  end

  def breadcrumb
    [Breadcrumb.new("Brewpub", @view.brewpub_path), Breadcrumb.new(@province, @view.brewpub_path(provincia: @province)), Breadcrumb.new("Mappa")]
  end

  def link_to_list
    @view.link_to "Elenco", @view.brewpub_path(provincia: @province)
  end
end