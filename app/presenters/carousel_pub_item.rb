class CarouselPubItem
  def initialize(pub, view)
    @pub = pub
    @view = view
  end

  def render(item_class)
    @view.content_tag :div, class: item_class do
      render_image +
      @view.content_tag(:div, class: "carousel-caption") do
        render_title + render_description
      end
    end
  end

  private
  def render_image
    @view.link_to @view.pub_path(@pub) do
      @view.tag :img, {src: @view.photo_url(@pub.images.first), alt: @pub.name}
    end
  end

  def render_title
    @view.content_tag(:h4) do
      @view.link_to @pub.name, @view.pub_path(@pub)
    end
  end

  def render_description
    @view.content_tag(:p) do
      @view.link_to @view.truncate(@pub.description, length: 100, separator: ' '), @view.pub_path(@pub)
    end
  end
end