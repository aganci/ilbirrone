class PubEventPresenter < EventPresenter
  def initialize(event, view)
    super event, view
  end

  def render_full_address
    @view.link_to(model.pub.name, @view.pub_path(model.pub)) + " " +
    model.pub.full_address
  end

  def render_breadcrumb
    items = PubPresenter.create(model.pub, @view).breadcrumb
    items.last.path = @view.pub_path(model.pub)
    items << Breadcrumb.new('Evento')
    @view.render_breadcrumb items
  end
end