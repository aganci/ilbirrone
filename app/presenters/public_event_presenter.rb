class PublicEventPresenter < EventPresenter
  def initialize(event, view)
    super event, view
  end

  def render_full_address
    "#{model.location} - #{model.full_address}"
  end

  def render_breadcrumb
    @view.render_breadcrumb [Breadcrumb.new('Eventi', @view.events_path), Breadcrumb.new('Evento')]
  end
end