class TimelineEvent
  def initialize(model, render_options)
    @model = model
    @render_options = render_options
  end

  def render(view)
    view.render @render_options
  end

  def created_at
    @model.created_at
  end
end