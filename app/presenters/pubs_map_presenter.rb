class PubsMapPresenter
  def initialize(province)
    @province = province
  end

  def view=(value)
    @view = value
  end

  def title
    "Mappa delle birrerie e pub in provincia di #{@province}"
  end

  def breadcrumb
    [Breadcrumb.new("Birrerie e Pub", @view.pubs_path), Breadcrumb.new(@province, @view.pubs_path(provincia: @province)), Breadcrumb.new("Mappa")]
  end

  def link_to_list
    @view.link_to "Elenco", @view.pubs_path(provincia: @province)
  end
end