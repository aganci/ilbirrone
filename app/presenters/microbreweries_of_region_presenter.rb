class MicrobreweriesOfRegionPresenter
  def initialize(view, microbreweries, region)
    @view = view
    @microbreweries = microbreweries
    @region = region
  end

  def title
    "Microbirrifici della regione #{@region}"
  end
  
  def meta_description
    "Tutti i microbirrifici della regione #{@region} e le birre artigianali prodotte"
  end
  
  def heading
    "Microbirrifici #{@region}"
  end

  def sub_heading
    "#{@region} (#{count})"
  end

  def count
    array = @microbreweries.to_a
    array.size
  end

  def render_breadcrumb
    items = [
      Breadcrumb.new('Microbirrifici', @view.microbreweries_path),
      Breadcrumb.new(@region)
    ]
    @view.render_breadcrumb items 
  end
  
  def render
    if (@microbreweries.empty?)
      @view.render(partial: "no_microbreweries")
    else
      @view.render(partial: "microbreweries", locals: {microbreweries: @microbreweries})
    end
  end
end