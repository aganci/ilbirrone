require 'delegate'
require_relative 'image_presenter'
require 'active_support/core_ext/object/blank'

class UserPresenter < SimpleDelegator
  include ImagePresenter

  def initialize(model, view)
    super model
    @view = view
  end

  def render_link
    @view.link_to model.username, @view.user_path(model)
  end

  def render_small_avatar(css_class='small-avatar')
    @view.content_tag :div, class: css_class do
      @view.link_to @view.user_path(model) do
        photo_tag
      end
    end
  end

  def render_select_avatar
    @view.render partial: 'users/select_avatar' if signed_in_user?
  end

  def render_edit_profile
    @view.link_to "Modifica Profilo", @view.edit_user_registration_path(@user) if signed_in_user?
  end

  def render_city
    return "" if model.city.blank?
    @view.content_tag(:p) do
      @view.content_tag(:i, "", class: "fa fa-map-marker") + 
      " " + 
      model.city
    end
  end

  def render_map
    if model.visited_pubs.present?
      @view.render partial: 'users/visit_map', locals: {pubs: model.visited_pubs}
    else
      @view.render partial: 'users/no_visits'
    end
  end

  def model
    __getobj__
  end

  private
  def signed_in_user?
    model == @view.current_user
  end
end