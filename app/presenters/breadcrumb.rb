class Breadcrumb < Struct.new(:text, :path)
  def initialize(text, path=nil)
    self.text = text
    self.path = path
  end

  def render(view)
    if path.present?
      return view.link_to text, path
    end
    text
  end
end