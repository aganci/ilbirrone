class BrewpubPresenter < PubPresenter
  def initialize(pub, view)
    super pub, view
  end
  
  def pub_type_link
    @view.link_to @pub.pub_type, @view.brewpub_path
  end

  def tabs
    [tab_comments(active: true), tab_reviews, tab_beers, tab_events]
  end

  def breadcrumb
    [
      Breadcrumb.new("Brewpub", @view.brewpub_path),
      Breadcrumb.new(@pub.province, @view.brewpub_path(provincia: @pub.province)),
      Breadcrumb.new(@pub.name)
    ]
  end

end
