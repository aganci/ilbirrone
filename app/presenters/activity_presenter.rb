class ActivityPresenter
  def initialize(user, view, created_at, render_options)
    @user = user
    @view = view
    @created_at = created_at
    @render_options = render_options
  end

  def render_avatar
    UserPresenter.new(@user, @view).render_small_avatar
  end

  def render_description
    @view.render @render_options
  end

  def date
    @created_at
  end
end