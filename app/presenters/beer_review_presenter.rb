class BeerReviewPresenter < SimpleDelegator
  def initialize(model, view)
    super model
    @model = model
    @view = view
  end

  def aroma
    return "-" if (@model.aroma.blank?)
    @model.aroma
  end

  def appearance
    return "-" if (@model.appearance.blank?)
    @model.appearance
  end

  def taste
    return "-" if (@model.taste.blank?)
    @model.taste
  end

  def palate
    return "-" if (@model.taste.blank?)
    @model.taste
  end
end