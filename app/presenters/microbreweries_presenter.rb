class MicrobreweriesPresenter
  def self.create(view, microbreweries, region)
    return MicrobreweriesOfRegionPresenter.new(view, microbreweries, region) if (region.present?)
    MicrobreweriesPresenter.new(view, microbreweries)
  end

  def initialize(view, microbreweries)
    @view = view
    @microbreweries = microbreweries
  end

  def title
    "Microbirrifici d'Italia"
  end

  def meta_description
    "I microbirrifici italiani e le birre prodotte da consultare e recensire"
  end

  def heading
    "Microbirrifici d'Italia"
  end

  def sub_heading
    "Nuove segnalazioni da tutte le regioni (#{count})"
  end

  def render_breadcrumb
    @view.render_breadcrumb 'Microbirrifici'
  end

  def render
    @view.content_tag :div, class: 'row' do
      render_thumbnails
    end
  end

  def count
    array = @microbreweries.to_a
    array.size
  end

  def group_by_region
    results = []
    @microbreweries.group_by(&:region).each do |region, pubs|
      results << pubs.first
    end
    results
  end

  private
  def render_thumbnails
    ThumbnailsPresenter.new(group_by_region, @view).render(3) do |pub|
        @view.render(partial: 'microbrewery_with_photo', locals: {microbrewery: pub})
    end
  end
end