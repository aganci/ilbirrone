class BeerPresenter < SimpleDelegator
  include ImagePresenter
  include CoverPresenter

  def initialize(model, view)
    super model
    @view = view
  end

  def alcohol_by_volume
    "#{model.alcohol_by_volume}%&nbsp;vol.".html_safe
  end

  def serving_temperature
    "#{model.serving_temperature} gradi"
  end

  def optional_data
    result = []
    [:fermentation, :serving_temperature, :family, :style].each do |message|
      next if model.send(message).blank?
      result << OpenStruct.new(title: model.class.human_attribute_name(message), value: send(message))
    end
    result
  end

  def render_description
    return "" if model.description.blank?
    @view.render partial: "beer_description", locals: {description: description}
  end

  def tabs
    tabs = []
    tabs << OpenStruct.new(id: 'reviews', text: 'Recensioni', template: 'beers/reviews', variables: {beer: model })
    tabs.first.active = true
    tabs
  end

  def render_breadcrumb
    if (model.pub.pub_type == 'Brewpub')
      items = [
        Breadcrumb.new("Brewpub", @view.brewpub_path),
        Breadcrumb.new(model.pub.name, @view.pub_path(model.pub)),
        Breadcrumb.new(model.name)
      ]      
    else
      items = [
        Breadcrumb.new("Microbirrifici", @view.microbreweries_path),
        Breadcrumb.new(model.pub.region, @view.microbreweries_path(region: model.pub.region)),
        Breadcrumb.new(model.pub.name, @view.pub_path(model.pub)),
        Breadcrumb.new(model.name)
      ]
    end
    @view.render_breadcrumb items
  end

  def model
    __getobj__
  end
end