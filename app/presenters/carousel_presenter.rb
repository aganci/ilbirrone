class CarouselPresenter
  def initialize(pubs, view)
    #@pubs = pubs.map {|p| CarouselPubItem.new(p, view)}.insert(1, CarouselAdvertisingItem.new(view)).reject {|item| item.nil?}
    @pubs = pubs.map {|p| CarouselPubItem.new(p, view)}.reject {|item| item.nil?}

    @view = view
  end

  def render_indicators(carousel_id)
    result = []
    image_index = 0
    for pub in @pubs do
      result << render_indicator(image_index, carousel_id)
      image_index += 1
    end
    result.join("\n").html_safe
  end

  def render_items
    result = []
    image_index = 0
    for pub in @pubs do
      result << render_item(pub, image_index)
      image_index += 1
    end
    result.join("\n").html_safe
  end

  private
  def render_indicator(index, carousel_id)
    options = {
      "data-target" => carousel_id, 
      "data-slide-to" => index.to_s 
    }
    if (index == 0)
      options['class'] = 'active'
    end
    @view.content_tag(:li, '', options)
  end

  def render_item(pub, index)
    if (index == 0)
      item_class = "item active"
    else
      item_class = "item"
    end

    pub.render(item_class)
  end
end