require_relative 'pub_presenter'

class PubChangesPresenter < SimpleDelegator
  def initialize(model, view)
    super model
    @view = view
  end

  def closing_days
    PubPresenter.create(model, @view).closing_days
  end

  def description
    @view.render_description(model.description)
  end

  def closed
    ''
  end

  def duplicated
    ''
  end

  def note
    @view.render_description(model.note)
  end


  def differences_from(pub, user)
    default = PubChanges.from(pub, user)
    result = {}
    [:name, :pub_email, :website, :closing_days, :full_address, :phone, :description, :pub_type, :closed, :duplicated, :note, :coordinates].each do |message|
      unless model.send(message) == default.send(message)
        translated = @view.t('activerecord.attributes.pub_changes.' + message.to_s)
        result[translated] = self.send(message) 
      end
    end
    result
  end

  def model
    __getobj__
  end
end