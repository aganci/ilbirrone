class EventPresenter < SimpleDelegator
  include ImagePresenter
  include GeolocablePresenter
  include CoverPresenter

  def self.create(event, view)
    return PubEventPresenter.new(event, view) if event.type == "PubEvent"
    return PublicEventPresenter.new(event, view) if event.type == "PublicEvent"
  end

  def initialize(event, view)
    super event
    @view = view
  end

  def when
    if model.start_date == model.end_date
      @view.l(model.start_date, format: :long).capitalize
    else
      "da #{@view.l model.start_date, format: :long} a #{@view.l model.end_date, format: :long}"
    end
  end

  def short_description
    @view.truncate(model.description, length: 100, separator: ' ')
  end

  def model
    __getobj__
  end
end