class SearchPresenter
  def initialize(beers, pubs)
    @beers = beers
    @pubs = pubs
  end

  def render(view)
    result = ""
    if (@pubs.present?)
      result << view.render(partial: 'search/pubs', locals: {pubs: @pubs})
    end 

    if @beers.present?
      result << view.render(partial: 'search/beers', locals: {beers: @beers})
    end

    result.html_safe
  end

  def render_add_pub_invitation(view)
    if (@beers.present? || @pubs.present?)
      return view.render(partial: 'search/with_results')
    end
    view.render(partial: 'search/no_results')
  end
end