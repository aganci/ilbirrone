class ThumbnailsPresenter
  def initialize(model, view)
    @model = model
    @view = view
  end

  def render(column_count, &block)
    result = ""
    @model.in_groups_of(column_count).transpose.each do |group|
      result << render_group(group, &block)
    end
    result.html_safe
  end

  private
  def render_group(group, &block)
    @view.content_tag :div, class: "col-md-4" do
      result = ""
      group.reject {|g| g.nil?}.each do |item|
        result << render_item(item, &block)
      end
      result.html_safe
    end
  end

  def render_item(item, &block)
    @view.content_tag :div, class: 'thumbnail' do
      yield(item)
    end
  end
end