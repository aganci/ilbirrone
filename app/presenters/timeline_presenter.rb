class TimelinePresenter
  def self.create_events(user)
    events = pubs_events(user) + comments_events(user) + user_comments_events(user) + events_events(user) + reviews_events(user) + beers_events(user) + beers_reviews(user)
    events = events.sort {|a, b| (b.created_at <=> a.created_at) }.take(20)
    events + sign_up_event(user)
  end

  def self.sign_up_event(user)
    [TimelineEvent.new(user, {partial: 'sign_up_event', locals: {user: user}})]
  end

  def self.pubs_events(user)
    user.pubs.map do |pub|
      TimelineEvent.new(pub, {partial: 'pub_event', locals: {pub: pub}})
    end
  end

  def self.comments_events(user)
    user.comments.map do |comment|
      CommentActivityPresenter.new(comment, nil)
    end
  end
  
  def self.user_comments_events(user)
    user.user_comments.map do |comment|
      TimelineEvent.new(comment, {partial: 'user_comment_event', locals: {comment: comment}})
    end
  end

  def self.events_events(user)
    user.events.map do |event|
      TimelineEvent.new(event, {partial: 'event_event', locals: {event: event}})
    end
  end

  def self.reviews_events(user)
    user.reviews.map do |review|
      TimelineEvent.new(review, {partial: 'review_event', locals: {review: review}})
    end
  end

  def self.beers_events(user)
    user.beers.map do |beer|
      TimelineEvent.new(beer, {partial: 'beer_event', locals: {beer: beer}})
    end
  end

  def self.beers_reviews(user)
    user.beer_reviews.map do |review|
      TimelineEvent.new(review, {partial: 'beer_review_event', locals: {review: review}})
    end
  end
end