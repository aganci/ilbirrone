module GeolocablePresenter
  def latitude
    model.latitude || 37.358901
  end

  def longitude
    model.longitude || 13.847684
  end

  def link_to_map
    position = "#{self.latitude},#{self.longitude}"
    @view.link_to @view.t('helpers.links.open_map_in_new_window'), 
      "http://maps.google.it?q=#{CGI.escape(position)}", target: '_blank'
  end  
end