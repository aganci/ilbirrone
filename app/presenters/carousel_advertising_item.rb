class CarouselAdvertisingItem
  def initialize(view)
    @view = view
  end

  def render(item_class)
    @view.content_tag :div, class: item_class do
      render_image
    end
  end

  private
  def render_image
    @view.link_to url, target: "_blank", onClick: "trackEvent('Banner', 'Fiera Birra piacenza Big');" do
      @view.tag :img, {src: photo_url, alt: "Birra Expo 2014"}
    end
  end

  def url
    "http://www.fierabirrapiacenza.com/"
  end

  def photo_url
    @view.image_path("banners/mastro-birraio-piacenza-big.jpg")
  end
end