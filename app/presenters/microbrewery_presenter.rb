require_relative 'pub_presenter'

class MicrobreweryPresenter < PubPresenter
  def initialize(pub, view)
    super pub, view
  end
  
  def pub_type_link
    @view.link_to @pub.pub_type, @view.microbreweries_path
  end

  def tabs
    [tab_comments(active: true), tab_beers, tab_events]
  end

  def breadcrumb
    [
      Breadcrumb.new("Microbirrifici", @view.microbreweries_path),
      Breadcrumb.new(@pub.region, @view.microbreweries_path(region: @pub.region)),
      Breadcrumb.new(@pub.name)
    ]
  end

  def render_visitor_link
    ""
  end
end
