require 'delegate'
require_relative 'image_presenter'
require_relative 'geolocable_presenter'
require_relative 'cover_presenter'

class PubPresenter < SimpleDelegator
  include ImagePresenter
  include GeolocablePresenter
  include CoverPresenter

  def self.create(pub, view)
      return MicrobreweryPresenter.new(pub, view) if (pub.pub_type == 'Microbirrificio')
      return PubPresenter.new(pub, view) if (pub.pub_type == 'Birreria')
      return BrewpubPresenter.new(pub, view) if (pub.pub_type == 'Brewpub')
  end

  def initialize(pub, view)
    super pub
    @pub = pub
    @view = view
  end

  def comments
    @pub.comments.reject { |comment| comment.cover? }
  end

  def pub_type_link
    @view.link_to @pub.pub_type, @view.pubs_path
  end

  def closing_days
    result = []
    @pub.closing_days.split(",").each do |day_index|
      result << @view.t('date.day_names')[day_index.to_i]
    end
    result.join(", ")
  end

  def website
    site = @pub.website
    if (!site.start_with?('http://') && !site.start_with?('https://'))
      site = "http://" + site
    end
    @view.link_to "Vai al sito", site, target: "_blank", rel: 'nofollow'
  end

  def tabs
    [tab_comments(active: true), tab_reviews, tab_events]
  end

  def breadcrumb
    [
      Breadcrumb.new("Birrerie e Pub", @view.pubs_path),
      Breadcrumb.new(@pub.province, @view.pubs_path(provincia: @pub.province)),
      Breadcrumb.new(@pub.name)
    ]
  end

  def render_description
    @view.render_description(@view.truncate(model.description, length: 100, separator: ' '))
  end

  def distance_from(latitude, longitude)
    @view.number_with_precision(model.distance_from([latitude, longitude], :km).to_f, precision: 2)
  end


  def tab_comments(options = {})
    OpenStruct.new( {id: 'comments', template: 'pubs/comments', variables: { pub: self }}.merge(options) ).tap do |tab|
      set_label_with_count(tab, comments, "Commenti")
    end
  end

  def tab_events(events = EventQuery.new.located_at(@pub))
    OpenStruct.new(id: 'events', template: 'pubs/events', variables: { pub: @pub, events: events }).tap do |tab|
      set_label_with_count(tab, events, "Eventi")
    end
  end

  def tab_reviews(options = {})
    OpenStruct.new( {id: 'reviews', template: 'pubs/reviews', variables: { pub: @pub }}.merge(options) ).tap do |tab|
      set_label_with_count(tab, @pub.reviews, "Recensioni")
    end  
  end

  def tab_beers(options = {})
    OpenStruct.new( {id: 'beers', template: 'pubs/beers', variables: { pub: @pub }}.merge(options) ).tap do |tab|
      set_label_with_count(tab, @pub.beers, "Birre")
    end
  end

  def render_visitor_link
    return "" if model.visitor?(@view.current_user)
    @view.link_to('Sono stato qui', @view.visit_pub_visitors_path(pub_id: model.id), class: 'visitor-button')
  end

  def model
    __getobj__
  end
  
  private
  def set_label_with_count(tab, collection, label)
    if collection.size == 0
      tab.text = label
    else
      tab.text = "#{label} (#{collection.size})"
    end
  end
end