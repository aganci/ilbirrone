require_relative 'activity_presenter'

class CommentActivityPresenter < ActivityPresenter
  def initialize(comment, view)
    super comment.user, view, comment.created_at, nil
    @comment = comment
  end

  def render_description
    if @comment.avatar?
      return @view.render partial: "home_page/changed_avatar_activity", locals: {comment: @comment}  
    end

    if @comment.cover?
      return @view.render partial: "home_page/changed_cover_activity", locals: {comment: @comment} 
    end

    if @comment.commentable_type == "User"
      if @comment.commentable == @comment.user
        return @view.render partial: "home_page/user_comment", locals: {comment: @comment} 
      end

      return @view.render partial: "home_page/user_comment_on_another_user", locals: {comment: @comment} 
    end

    @view.render partial: "home_page/comment_activity", locals: {comment: @comment} 
  end

  def render(view)
    if @comment.avatar?
      return view.render partial: "users/changed_avatar_event", locals: {comment: @comment} 
    end

    if @comment.cover?
      return view.render partial: "users/changed_cover_event", locals: {comment: @comment} 
    end

    view.render partial: 'users/comment_event', locals: {comment: @comment}
  end

  def created_at
    @comment.created_at
  end
end