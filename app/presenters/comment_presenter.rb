require_relative '../models/comment_permissions'

class CommentPresenter
  attr_reader :model

  def initialize(model, view)
    @model = model
    @view = view
  end

  def link_id
    "comment_#{@model.id}"
  end

  def render_form
    return @view.render partial: 'comments/new', locals: { comment: @model } if @view.signed_in?
    @view.render partial: 'comments/sign_in_invitation' 
  end

  def render_commands
    @view.render(partial: 'comments/commands', locals: { comment: self })  if can_delete?
  end

  def pub_url
    @view.pub_url @model.commentable
  end

  def share_message
    return "Ha inserito una foto #{location}" if @model.has_image?
    "Ha inserito un commento #{location}"
  end

  def render_image
    return @view.image_tag(@view.photo_url(@model)) if @model.has_image?
    ""
  end

  def sharing_image_url
    return @view.photo_url(@model) if @model.has_image?
    @view.asset_url("facebook-logo.png")
  end

  def can_delete?
    CommentPermissions.new(@model).can_delete?(@view.current_user)
  end

  private
  def location
    "sulla pagina de #{@view.t('site_name')} #{@model.commentable.pub_type} #{@model.commentable.name} - #{@model.commentable.city}"
  end
end