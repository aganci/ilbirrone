module ImagePresenter
  def render_photo
    return @view.render partial: 'shared/photo', locals: {imageable: model} if has_photo?
    @view.image_tag model.default_photo
  end

  def photo_tag
    return @view.render_photo(model) if has_photo?
    @view.image_tag model.default_photo
  end

  def photo_url
    @view.default_photo_url(model)
  end

  def has_photo?
    model.images.size > 0
  end
end
