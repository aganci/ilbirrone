module CoverPresenter

  def render_add_cover
    return "" if model.cover?
    return @view.render(partial: 'comments/add_cover_form', locals: {commentable_id: model.id, commentable_type: model.class.name}) if @view.signed_in?
    @view.render(partial: 'comments/add_cover_login_button')
  end

  def render_delete_cover
    if model.cover? && @view.current_user && (@view.current_user.admin? || model.cover_owner == @view.current_user)
      @view.render(partial: 'comments/delete_cover', locals: {comment: model.cover})
    else
      ""
    end
  end

end