class CommentPermissions
  def initialize(comment)
    @comment = comment
  end

  def can_delete?(user)
    return false if user.nil?
    return true if user.admin?
    return true if user == @comment.commentable
    return @comment.user == user
  end
end