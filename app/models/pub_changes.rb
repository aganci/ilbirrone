require 'active_model'

class PubChanges

  def self.from(pub, current_user)
    user_email = current_user.email if current_user 

    PubChanges.new(
      pub_id: pub.id, 
      name: pub.name, 
      pub_email: pub.email,
      website: pub.website,
      closing_days: pub.closing_days,
      user_email: user_email,
      full_address: pub.full_address,
      phone: pub.phone,
      description: pub.description,
      pub_type: pub.pub_type,
      closed: '0',
      duplicated: '0',
      note: '',
      coordinates: "#{pub.latitude}, #{pub.longitude}")
  end

  include ActiveModel::Validations
  include ActiveModel::Conversion

  class << self
    def i18n_scope
      :activerecord
    end
  end

  attr_accessor :pub_id, :name, :pub_email, :user_email, :website, :closing_days, :full_address, :coordinates, :phone, :description, :pub_type
  attr_accessor :closed, :duplicated, :note

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end