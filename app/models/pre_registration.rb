class PreRegistration < ActiveRecord::Base
  attr_accessible :user_id

  belongs_to :user

  validates :user_id, uniqueness: true
end
