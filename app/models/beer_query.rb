class BeerQuery
  def initialize
    @beer = Beer
  end

  def self.most_recent(count)
    query = BeerQuery.new
    query.limit(count)
    query.includes_avatar
    query.order_by_created_at
  end

  def self.most_recent_with_photo(count)
    query = BeerQuery.new
    query.limit(count)
    query.includes_reviews
    query.where_pub_status("approved")
    query.order_by_image_created_at
  end

  def types
    @beer = @beer.select('beer_type')
    @beer = @beer.group('beer_type')
    @beer = @beer.where('beer_type is not null')
    @beer = @beer.map {|b|b.beer_type}    
  end

  def self.find(id)
    BeerQuery.new.find_by_id(id)
  end

  def find_by_id(id)
    include_images
    @beer = @beer.includes(:pub)
    @beer = @beer.includes(:reviews => {:user => :avatar})
    @beer.find(id)
  end

  def find_by_type(beer_type)
    where_pub_status('approved')
    @beer = @beer.where('beer_type = ?', beer_type)
    sort_by_rating
  end

  def same_type(beer, count)
    limit(count)
    include_images
    where_pub_status('approved')
    @beer = @beer.where('beer_type = ? and beers.id <> ?', beer.beer_type, beer.id)
  end

  def best
    include_images
    @beer = @beer.joins(:reviews)
    where_pub_status("approved")
    sort_by_rating
  end

  def search(text)
    where_name_like(text)
    where_pub_status("approved")
    sort_by_rating
  end

  def search_first(text, count)
    limit(count)
    where_name_like(text)
    where_pub_status("approved")
  end
  
  def where_name_like(name)
    @beer = @beer.where('lower(beers.name) like ? ', "%#{name.downcase}%")
  end
  
  def where_pub_status(value)
    @beer = @beer.includes(:pub).where(pubs: { status: value })
  end

  def include_images
    @beer = @beer.includes(:comments)
  end

  def limit(count)
    @beer = @beer.limit(count)
  end

  def sort_by_rating
    @beer = @beer.includes(:reviews).to_a.sort! {|a, b| (b.rating <=> a.rating).nonzero? || (a.name <=> b.name) }
  end

  def includes_avatar
    @beer = @beer.includes(:user => :avatar)
  end

  def order_by_created_at
    @beer = @beer.order('beers.created_at DESC')
  end

  def order_by_image_created_at
    @beer = @beer.joins(:comments).order('comments.created_at DESC')
  end

  def includes_reviews
    @beer = @beer.includes(:reviews)
  end
end