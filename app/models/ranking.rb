class Ranking < ActiveRecord::Base
  def self.popular_pubs
    includes(:pub).includes(:pub => :comments).order(:position).to_a.map {|r| r.pub}
  end

  belongs_to :pub
  attr_accessible :pub, :position
end
