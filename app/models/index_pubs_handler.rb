class IndexPubsHandler
  def self.execute(controller, province, show_map)
    if (province.present?) && (show_map.present?)
        pubs = PubQuery.in_province_of_with_coordinates(province, ['Brewpub', 'Birreria'])
        presenter = PubsMapPresenter.new(province)
        controller.render 'pubs/map', locals: { presenter: presenter, pubs: pubs }
        return
    end

    if (province.present?) && (show_map.nil?)
        pubs = PubQuery.in_province_of(province, ['Brewpub', 'Birreria'])
        controller.render 'pubs/list', locals: { province: province, pubs: pubs }
        return
    end

    pubs = PubQuery.recents_with_cover(20, ['Brewpub', 'Birreria'])
    controller.render 'index', locals: { pubs: pubs }
  end
end