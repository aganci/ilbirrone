class SensoryAnalysis
  class Section
    attr_reader :name, :values

    def initialize(name, values = [])
      @name = name
      @values = values
    end
  end

  def self.items(view)
    result = []
    analysis_types = view.t("sensory_analysis")
    for analysis_type in analysis_types
      result << SensoryAnalysis.create(analysis_type, view)
    end
    result
  end

  def self.load_sections(sections)
    result = []
    for section in sections do
      result << Section.new(section[:name], section[:terms])
    end
    result
  end

  def self.create(options, view)
    SensoryAnalysis.new(
      id: options[:type], 
      field_id: "beer_review_#{options[:type]}", 
      tags_id: "#{options[:type]}_tags", 
      title: view.t("activerecord.attributes.beer_review.#{options[:type]}"),
      description: options[:description],
      sections: load_sections(options[:sections]))
  end

  attr_reader :id, :field_id, :title, :description, :sections, :tags_id

  def initialize(options)
    @id = options[:id]
    @field_id = options[:field_id]
    @title = options[:title]
    @description = options[:description]
    @sections = options[:sections] || []
    @active = false
    @tags_id = options[:tags_id]
  end

  def active=(value)
    @active = value
  end

  def active?
    @active
  end
end