class Place
  def self.countries
    [ "Italia", "Austria", "Belgio", "Germania", "Olanda", "Regno Unito", "USA"]
  end

  def self.cities
    return @cities if @cities.present?
    @cities = load_file("cities.yml")
  end

  def self.where_city(starts_with = "")
    cities.select {|city| city[0].downcase.start_with?(starts_with.downcase)}
  end
  
  private
  def self.load_file(name)
    path = File.join(Rails.root, "db", name)
    File.open(path, "r", encoding: 'iso-8859-16') do |file|
      return YAML::load(file.read).sort {|a, b| (a[0] <=> b[0]) }
    end
  end

end