class EventQuery
  def initialize
    @event = Event
  end

  def all
    @events = @event.includes(:comments, :pub).where('end_date >= ?', Date.today).order(:end_date)
  end

  def first(count)
    @event = @event.limit count
    all
  end

  def located_at(pub)
    @event = @event.where('pub_id = ?', pub.id)
    all
  end
end