class Event < ActiveRecord::Base
  include FriendlyId
  include Cover
  include UserPointsUpdate  
  
  def self.most_recent(count)
    Event.limit(count).includes(:user => :avatar).order(created_at: :desc)
  end


  attr_accessible :pub_id, :pub, :user_id, :user, :start_date, :end_date, :title, :description, :website, :type
  friendly_id :title, use: [:slugged, :finders]
  
  belongs_to :pub
  belongs_to :user
  has_many :comments, -> { order(created_at: :desc) }, as: :commentable, :dependent => :destroy

  validates :title, presence: true
  validates :description, presence: true
  validates :end_date, :date => { :after_or_equal_to => Proc.new { Date.today } }
  validates :end_date, :date => {:after_or_equal_to => :start_date}
  validates :website, url: true
  
  def images
    covers
  end

  def default_photo
    'no-photo.jpg'
  end

  def name
    title
  end
  
  def photo_description
    title
  end
end
