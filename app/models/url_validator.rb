class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if not valid?(value)
      record.errors.add(attribute, :invalid_url, options)
    end
  end

  def valid?(uri)
    return true if uri.blank?

    uri = URI.parse(uri)
    uri.kind_of?(URI::HTTP) || uri.kind_of?(URI::HTTPS) 
  rescue URI::InvalidURIError
    false
  end  
end