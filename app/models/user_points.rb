class UserPoints
  def self.calculate_on(user)
    points = points_for_sign_up
    points += user.approved_pubs.size * points_for_pub_approval
    points += user.events.size * points_for_event
    points += user.reviews.size * points_for_pub_review
    points += user.beers.size * points_for_beer
    points += user.beer_reviews.size * points_for_beer_review
    points += user.comments_with_image.size * points_for_image
    points += user.text_only_comments.size * points_for_comment
    
    user.points = points
  end

  def self.table
    [
      row("Iscrizione", points_for_sign_up), 
      row("Segnalazione locale", points_for_pub_approval),
      row("Segnalazione evento", points_for_event),
      row("Recensione locale", points_for_pub_review),
      row("Segnalazione Birra", points_for_beer),
      row("Recensione Birra", points_for_beer_review),
      row("Caricamento immagine", points_for_image),
      row("Commento", points_for_comment)
    ]
  end

  def self.row(description, points)
    OpenStruct.new(description: description, points: points)
  end

  def self.points_for_sign_up
    1
  end

  def self.points_for_pub_approval
    10
  end

  def self.points_for_event
    10
  end

  def self.points_for_pub_review
    5
  end

  def self.points_for_beer
    10
  end

  def self.points_for_beer_review
    5
  end

  def self.points_for_image
    5
  end

  def self.points_for_comment
    1
  end

end