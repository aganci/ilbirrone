module Cover
  def cover?
    covers.size > 0
  end
  
  def covers 
    comments.select {|comment| comment.cover?}
  end
  
  def cover_owner
    cover.user
  end

  def cover
    covers.first
  end
end