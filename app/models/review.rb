class Review < ActiveRecord::Base
  include UserPointsUpdate
  
  attr_accessible :pub_id, :user_id, :rating, :date, :description, :user, :beers, :food, :value, :atmosphere, :frequenter
  
  belongs_to :pub
  belongs_to :user
  has_many :notifications, :dependent => :destroy

  validates :user, presence: true
  validates :date, presence: true
  validates :date, :date => { :before_or_equal_to => Proc.new { Date.today } }
  validates :rating, presence: true
  validates :rating, :inclusion => 1..5
  validates :description, presence: true
  validates_length_of :description, :minimum => 100

end
