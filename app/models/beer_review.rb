class BeerReview < ActiveRecord::Base
  include UserPointsUpdate
  
  attr_accessible :beer_id, :user_id, :rating, :user, :content, :aroma, :appearance, :taste, :palate

  belongs_to :beer
  belongs_to :user

  validates :rating, presence: true
  validates :rating, :inclusion => 1..5

  def self.most_recent(count)
    BeerReview.limit(count).includes(:user => :avatar).includes(:beer).order(created_at: :desc)
  end

end