class Notification < ActiveRecord::Base
  def self.notify_pub_approval(pub)
    NotificationsMailer.pub_approved(pub).deliver
    Notification.create!(notification_type: 'approval', pub: pub, user: pub.user)
  end

  def self.notify_pub_review(review)
    Notification.create!(notification_type: 'review', review: review, pub: review.pub, user: review.user)
  end

  def self.notify_pub_comment(comment)
    Notification.create!(notification_type: 'pub_comment', pub: comment.commentable, comment: comment, user: comment.user)
  end
  
  def self.notify_user_comment(comment)
    Notification.create!(notification_type: 'user_comment', comment: comment, user: comment.user)
  end

  def self.notify_user_registration(user)
    NotificationsMailer.welcome_new_user(user).deliver
    Notification.create!(notification_type: 'user_registration', user: user)
  end

  def self.notify_event(event)
    Notification.create!(notification_type: 'event', event: event, user: event.user)
  end

  def self.notify_beer(beer)
    Notification.create!(notification_type: 'beer', beer: beer, pub: beer.pub, user: beer.user)
  end

  def self.notify_beer_review(review)
    Notification.create!(notification_type: 'beer_review', beer_review: review, beer: review.beer, user: review.user)
  end

  def self.paginated_timeline(page)
    Notification.includes(:pub, :review, :user, :comment, :event, :beer, :beer_review).paginate(:page => page, :per_page => 20).order(created_at: :desc)
  end

  def self.timeline(count)
    Notification.includes(:pub, :review, :user, :comment, :event, :beer, :beer_review).limit(count).order(created_at: :desc)
  end

  attr_accessible :notification_type, :pub, :review, :user, :comment, :event, :beer, :beer_review

  belongs_to :pub
  belongs_to :review
  belongs_to :user
  belongs_to :comment
  belongs_to :event
  belongs_to :beer
  belongs_to :beer_review
end
