class PubEvent < Event
  def location
    pub.name
  end

  def address
    pub.address
  end

  def city
    pub.city
  end
  
  def city_with_province
    pub.city_with_province
  end

  def region
    pub.region
  end

  def latitude
    pub.latitude
  end

  def longitude
    pub.longitude
  end
end