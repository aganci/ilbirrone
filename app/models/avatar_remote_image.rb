class AvatarRemoteImage
  def initialize(model)
    @model = model
  end

  def path(local_file_name)
    "users/#{@model.id}#{extension_name_for(local_file_name)}"
  end

  def prepare_image(image)
    image.resize_to_fill(200, 200)
  end

  private
  def extension_name_for(local_file_name)
    path = Pathname.new(local_file_name).extname
  end
end