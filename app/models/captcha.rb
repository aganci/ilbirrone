class Captcha
  def self.valid?(params)
    uri = URI.parse("https://www.google.com/recaptcha/api/siteverify")
    http_response = Net::HTTP.post_form(uri, {"secret" => "6Lc6xAoTAAAAADlRxmLamlODPrYVBiR0PH38_Yvs", "response" => params["g-recaptcha-response"]})
    response = JSON.parse(http_response.body)
    response["success"]
  end
end