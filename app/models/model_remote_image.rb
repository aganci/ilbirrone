require 'pathname'

class ModelRemoteImage
  def initialize(model, model_type)
    @model = model
    @model_type = model_type.underscore.pluralize 
  end

  def path(local_file_name)
    "#{folder_name}/#{@model.friendly_id}#{extension_name_for(local_file_name)}"
  end

  def prepare_image(image)
    image.change_geometry "500>" do |cols, rows, img|
      img.resize! cols, rows
    end
  end

  private
  def folder_name
    @model_type.underscore.pluralize
  end

  def extension_name_for(local_file_name)
    path = Pathname.new(local_file_name).extname
  end
end