class Rating
  def self.calculate(reviews)
    return 0.0 if reviews.size == 0
    average = reviews.collect(&:rating).sum.to_f / reviews.size
    round(average, 0.5)
  end
  
  private
  def self.round(value, precision)
    (value*(1.0 / precision)).round / (1.0 / precision)
  end
end