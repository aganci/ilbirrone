require 'ftp_client'

class Comment < ActiveRecord::Base
  include UserPointsUpdate

  attr_accessible :user_id, :user, :content, :commentable_id, :commentable_type, :path, :width, :height
  before_destroy :destroy_image

  belongs_to :user
  belongs_to :commentable, polymorphic: true

  has_many :notifications, :dependent => :destroy

  validates_length_of :content, :minimum => 10, :allow_blank => true

  def cover?
    image_type == "Cover"
  end

  def avatar?
    commentable_type == "User" && cover?
  end

  def has_image?
    return path.present?
  end

  def user_status?
    user == commentable
  end

  def destroy_image
    return if !has_image?

    FtpClient.image_server.delete_file(path)
  end

  def self.most_recent(count)
    Comment.limit(count).includes(:user => :avatar).order(created_at: :desc)
  end
end
