require 'active_support/core_ext/object/blank'

class CommentRegistration
  def self.upload_comment(pub, content, image_file, user)
    comment = pub.comments.build
    comment.content = content
    comment.user = user

    if image_file.present?
      ImageStorage.upload_comment_image(image_file, comment)
    end

    if (image_file.nil? && content.blank?)
      return false
    end

    if comment.save
      Notification.notify_pub_comment(comment)
      Visitor.find_or_create_by(user, pub)
      return true
    end
    return false
  end

  def self.upload_user_comment(user, content, image_file, logged_user)
    comment = user.user_comments.build
    comment.content = content
    comment.user = logged_user

    if image_file.present?
      ImageStorage.upload_comment_image(image_file, comment)
    end

    if (image_file.nil? && content.blank?)
      return false
    end

    if comment.save
      comment.save
      Notification.notify_user_comment(comment)
      return true
    else
      return false
    end
  end

  def self.upload_cover(commentable, image_file, user)
    comment = commentable.comments.build
    comment.image_type = "Cover"
    comment.user = user
    
    if image_file.present?
      ImageStorage.upload_model(image_file, comment)
    end

    comment.save!
  end

  def self.upload_avatar(commentable, image_file, user)
    comment = commentable.build_avatar
    comment.image_type = "Cover"
    comment.user = user
    
    if image_file.present?
      ImageStorage.upload_avatar(image_file, comment)
    end

    comment.save!
  end
end