class ReviewQuery
  def initialize
    @review = Review
  end

  def self.most_recent(count)
    query = ReviewQuery.new
    query.limit(count)
    query.includes_pub
    query.includes_avatar
    query.where_pub_approved
    query.order_by_created_at
  end

  def most_recent_with_ratings(count)
    limit count
    @review = @review.includes({pub: [:reviews]}, :user)
    where_pub_approved
    order_by_created_at
  end

  def excludes(pub)
    @review = @review.where("pubs.id <> ?", pub.id).references(:pubs)
  end

  def limit(count)
    @review = @review.limit(count)
  end

  def includes_pub
    @review = @review.includes(:pub)
  end

  def includes_avatar
    @review = @review.includes(:user => :avatar)
  end

  def where_pub_approved
    @review = @review.where(pubs: { status: 'approved' })
  end

  def order_by_created_at
    @review = @review.order("reviews.created_at DESC")
  end
end