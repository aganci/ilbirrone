class PubQuery
  def initialize
    @pub = Pub
  end

  def self.recents_with_cover(count, types = ["Birreria", "Brewpub", "Microbirrificio"] )
    query = PubQuery.new
    query.where_status('approved')
    query.where_type(types)
    query.include_images
    query.where_have_cover
    query.limit(count)
    query.order('pubs.created_at DESC')
  end
  
  def recents_with_review(count, types)
    where_status('approved')
    where_type(types)
    include_images
    @pub = @pub.includes(:reviews)
    @pub = @pub.where('reviews.rating is not null').references(:reviews)
    @pub = @pub.limit(count * 2)
    @pub = @pub.order('reviews.created_at DESC')
    @pub.take(count)
  end

  def self.most_recent(count)
    query = PubQuery.new
    query.where_status('approved')
    query.includes_avatar
    query.limit(count)
    query.order(created_at: :desc)
  end


  def self.near(latitude, longitude, max_count, max_distance = 20)
    pubs = Pub.limit(max_count).near([latitude, longitude], max_distance, :units => :km)
    return [] if pubs.nil?
    PubQuery.new.load(pubs.select{|p| p.approved? && ["Birreria", "Brewpub"].include?(p.pub_type)}.map {|pub| pub.id})
  end
  
  def self.nearest(latitude, longitude)
    pub = Pub.limit(1).near([latitude, longitude], 1, :units => :km).first
    return nil if pub.nil?
    return pub if pub.approved? && ["Birreria", "Brewpub"].include?(pub.pub_type)
    nil
  end

  def self.curious
    PubQuery.new.load([1857, 1933, 1932, 386, 1094, 1089, 1108, 1078, 1381, 1441, 1457, 993, 1491, 119])
  end

  def load(pub_ids)
    @pub = @pub.where(id: pub_ids)
    include_images
    @pub = @pub.includes(:reviews)
    pub_ids.map { |id| @pub.to_a.find {|p| p.id == id } }
  end

  def provinces_with_count(pub_type)
    @pub = @pub.select('province, region, count(id) count')
    @pub = @pub.group(:province, :region)
    @pub = @pub.where("region <> ''")
    where_type(pub_type)
    where_status('approved')
    @pub = @pub.order(:region, :province)
  end

  def provinces(pub_type)
    @pub = @pub.select('province')
    where_status('approved')
    where_type(pub_type)
    @pub = @pub.group('province')
    @pub = @pub.map {|pub| pub.province}
  end
  
  def regions(pub_type)
    @pub = @pub.select('region')
    where_status('approved')
    where_type(pub_type)
    @pub = @pub.group('region')
    @pub = @pub.map {|pub| pub.region}
  end

  def self.in_province_of(province, types)
    query = PubQuery.new
    query.include_images
    query.where_status('approved')
    query.where_location(province)
    query.where_type(types)
    query.sort_by_rating
  end

  def self.in_province_of_with_coordinates(province, types)
    PubQuery.in_province_of(province, types).select {|pub| pub.has_coordinates?} 
  end

  def best(type)
    @pub = @pub.joins(:reviews)
    include_images
    where_status('approved')
    where_type(type)
    sort_by_rating
    @pub = @pub.take(50)    
  end

  def search(text)
    where_name_or_location_like(text)
    where_status('approved')
    sort_by_rating
  end

  def search_first(text, count)
    @pub = @pub.limit(count)
    where_name_or_location_like(text)
    where_status('approved')
  end

  def find_by_location(location)
    where_location(location)
    where_status('approved')
    sort_by_rating
  end

  def in_the_same_province(pub, count)
    include_images
    where_status('approved')
    where_province(pub.province.downcase)
    where_name_is_not(pub.name)
    sort_by_rating
    @pub.take(count)
  end

  def to_approve_count
    @pub = @pub.where("status <> 'approved'").count
  end

  def microbreweries_by_region
    include_beers_count
    include_images
    where_status('approved')
    where_type(["Microbirrificio", "Brewpub"])
    @pub = @pub.order(created_at: :desc)
  end

  def microbreweries(region)
    include_beers_count
    where_status('approved')
    where_type(["Microbirrificio", "Brewpub"])
    @pub = @pub.order('pubs.name')
    where_region(region)
  end

  def self.find(id)
    PubQuery.new.find_by_id(id)
  end

  def find_by_id(id)
    @pub = @pub.includes(:user)
    @pub = @pub.includes(:reviews => {:user => :avatar})
    @pub = @pub.includes(:beers => :reviews)
    @pub = @pub.includes(:events => :user)
    @pub = @pub.includes(:comments => {:user => :avatar})
    @pub = @pub.includes(:comments)
    @pub = @pub.includes({:visitors => :user})
    @pub.find(id)
  end

  def where_location(city_or_province)
    @pub = @pub.where('lower(city) = ? or lower(province) = ?', city_or_province.downcase, city_or_province.downcase)
  end

  def where_name_like(name)
    @pub = @pub.where('lower(name) like ? ', "%#{name.downcase}%")
  end

  def where_name_or_location_like(text)
    @pub = @pub.where('lower(city) like ? or lower(province) like ? or lower(name) like ? ', 
      "%#{text.downcase}%", "%#{text.downcase}%", "%#{text.downcase}%")
  end

  def where_status(value)
    @pub = @pub.where('status = ?', value)
  end

  def sort_by_rating
    @pub = @pub.includes(:reviews).to_a.sort! do |a, b| 
      (b.rating <=> a.rating).nonzero? || 
      (b.reviews.size <=> a.reviews.size).nonzero? || 
      a.name <=> b.name
    end
  end

  def where_province(province)
    @pub = @pub.where('lower(province) = ?', province)
  end

  def where_name_is_not(name)
    @pub = @pub.where('name <> ?', name)
  end

  def where_type(types)
    @pub = @pub.where(pub_type: types)
  end
  
  def where_region(region)
    @pub = @pub.where(region: region)
  end

  def include_images
    @pub = @pub.includes(:comments)
  end

  def include_beers_count
    @pub = @pub.select("pubs.*, COUNT(beers.id) as beers_count").joins("LEFT OUTER JOIN beers ON (beers.pub_id = pubs.id)").group("pubs.id")
  end

  def includes_avatar
    @pub = @pub.includes(:user => :avatar)
  end

  def limit(count)
    @pub = @pub.limit(count)
  end

  def order(clause)
    @pub = @pub.order(clause)
  end

  def where_have_cover
    @pub = @pub.where(comments: { image_type: 'Cover' })
  end
end