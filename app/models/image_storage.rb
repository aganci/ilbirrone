require 'ftp_client'
require 'pathname'
require 'RMagick'

class ImageStorage
  def self.upload_comment_image(image_file, comment)
    ImageStorage.new(CommentRemoteImage.new).upload(image_file, comment)
  end

  def self.upload_model(image_file, comment)
    ImageStorage.new(ModelRemoteImage.new(comment.commentable, comment.commentable_type)).upload(image_file, comment)
  end

  def self.upload_avatar(image_file, comment)
    ImageStorage.new(AvatarRemoteImage.new(comment.commentable)).upload(image_file, comment)
  end


  include Magick

  def initialize(remote_image)
    @remote_image = remote_image
  end

  def upload(image_file, image_model)
    @image_file = image_file
    @image_model = image_model
    @file_name = generate_file_name
    image = write_file
    FtpClient.image_server.upload_binary_file(temp_file_path, remote_path)
    update_image_model(image)
    delete_temp_file
  end

  private
  def write_file
    File.open(temp_file_path, 'wb') do |file|
      file.write(@image_file.read)
    end
    image = Image::read(temp_file_path).first
    image = @remote_image.prepare_image(image)
    image.write temp_file_path
    image
  end

  def update_image_model(image)
    @image_model.path = remote_path
    @image_model.width = image.columns
    @image_model.height = image.rows
  end

  def generate_file_name
    path = Pathname.new(@image_file.original_filename)
    "#{SecureRandom.hex(16)}#{path.extname}"
  end

  def delete_temp_file
    File.delete(temp_file_path)
  end

  def temp_file_path
    Rails.root.join "tmp", @file_name
  end

  def remote_path
    @remote_image.path(@file_name)
  end
end