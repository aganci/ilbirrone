class Visitor < ActiveRecord::Base
  def self.find_or_create_by(user, pub)
    Visitor.where(user_id: user.id).where(pub_id: pub.id).present? || Visitor.create(user: user, pub: pub)
  end

  belongs_to :user
  belongs_to :pub

  attr_accessible :user, :pub, :latitude, :longitude, :confirmed
end
