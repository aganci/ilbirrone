class Pub < ActiveRecord::Base
  include FriendlyId
  include Cover
  include  UserPointsUpdate  
  
  geocoded_by :full_address

  attr_accessible :address, :city, :name, :province, :postcode, :phone, :email, :website, 
    :closing_days, :status, :description, :pub_type, :region, :latitude, :longitude, :country
  has_many :reviews, -> { order(date: :desc) }, :dependent => :destroy
  has_many :beers, -> { order(:name) }, :dependent => :destroy
  has_many :events, class_name: "PubEvent", :dependent => :destroy
  has_many :comments, -> { order(created_at: :desc) }, as: :commentable, :dependent => :destroy
  has_many :visitors, :dependent => :destroy
  belongs_to :user

  friendly_id :formatted_friendly_id, use: [:slugged, :finders]

  validates :name, presence: true
  validates :address, presence: true
  validates :city, presence: true
  validates :province, presence: true
  validates :user, presence: true

  def images
    covers
  end

  def full_address
    "#{self.address} - #{self.postcode} #{self.city_with_province}"
  end

  def city_with_province
    return self.city if city.downcase == province.downcase
    "#{self.city} (#{self.province})"
  end

  def rating
    Rating.calculate(reviews)
  end

  def formatted_friendly_id
    "#{self.name} #{self.city}"
  end

  def submit!
    self.status = 'submitted'
    save
  end

  def approved?
    self.status == 'approved'
  end

  def approve!
    self.status = 'approved'
    save
  end

  def set_location(latitude, longitude)
    self.latitude = latitude
    self.longitude = longitude
  end

  def has_coordinates?
    self.latitude.present? && self.longitude.present?
  end

  def photo_description
    "#{pub_type} #{name}"
  end

  def default_photo
    'no-photo.jpg'
  end

  def visitor_count
    anonymous_visits = visitors.select {|v| v.confirmed? && v.user.nil? }.size
    visits = visitors.select {|v| v.confirmed? && v.user.present? }.group_by(&:user).size
    anonymous_visits + visits
  end

  def visitor?(user)
    return false if user.nil?
    visitors.select { |v| v.confirmed? && v.user.present? && v.user.id == user.id }.present?
  end
end
