# encoding: utf-8

class Beer < ActiveRecord::Base
  include FriendlyId
  include Cover
  include UserPointsUpdate

  attr_accessible :name, :pub_id, :user_id, :user, :alcohol_by_volume, :color, :fermentation, :family, :style, 
    :serving_temperature, :beer_type, :description

  friendly_id :formatted_friendly_id, use: [:slugged, :finders]
  
  belongs_to :pub
  belongs_to :user
  has_many :reviews, -> { order(created_at: :desc) }, class_name: "BeerReview", :dependent => :destroy
  has_many :comments, -> { order(created_at: :desc) }, as: :commentable, :dependent => :destroy


  validates :name, presence: true
  validates :alcohol_by_volume, presence: true
  validates_format_of :alcohol_by_volume, :with => /\A\d+(,\d)?\z/
  validates :color, presence: true
  
  def images
    covers
  end

  def formatted_friendly_id
    "#{name} #{pub.name}"
  end

  def similar
    BeerQuery.new.same_type(self, 6)
  end

  def rating
    Rating.calculate(reviews)
  end

  def photo_description
    "#{name} #{pub.name}"
  end

  def default_photo
    "beer.jpg"
  end

  def self.styles
    [
      "Ale Belghe & Francesi",
      "American Pale Ales",
      "Barleywine & Imperial Stout",
      "Bitter & English Pale Ale",
      "Brown Ale",
      "English & Scottish Strong Ale",
      "India Pale Ale", 
      "Koelsch & Altbier",
      "Lambic & Ale Belghe Acidule",
      "Light Ale",
      "Porter",
      "Scottish Ale",
      "Stout",
      "Strong Belgian Ale",
      "Birre di Grano",
      "Bock",
      "European Dark Lager",
      "European Pale Lager",
      "German Amber Lager",
      "Lager Americane",
      "Birre Affumicate",
      "Birre alla frutta",
      "Birre con Spezie o Erbe",
      "Specialità",
      "Idromele",
      "Sidro"
    ].sort
  end

  def self.types
    [
      "Belgian Pale Ale",
      "Belgian Speciality Ale",
      "Bière de Garde",
      "Saison",
      "Witbier",
      "American Amber Ale",
      "American Pale Ale",
      "American-style Barleywine",
      "English-style Barleywine",
      "Russian Imperial Stout",
      "Ordinary Bitter",
      "Special or Best Bitter",
      "Strong Bitter/English Pale Ale",
      "American Brown Ale",
      "Mild",
      "Northern Brown",
      "Southern Brown",
      "Old Ale",
      "Strong Scotch Ale (wee heavy)",
      "India Pale Ale", 
      "Dusseldorfer Altbier",
      "Koelsch-style Ale",
      "Northern German Altbier",
      "Flanders Red Ale",
      "Fruit Lambic-style Ale",
      "Gueuze-style Ale",
      "Oud Bruin Ale",
      "Straight Lambic-style Ale",
      "American Wheat",
      "Blonde Ale",
      "Cream Ale",
      "Brown Porter",
      "Robust Porter",
      "Export 80",
      "Heavy 70",
      "Light 60",
      "Dry Stout",
      "Foreign Extra Stout",
      "Oatmeal Stout",
      "Sweet Stout",
      "Belgian Strong Dark Ale",
      "Belgian Strong Golden Ale",
      "Dubbel",
      "Tripel",
      "Bavarian Dunkelweizen",
      "Bavarian Weizen",
      "Berliner Weisse",
      "Weizenbock",
      "Doppelbock",
      "Eisbock",
      "Helles Bock/Maibock",
      "Traditional Bock",
      "Munich Dunkel",
      "Schwarzbier",
      "Bohemian Pilsner",
      "Dortmunder Export",
      "Munich Helles",
      "Northern German Pilsner",
      "Oktoberfest/Märzen",
      "Vienna Lager",
      "Classic American Pilsner",
      "Dark",
      "Light/Standard/Premium",
      "Malt Liquor",
      "Classic Rauchbier (birre affumicate)",
      "Other Smoked Beer (altre birre affumicate)",
      "Fruit Beer (birre alla frutta)",
      "Spice/Herb/Vegetable (birre speziate/con erbe)",
      "Castagna (Chestnut)",
      "Sperimentali e storiche",
      "Gose",
      "Zoigl",
      "Mead (Idromele/Fermentato di Miele)",
      "Cider & Perry (Sidro di Mele e di Pere)"
      ].sort
  end
end