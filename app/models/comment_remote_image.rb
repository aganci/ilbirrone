class CommentRemoteImage
  def path(local_file_name)
    "comments/#{local_file_name}"
  end

  def prepare_image(image)
    image.change_geometry "500>" do |cols, rows, img|
      img.resize! cols, rows
    end
  end
end