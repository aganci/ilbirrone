class PublicEvent < Event
  attr_accessible :location, :address, :city, :province, :region, :postcode

  validates :location, presence: true
  validates :address, presence: true
  validates :city, presence: true
  validates :province, presence: true
  validates :region, presence: true
  validates :postcode, presence: true

  def full_address
    "#{address} - #{postcode} #{city_with_province}"
  end

  def city_with_province
    return city if city.downcase == province.downcase
    "#{city} (#{province})"
  end

  def set_location(latitude, longitude)
    self.latitude = latitude
    self.longitude = longitude
  end

  def has_coordinates?
    self.latitude.present? && self.longitude.present?
  end
end