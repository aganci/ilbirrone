class GeolocatedPub
  def initialize pub, latitude, longitude
    @pub = pub
    @latitude = latitude
    @longitude = longitude
  end

  def to_hash(view)
    @view = view

    {
      pub_type: @pub.pub_type,
      name: @pub.name,
      path: view.pub_url(@pub, host: 'www.beerky.it'),
      city: @pub.city,
      distance: @pub.distance_from([@latitude, @longitude], :km),
      rating: @pub.rating,
      image_url: photo_url,
      latitude: @pub.latitude,
      longitude: @pub.longitude,
      description: @pub.description,
      phone: @pub.phone,
      closing_days: @pub.closing_days,
      full_address: @pub.full_address,
      image_width: width,
      image_height: height
    }
  end

  def photo_url
    return "http://ilbirrone.it/#{@pub.images.first.path}" if @pub.cover?
    "#{@view.request.protocol}#{@view.request.host_with_port}#{ActionController::Base.helpers.asset_path 'no-photo.jpg'}"
  end

  def width
    return @pub.cover.width if @pub.cover?
    500
  end
  
  def height
    return @pub.cover.height if @pub.cover?
    335
  end
end