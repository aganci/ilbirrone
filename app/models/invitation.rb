class Invitation

  include ActiveModel::Validations
  include ActiveModel::Conversion

  class << self
    def i18n_scope
      :activerecord
    end
  end

  attr_accessor :email, :friend

  validates :email, :friend, :presence => true
  validates :email, :format => { :with => %r{@} }, :allow_blank => false
  
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end