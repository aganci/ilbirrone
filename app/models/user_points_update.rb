module UserPointsUpdate
 extend ActiveSupport::Concern

  included do
    after_save :update_user_points
    after_destroy :update_user_points
  end

  def update_user_points
    user.reload
    UserPoints.calculate_on(user)
    user.save!
  end
end