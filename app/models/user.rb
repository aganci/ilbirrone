class User < ActiveRecord::Base
  
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    return user if user.present?

    password = Devise.friendly_token[0,20] 
    user = User.create!(
      username: auth.extra.raw_info.name,
      provider: auth.provider,
      uid: auth.uid,
      email: auth.info.email,
      password: password,
      password_confirmation: password,
      privacy_policy_accepted: true
      )
    Notification.notify_user_registration(user)
    user
  end 

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end  

  def self.rankings(search, pagination_options)
    @user = User
    @user = @user.where('lower(username) like ?', "%#{search.downcase}%") if search.present?
    @user = @user.where(admin: false).includes(:avatar).order('points DESC').paginate(pagination_options)
  end

  def self.preload_data(id)
    User.includes(:pubs, :reviews, :beer_reviews, { :events => :pub }, { :visitors => {:pub => :reviews }} ).find(id)
  end

  def self.most_recent(count)
    User.limit(count).includes(:avatar).order('created_at DESC')
  end


  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable

  before_save :init_user_points

  attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :privacy_policy_accepted, :admin, :provider, :uid, :points, :city

  has_many :reviews, -> { order(date: :desc) }
  has_many :pubs, -> { order(created_at: :desc) }
  has_many :beers
  has_many :beer_reviews, -> { order(created_at: :desc) }
  has_many :events, -> { order(start_date: :desc) }
  has_one :avatar, -> { where(['image_type = ?', 'Cover']) }, as: :commentable, class_name: 'Comment', dependent: :destroy
  has_many :comments, -> { where(['commentable_type <> ?', 'User']).order(created_at: :desc) }
  has_many :user_comments, -> { where('image_type is null') }, as: :commentable, class_name: 'Comment', dependent: :destroy
  has_many :visitors

  validates_length_of :username, :minimum => 3, :allow_blank => false
  validates_inclusion_of :privacy_policy_accepted, in: [true], allow_nil: false

  def init_user_points
    UserPoints.calculate_on(self)
  end

  def name
    username
  end

  def images
    return [avatar] if avatar.present?
    []
  end

  def comments_with_image
    (comments.to_a + user_comments.to_a).select {|comment| comment.has_image?}
  end

  def text_only_comments
    comments.reject {|comment| comment.has_image?}
  end

  def photo_description
    username
  end

  def default_photo
    'avatar.jpg'
  end

  def approved_pubs
    pubs.select {|pub| pub.approved?}
  end

  def visited_pubs
    self.visitors.select { |visit| visit.pub.has_coordinates? }.map {|v| v.pub}
  end
end
