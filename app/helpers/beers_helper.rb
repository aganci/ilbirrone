module BeersHelper
  def cloud_link(tipology, weight)
    link_to tipology, beers_path(tipologia: tipology), class: "cloud_#{weight}"
  end
end