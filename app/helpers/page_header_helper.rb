module PageHeaderHelper
  def page_title
    "#{og_title} - #{t('site_name')}".html_safe
  end
  
  def og_title
    return content_for(:title).html_safe if content_for?(:title)
    t('home_page.show.title').html_safe
  end

  def render_breadcrumb(options)
    return render_breadcrumb [Breadcrumb.new(options)] if options.kind_of?(String)
    render partial: 'shared/breadcrumb', locals: {items: options} if options.kind_of?(Array)
  end
end