module RedirectToCurrentPageAfterSignInHelper
  def store_location
    
    if request.fullpath =~ /\/utenti\/\d+/
      session[:previous_url] = request.fullpath
      return
    end

    return if request.fullpath =~ /\/utenti/
    return if request.xhr?
    session[:previous_url] = request.fullpath
  end

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end
  
  def after_sign_up_path_for(resource)
    session[:previous_url] || root_path
  end  
end