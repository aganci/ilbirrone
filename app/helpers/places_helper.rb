module PlacesHelper
  def render_cities
    Place.cities.map {|city| format_city(city) }.to_json.html_safe
  end

  def format_city(city)
    return "#{city[1]}, #{city[2]}" if city[0] == city[1]
    city.join(", ")
  end
end