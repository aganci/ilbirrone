module GeolocationHelper
  def render_where_are_you_question(request)
    if request.user_agent =~ /Mobile|webOS/ && !cookies[:where_are_you].present?
      cookies[:where_are_you] = { value: true, expires: 12.hour.from_now }
      session[:return_to] = request.original_url
      render partial: 'shared/where_are_you_question'
    end
  end
end