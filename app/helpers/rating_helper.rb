require 'rating_view'

module RatingHelper
  def big_rating_tag(score)
    images = {
      0.0 => ["raty/not-rated-big.png", "0 su 5 stelle"],
      0.5 => ["raty/half-star-big.png", "0,5 su 5 stelle"],
      1.0 => ["raty/one-star-big.png", "1 su 5 stelle"],
      1.5 => ["raty/one-half-star-big.png", "1,5 su 5 stelle"],
      2.0 => ["raty/two-stars-big.png", "2 su 5 stelle"],
      2.5 => ["raty/two-half-stars-big.png", "2,5 su 5 stelle"],
      3.0 => ["raty/three-stars-big.png", "3 su 5 stelle"],
      3.5 => ["raty/three-half-stars-big.png", "3,5 su 5 stelle"],
      4.0 => ["raty/four-stars-big.png", "4 su 5 stelle"],
      4.5 => ["raty/four-half-stars-big.png", "4,5 su 5 stelle"],
      5.0 => ["raty/five-stars-big.png", "5 su 5 stelle"],
    }

    image_tag images[score.to_f].first, alt: images[score.to_f].second, class: 'big-rating' 
  end

  def small_rating_tag(score)
    images = {
      0.0 => ["raty/not-rated.png", "0 su 5 stelle"],
      0.5 => ["raty/half-star.png", "0,5 su 5 stelle"],
      1.0 => ["raty/one-star.png", "1 su 5 stelle"],
      1.5 => ["raty/one-half-star.png", "1,5 su 5 stelle"],
      2.0 => ["raty/two-stars.png", "2 su 5 stelle"],
      2.5 => ["raty/two-half-stars.png", "2,5 su 5 stelle"],
      3.0 => ["raty/three-stars.png", "3 su 5 stelle"],
      3.5 => ["raty/three-half-stars.png", "3,5 su 5 stelle"],
      4.0 => ["raty/four-stars.png", "4 su 5 stelle"],
      4.5 => ["raty/four-half-stars.png", "4,5 su 5 stelle"],
      5.0 => ["raty/five-stars.png", "5 su 5 stelle"],
    }

    image_tag images[score.to_f].first, alt: images[score.to_f].second, class: 'small-rating' 
  end
end
