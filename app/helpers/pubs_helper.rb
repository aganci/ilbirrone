module PubsHelper
  def pub_cloud_link(province, weight)
    link_to province, pubs_path(provincia: province), class: "cloud_#{weight}"
  end
  
  def brewpub_cloud_link(province, weight)
    link_to province, brewpub_path(provincia: province), class: "cloud_#{weight}"
  end

  def pub_owner?
    current_user && current_user.admin?
  end

  def webmaster_code(pub, image_name, width, height)
    link_to pub_url(pub) do
      tag :img, 
        src: URI.join(root_url, image_path(image_name)), 
        alt: 'Segnalato su Beerky', 
        width: width, height: height
    end
  end

  def format_location(pub)
    return "(#{pub.latitude}, #{pub.longitude})" if pub.latitude.present?
    "non geocalizzato"
  end

  def marker_image(pub)
    if pub.reviews.size > 0
      return URI.join(root_url, image_path('marker-con-recensioni.png'))
    end
    return URI.join(root_url, image_path('marker.png'))
  end

  def user_with_points(user)
      [
        link_to(user.username, user_path(user)), tag(:br), 
        user.points, "&nbsp;", t("point", count: user.points)
      ].join.html_safe
  end
end