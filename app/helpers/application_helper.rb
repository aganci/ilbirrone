module ApplicationHelper
  include RatingHelper
  include SocialHelper
  include PageHeaderHelper
  include BootstrapFlashHelper

  def render_description(text)
    text.gsub("\n", "<br>").html_safe
  end

  def link_to_pubs_in(city)
    link_to city, pubs_path(provincia: city)
  end

  def link_to_microbreweries_in(region)
    link_to region, microbreweries_path(region: region)
  end

  def asset_url(image_name)
    "#{request.protocol}#{request.host_with_port}#{asset_path(image_name)}"
  end
end
