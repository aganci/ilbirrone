module PicturesHelper
  def render_photo(imageable)
    tag :img, src: default_photo_url(imageable), alt: imageable.photo_description
  end

  def default_photo_url(imageable)
    photo_url(imageable.images.first)
  end
  
  def photo_url(image)
    "http://ilbirrone.it/#{image.path}"
  end
end