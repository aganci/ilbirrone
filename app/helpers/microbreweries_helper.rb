module MicrobreweriesHelper
  def region_tag_link_to(region, weight)
    link_to region, microbreweries_path(region: region), class: "cloud_#{weight}"
  end
end