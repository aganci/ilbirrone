module AutocompleteHelper
  def autocomplete_search_text(input_selector)
    search_text_script(input_selector, autocomplete_path)
  end

  def autocomplete_search_pub(input_selector)
    search_text_script(input_selector, autocomplete_pub_new_event_path)
  end
  
  def autocomplete_search_pub_new_review(input_selector)
    search_text_script(input_selector, autocomplete_pub_new_review_path)
  end

  private
  def search_text_script(selector, url)
    raw(search_text_code(selector, url))
  end

  def search_text_code(selector, url)
<<-SCRIPT
var autocomplete_values;
$('#{selector}').typeahead({
    source: function (query, process) {
        return $.post('#{url}', { query: query }, function (data) {
            autocomplete_values = data;
            values = $.map(data, function(element, index) {
              return element[0];
              });
            return process(values);
        });
    },
    minLength: 2,
    items: 20,
    updater: function(item) {
      $.each(autocomplete_values, function(index, value) {
        if (item == value[0]) {
          window.location = value[1];
        }
        })
    }
});
SCRIPT
  end
end