#!/bin/sh
set -e
# Example init script, this can be used with nginx, too,
# since nginx and unicorn accept the same signals

# Feel free to change any of the following variables for your app:
TIMEOUT=${TIMEOUT-60}
APP_ROOT=/home/ilbirrone/current
PID=$APP_ROOT/tmp/pids/unicorn.pid

set -u

old_pid="$PID.oldbin"

cd $APP_ROOT || exit 1

sig () {
  test -s "$PID" && kill -$1 `cat $PID`
}

oldsig () {
  test -s $old_pid && kill -$1 `cat $old_pid`
}

if sig USR2 && sleep 2 && sig 0 && oldsig QUIT
then
  n=$TIMEOUT
  while test -s $old_pid && test $n -ge 0
  do
    printf '.' && sleep 1 && n=$(( $n - 1 ))
  done
  echo

  if test $n -lt 0 && test -s $old_pid
  then
    echo >&2 "$old_pid still exists after $TIMEOUT seconds"
    exit 1
  fi
  exit 0
fi

echo >&2 "Couldn't upgrade, starting unicorn instead"
bundle exec unicorn -c $APP_ROOT/config/unicorn.rb -E $1 -D
