require 'ftp_client'

namespace :ftp do
  desc "delete file older than 90 days from backup ftp"
  task :delete_old_backups => :environment do
    FtpClient.backup_server.execute_remote_operation do |ftp|
      files = ftp.nlst
      files.each do |f|
        next if (f == "." || f == "..")
        next if (Date.today - ftp.mtime(f).to_date) < 90
        puts "deleting #{f} #{ftp.mtime(f).to_date}"
        ftp.delete(f)
      end
    end
  end
end
