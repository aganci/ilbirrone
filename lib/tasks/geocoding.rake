require 'map'

namespace :geocoding do
  desc "geocoding pubs"
  task :pubs => :environment do
    Pub.all.each do |pub|
      location = Map.geocode(pub)
      puts pub.name
      if location.success
        pub.save!
        puts pub.full_address
        puts location.address
      else
        puts location.status
        puts "Invalid address!"
      end
      puts "============================================="
      sleep(0.5)
    end
  end
end
