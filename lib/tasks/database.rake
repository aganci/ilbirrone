require 'database'

namespace :database do
  desc "backup database and copy via ftp"
  task :backup_to_ftp => :environment do
    Database.new.backup_to_ftp
  end

  desc "restore database from ftp"
  task :restore_from_ftp => :environment do
    Database.new.restore_from_ftp
  end

  desc "restore database from file"
  task :restore, [:file_name] => [:environment] do |t, args|
    Database.new.restore(args[:file_name])
  end
end
