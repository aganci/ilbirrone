namespace :users do
  desc "update earned user points"
  task :update_points => :environment do
    User.all.each do |user|
      UserPoints.calculate_on(user)
      user.save!
    end
  end

end
