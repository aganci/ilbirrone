require 'sitemap'

namespace :sitemap do
  desc "generate sitemap"
  task :generate => :environment do
    sitemap = Sitemap.new

    sitemap.add_url controller: 'beers', action: 'index', priority: 1.0
    
    sitemap.add_url controller: 'pubs', action: 'index', priority: 1.0
    sitemap.add_url controller: 'pubs', action: 'provinces', priority: 1.0
    sitemap.add_url controller: 'pubs', action: 'microbreweries', priority: 1.0
    sitemap.add_url controller: 'pubs', action: 'recent_reviews', priority: 0.8
    sitemap.add_url controller: 'pubs', action: 'curious', priority: 0.8
    
    sitemap.add_url controller: 'brewpubs', action: 'index', priority: 1.0
    sitemap.add_url controller: 'brewpubs', action: 'recent_reviews', priority: 0.8

    sitemap.add_url controller: 'events', action: 'index', priority: 1.0


    PubQuery.new.provinces('Birreria').each do |province|
      sitemap.add_url controller: 'pubs', action: 'index', provincia: province, priority: 0.8     
    end
    
    PubQuery.new.provinces('Brewpub').each do |province|
      sitemap.add_url controller: 'brewpubs', action: 'index', provincia: province, priority: 0.8
    end
    
    PubQuery.new.regions('Microbirrificio').each do |region|
      sitemap.add_url controller: 'pubs', action: 'microbreweries', region: region, priority: 0.8
    end

    BeerQuery.new.types.each do |beer_type|
       sitemap.add_url controller: 'beers', action: 'index', tipologia: beer_type, priority: 0.7      
    end
    
    PubQuery.new.where_status('approved').each do |pub|
      sitemap.add_url controller: 'pubs', action: 'show', id: pub.friendly_id
      pub.beers.each do |beer|
        sitemap.add_url controller: 'beers', action: 'show', id: beer.friendly_id
      end
    end

    Event.all.each do |event|
        sitemap.add_url controller: 'events', action: 'show', id: event.friendly_id
    end

    File.write('public/sitemap.xml', sitemap.to_s)
  end
end
