require 'madmimi'

namespace :madmimi do
  desc "import all pubs contacts"
  task :import_pubs => :environment do
    mimi = MadMimi.new("ilbirrone@gmail.com", "0eb63591ac945b107a6db90fa949c36f")
    Pub.all.each do |pub|
      next if pub.email.blank?
      mimi.add_user(
        email: pub.email,
        name: pub.name,
        website: "www.beerky.it#{Rails.application.routes.url_helpers.pub_path(pub)}",
        add_list: 'locali')

      puts "#{pub.name} inserito"
    end
  end

  desc "import all users"
  task :import_users => :environment do
    mimi = MadMimi.new("ilbirrone@gmail.com", "0eb63591ac945b107a6db90fa949c36f")
    User.all.each do |user|
      mimi.add_user(
        email: user.email,
        name: user.username,
        profile: "www.beerky.it#{Rails.application.routes.url_helpers.user_path(user)}",
        add_list: 'iscritti a ilBirrone')

      puts "#{user.username} inserito"
    end
  end
end
