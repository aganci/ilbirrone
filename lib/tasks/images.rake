require 'images'

namespace :images do
  desc "backup images from ftp"
  task :backup => :environment do
    Images.new.backup
  end
end
