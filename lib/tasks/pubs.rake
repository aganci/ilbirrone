require 'popular_pubs'

namespace :pubs do
  desc "update popular pubs using google analytics data"
  task :update_popular => :environment do
    Ranking.delete_all
    pubs = PopularPubs.new(Pub).find(Date.yesterday, Date.yesterday)
    position = 1
    pubs.select{|p| p.cover?}.select{|p| p.approved?}.take(10).each do |pub|
      Ranking.create(pub: pub, position: position)
      position += 1
    end
  end
end
