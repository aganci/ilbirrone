require 'net/ftp'

class FtpClient
  def self.image_server
    FtpClient.new(Rails.application.config.ftp_server, Rails.application.config.ftp_server_port, 'ilbirrone', 'Marina1973@')
  end

  def self.backup_server
    FtpClient.new('ftpback-rbx2-107.ovh.net', 21, 'vps30051.ovh.net',  'bVVbwxfsruJ')
  end

  attr_reader :ftp_server, :ftp_server_port, :username, :password

  def initialize(ftp_server, ftp_server_port, username, password)
    @ftp_server = ftp_server
    @ftp_server_port = ftp_server_port
    @username = username
    @password = password
  end

  def upload_binary_file(local_file, remote_file)
    execute_remote_operation do |ftp|
      ftp.putbinaryfile(local_file, remote_file)
    end
  end

  def download_binary_file(remote_file, local_file)
    execute_remote_operation do |ftp|
      ftp.getbinaryfile(remote_file, local_file)
    end
  end

  def delete_file(remote_file)
    execute_remote_operation do |ftp|
      begin
        ftp.delete(remote_file)
      rescue Net::FTPPermError
        # don't raise exception when the file don't exist
      end
    end
  end

  def execute_remote_operation(&block)
    ftp = Net::FTP.new
    ftp.connect(ftp_server, ftp_server_port)
    ftp.login(username, password)
    ftp.passive = true
    
    block.call(ftp)

    ftp.close
  end
end