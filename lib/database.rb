require_relative 'ftp_client'

class Database
  attr_reader :ftp_client

  def initialize(ftp_client = FtpClient.backup_server)
    @ftp_client = ftp_client
  end

  def backup_to_ftp
    puts %x(pg_dump -Fc ilbirrone > #{temporary_file_name})
    ftp_client.upload_binary_file(temporary_file_name, default_file_name)
    delete_temporary_file
  end

  def restore_from_ftp
    ftp_client.download_binary_file(default_file_name, temporary_file_name)
    restore(default_file_name)
    delete_temporary_file
  end

  def restore(file_name)
    %x(pg_restore --verbose --clean --no-acl --no-owner -d #{database_name} #{file_name})
  end

  private
  def delete_temporary_file
    puts %x(rm #{temporary_file_name})
  end

  def default_file_name
    "#{Date.today.strftime("%Y-%m-%d")}-ilbirrone.dump"
  end

  def temporary_file_name
    "./tmp/#{default_file_name}"
  end

  def database_name
    config = Rails.configuration.database_configuration
    config[Rails.env]["database"]
  end
end