class ContentTypeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.nil?
    return if options[:content_type].include?(value.content_type)
    
    record.errors[attribute] << (options[:message] || "is not a #{options[:content_type]} file")
  end
end