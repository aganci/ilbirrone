require_relative 'ftp_client'

class Images
  def backup
    puts %x(wget -r --ftp-user=#{FtpClient.image_server.username} --ftp-password=#{FtpClient.image_server.password} -P #{temp_directory} ftp://ftp.ilbirrone.it)
    puts %x(zip -r #{default_file_path} #{download_directory})
    FtpClient.backup_server.upload_binary_file(default_file_path, default_file_name)
    puts %x(rm -rf #{download_directory})
    puts %x(rm #{default_file_path})
  end

  def download_directory
    temp_directory + "ftp.ilbirrone.it"
  end

  def temp_directory
    Rails.root.join('tmp/')
  end

  def default_file_path
    temp_directory + default_file_name
  end

  def default_file_name
    "#{Date.today.strftime("%Y-%m-%d")}-ilbirrone-images.zip"
  end
end