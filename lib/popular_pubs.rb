require 'google/api_client'

class PopularPubs
  attr_reader :model

  def initialize(model)
    @model = model
  end

  def find(from_date, to_date)
    client = authorize_client
    analytics = client.discovered_api('analytics', 'v3')

    result = client.execute(
      api_method: analytics.data.ga.get, 
      parameters: {
        'ids' => 'ga:62527313',
        'start-date' => format_date(from_date),
        'end-date' => format_date(to_date),
        'metrics' => 'ga:visitors',
        'dimensions' => 'ga:pagePath',
        'filters' => 'ga:pagePath=~^/birrerie/',
        'sort' => '-ga:visitors'
        })

    pubs = []

    result.data.rows.each do |row|
      begin
        pub = model.find(extract_id(row[0]))
        pubs << pub
      rescue ActiveRecord::RecordNotFound
      end
    end
    pubs
  end

  private
  def format_date(date)
    date.strftime("%Y-%m-%d")
  end

  def key
    Google::APIClient::KeyUtils.load_from_pkcs12(Rails.root.join('lib/data').join('3f4190b0ab7a6f440a25f196d3e0014d858aaaea-privatekey.p12'), 'notasecret')
  end

  def authorize_client
    client = Google::APIClient.new(:application_name => 'ilBirrone', :application_version => '1.0.0')
    client.authorization = Signet::OAuth2::Client.new(
      :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
      :audience => 'https://accounts.google.com/o/oauth2/token',
      :scope => 'https://www.googleapis.com/auth/analytics.readonly',
      :issuer => '36203545598-cj9tavi3dj1uokkd4uj8k15s607rmqvt@developer.gserviceaccount.com',
      :signing_key => key)

    client.authorization.fetch_access_token!
    client
  end

  def extract_id(page_path)
    page_path.split('/').last
  end
end