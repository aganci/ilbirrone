class RatingView
  def initialize(template)
    @template = template
  end

  def render_script(tag_id, target_id, score)
    @template.javascript_tag do
      @template.raw("render_editable_rating('#{tag_id}', '#{target_id}', #{score || 0});")
    end
  end
end