require 'active_support/all'

class Map
  def self.geocode(pub)
      address = pub.full_address
      response = Net::HTTP.get_response(request_uri(address))
      json = ActiveSupport::JSON.decode(response.body.to_s)
      status = parse_status(json)
      
      if (status == "OK")
        location = parse_location(json)
        response = OpenStruct.new(latitude: location["lat"].to_f, longitude: location["lng"].to_f, success: true, address: parse_address(json))
        pub.set_location(response.latitude, response.longitude)
        return response
      end

      OpenStruct.new(success: false, status: status)
  end

  private
  def self.request_uri(address)
      uri = URI.parse("http://maps.googleapis.com/maps/api/geocode/json?address=" + CGI.escape(address) + "&sensor=false&language=it")
  end

  def self.parse_location(json)
    json["results"][0]["geometry"]["location"]
  end

  def self.parse_status(json)
    json["status"]
  end

  def self.parse_address(json)
    json["results"][0]["formatted_address"]
  end
end