class Sitemap
  def initialize
    @builder = Nokogiri::XML::Builder.new do |xml|
      xml.urlset("xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9") do |xml|
        @xml = Nokogiri::XML::Builder.new({}, xml.parent)
      end
    end
  end

  def add_url(options = {})
    options = {only_path: false, host: "www.beerky.it"}.merge(options)
    priority = options[:priority]
    options.delete(:priority)

    @xml.url do |xml|
      xml.loc_ Rails.application.routes.url_for(options)
      xml.priority_ priority if priority.present?
    end
  end 

  def to_s
    @builder.to_xml
  end
end
