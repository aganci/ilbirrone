# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'ilbirrone'
set :repo_url, 'git@bitbucket.org:aganci/ilbirrone.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :deploy_to, "/home/ilbirrone"

set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_pid, "#{shared_path}/pids/unicorn.pid"

after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  task :restart do
    on roles(fetch(:unicorn_roles)) do
      within current_path do
        sig_pid("USR2")
        execute :sleep, 2
        sig_pid("0")
        sig_old_pid("QUIT")
        
        info "waiting for #{fetch(:unicorn_pid)}.oldbin} to be deleted"
        wait_for_old_process
      end
    end
  end

  def wait_for_old_process
    timeout = 120
    while test("[ -s #{fetch(:unicorn_pid)}.oldbin ]") && timeout > 0 do
      execute :sleep, 1
      timeout = timeout - 1
    end

    if timeout == 0 
      puts "#{fetch(:unicorn_pid)}.oldbin still exists after #{timeout} seconds"
      execute :exit, 1
    end

  end

  def sig_pid(signal)
    if test("[ -s #{fetch(:unicorn_pid)} ]") 
      execute :kill, "-s #{signal}", pid
    end
  end
  
  def sig_old_pid(signal)
    if test("[ -s #{fetch(:unicorn_pid)}.oldbin ]") 
      execute :kill, "-s #{signal}", pid_oldbin
    end
  end

  def pid
    "`cat #{fetch(:unicorn_pid)}`"
  end

  def pid_oldbin
    "`cat #{fetch(:unicorn_pid)}.oldbin`"
  end
end