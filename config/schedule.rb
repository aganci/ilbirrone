every :day, :at => '02:00am' do
  rake "ftp:delete_old_backups"
end

every :day, :at => '03:00am' do
  rake "database:backup_to_ftp"
end

every :sunday, :at => '04:00am' do
  rake "images:backup"
end

every :day, :at => '07:00am' do
  rake "pubs:update_popular"
end