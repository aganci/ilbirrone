Ilbirrone::Application.routes.draw do

  root to: 'home_page#show'

  scope(:path_names => { :new => "nuova", :edit => "modifica" }) do
    resources :pubs, path: 'birrerie' do
      collection do
        get 'cerca', to: 'pubs#search', as: :search
        get 'birrerie-nei-dintorni', to: 'geolocation#nearby', as: :nearby
        get :nearest, to: 'geolocation#nearest_pub'
        get 'recensioni-recenti-birrerie-pub', to: 'pubs#recent_reviews', as: :recent_reviews
        get 'recensioni-recenti-brewpub', to: 'brewpubs#recent_reviews', as: :brewpub_recent_reviews
        get 'birrerie-pub-curiosi', to: 'pubs#curious', as: :curious
        get 'scrivi-recensione-birreria', to: 'pubs#select_for_review', as: :select_for_review
        get 'province-italiane', to: 'pubs#provinces', as: :provinces
        post :send_changes, to: 'changes#send_pub_changes'
      end
      member do
        get :webmaster
        get :change, to: 'changes#pub'
      end
    end
    resources :beers, path: 'birre'
    resources :reviews, path: 'recensioni'
    resources :beer_reviews, path: 'recensioni-birre'

    namespace :admin do
      resources :pubs, path: 'birrerie' do
        member do
          post 'approve'
          get :edit_location
        end
      end
      get 'statistiche', to: 'statistics#index', as: :statistics
      resources :users, path: 'utenti'
      resources :beers, path: 'birre'
      resources :pub_events, path: 'eventi-birrerie'
      resources :public_events, path: 'eventi-pubblici'
      resources :events, path: 'eventi', only: [:index, :destroy]
      resources :reviews, path: 'recensioni'
      resources :comments
      resources :pre_registrations
      resources :visitors, only: [:index]
    end
  end

  resources :comments, only: [:destroy] do
    collection do
      post :upload
      post :upload_avatar
      post :upload_comment
      post :upload_user_comment
    end
  end

  get "microbirrifici" => "pubs#microbreweries", as: :microbreweries
  get "brewpub" => "brewpubs#index", as: :brewpub

  scope(:path_names => { :new => "nuovo", :edit => "modifica" }) do
    devise_for :users, 
      :path => "utenti", 
      :path_names => { 
        :sign_in => 'accedi', 
        :sign_out => 'esci', 
        :sign_up => 'registrati' },
      :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations" }

    resources :events, path: 'eventi', only: [:index, :show] do
      collection do
        get 'seleziona-tipo-evento', to: 'events#select_type', as: :select_type
      end
    end

    resources :public_events, path: 'eventi-pubblici', only: [:new, :create]
    resources :public_events, path: 'eventi', only: [:show]
    resources :pub_events, path: 'eventi-birrerie', only: [:new, :create]
    resources :pub_events, path: 'eventi', only: [:show]
  end

  resources :users, path: 'utenti', only: [:show, :index] do
    collection do
      get :search, to: 'users#search'
    end
  end

  resource :informations, path: 'informazioni', only: [] do
    get 'privacy', to: 'informations#privacy', as: :privacy
    get 'chi-siamo', to: 'informations#who_we_are', as: :who_we_are
  end
  
  resource :contact_us, path: 'contattaci', only: [:new, :create]

  get 'cosa-e-la-birra', to: 'beer_pages#whatis', as: :what_is_beer
  get 'birra-artigianale', to: 'beer_pages#craft_beer', as: :craft_beer
  get 'storia-della-birra', to: 'beer_pages#history', as: :history
  get 'oktoberfest', to: 'beer_pages#oktoberfest', as: :oktoberfest

  get 'sfide', to: 'achievements#index', as: :achievements

  get 'cerca', to: 'search#index', as: :search
  post 'cerca', to: 'search#autocomplete', as: :autocomplete
  post 'cerca-birrerie-nuovo-evento', to: 'search#autocomplete_pub_new_event', as: :autocomplete_pub_new_event
  post 'cerca-birrerie-nuova-recensione', to: 'search#autocomplete_pub_new_review', as: :autocomplete_pub_new_review

  get 'guida-recensione', to: 'reviews#guide', as: :review_guide
  get 'come-inserire-una-recensione', to: 'reviews#how_to_create', as: :review_how_to_create

  resources :homebrewers, path: 'homebrewer', only: [] do
    collection do
      get :pre_registration, path: 'preiscriviti'
      post :invite
    end
  end
  
  resources :pre_registrations

  resources :visitors, only: [] do
    collection do
      get :visit_pub
      post :geolocated_visit_pub
    end
  end

  resources :notifications
end
