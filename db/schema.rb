# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150407082233) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "beer_reviews", force: true do |t|
    t.integer  "beer_id",    null: false
    t.integer  "user_id",    null: false
    t.integer  "rating",     null: false
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "aroma"
    t.string   "appearance"
    t.string   "taste"
    t.string   "palate"
  end

  create_table "beers", force: true do |t|
    t.string   "name",                null: false
    t.integer  "pub_id",              null: false
    t.integer  "user_id",             null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "alcohol_by_volume"
    t.string   "color"
    t.string   "fermentation"
    t.string   "family"
    t.string   "style"
    t.string   "beer_type"
    t.string   "serving_temperature"
    t.text     "description"
    t.string   "slug"
  end

  add_index "beers", ["slug"], name: "index_beers_on_slug", unique: true, using: :btree

  create_table "comments", force: true do |t|
    t.text     "content"
    t.integer  "user_id",          null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "image_type"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.string   "path"
    t.integer  "width"
    t.integer  "height"
  end

  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "events", force: true do |t|
    t.integer  "pub_id"
    t.date     "start_date",  null: false
    t.date     "end_date",    null: false
    t.string   "title",       null: false
    t.text     "description", null: false
    t.string   "website"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id",     null: false
    t.string   "slug"
    t.string   "type"
    t.string   "location"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.string   "region"
    t.string   "postcode"
    t.float    "latitude"
    t.float    "longitude"
  end

  add_index "events", ["slug"], name: "index_events_on_slug", unique: true, using: :btree

  create_table "images", force: true do |t|
    t.string   "path",           null: false
    t.integer  "width",          null: false
    t.integer  "height",         null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "imageable_id"
    t.string   "imageable_type"
  end

  create_table "notifications", force: true do |t|
    t.string   "notification_type", null: false
    t.integer  "pub_id"
    t.integer  "review_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "comment_id"
    t.integer  "event_id"
    t.integer  "beer_id"
    t.integer  "beer_review_id"
  end

  add_index "notifications", ["beer_id"], name: "index_notifications_on_beer_id", using: :btree
  add_index "notifications", ["beer_review_id"], name: "index_notifications_on_beer_review_id", using: :btree
  add_index "notifications", ["comment_id"], name: "index_notifications_on_comment_id", using: :btree
  add_index "notifications", ["event_id"], name: "index_notifications_on_event_id", using: :btree
  add_index "notifications", ["pub_id"], name: "index_notifications_on_pub_id", using: :btree
  add_index "notifications", ["review_id"], name: "index_notifications_on_review_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "pre_registrations", force: true do |t|
    t.integer  "user_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pubs", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "postcode"
    t.string   "phone"
    t.string   "email"
    t.string   "website"
    t.string   "slug"
    t.string   "closing_days"
    t.string   "status"
    t.integer  "user_id"
    t.text     "description"
    t.string   "pub_type"
    t.string   "region"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "country"
  end

  add_index "pubs", ["slug"], name: "index_pubs_on_slug", unique: true, using: :btree

  create_table "rankings", force: true do |t|
    t.integer  "pub_id",     null: false
    t.integer  "position",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "rankings", ["pub_id"], name: "index_rankings_on_pub_id", using: :btree

  create_table "reviews", force: true do |t|
    t.integer  "pub_id"
    t.integer  "user_id"
    t.integer  "rating"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.date     "date"
    t.text     "description"
    t.integer  "food"
    t.integer  "value"
    t.integer  "atmosphere"
    t.integer  "beers"
    t.boolean  "frequenter"
  end

  create_table "users", force: true do |t|
    t.string   "email",                   default: "",    null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "username"
    t.string   "encrypted_password",      default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "privacy_policy_accepted"
    t.boolean  "admin",                   default: false, null: false
    t.string   "provider"
    t.string   "uid"
    t.integer  "points"
    t.string   "city"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "visitors", force: true do |t|
    t.integer  "user_id"
    t.integer  "pub_id",                    null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "confirmed",  default: true
  end

end
