class RemoveNullContraintComments < ActiveRecord::Migration
  def change
    change_column :comments, :content, :string, :null => true
  end
end
