class AddClosingDaysToPubs < ActiveRecord::Migration
  def change
    add_column :pubs, :closing_days, :string
  end
end
