class CreateApprovalNotifications < ActiveRecord::Migration
  class Pub < ActiveRecord::Base
    attr_accessible :status
    belongs_to :user
  end

  class Notification < ActiveRecord::Base
    def self.notify_pub_approval(pub)
      Notification.create!(notification_type: 'approval', pub: pub, user: pub.user)
    end
    attr_accessible :notification_type, :pub, :user

    belongs_to :pub
    belongs_to :user
  end

  def up
    Pub.where(status: 'approved').all.each do |pub|
      n = Notification.notify_pub_approval(pub)
      n.update_attribute :created_at, pub.created_at
    end
  end
end
