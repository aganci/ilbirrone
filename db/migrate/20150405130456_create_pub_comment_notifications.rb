class CreatePubCommentNotifications < ActiveRecord::Migration
  class Comment < ActiveRecord::Base
    belongs_to :user
    belongs_to :commentable, polymorphic: true
  end

  class Pub < ActiveRecord::Base
  end

  class Notification < ActiveRecord::Base
    def self.notify_pub_comment(comment)
      Notification.create!(notification_type: 'pub_comment', pub: comment.commentable, comment: comment, user: comment.user)
    end

    attr_accessible :notification_type, :pub, :user, :comment

    belongs_to :pub
    belongs_to :user
    belongs_to :comment
  end

  def up
    Notification.connection.schema_cache.clear!
    Notification.reset_column_information
    Comment.connection.schema_cache.clear!
    Comment.reset_column_information
    Pub.connection.schema_cache.clear!
    Pub.reset_column_information


    Comment.where(commentable_type: 'Pub', image_type: nil).each do |comment|
      n = Notification.notify_pub_comment(comment)
      n.update_attribute :created_at, comment.created_at
    end
  end
end
