class Image < ActiveRecord::Base
  attr_accessible :path, :width, :height, :imageable, :imageable_id, :imageable_type

  belongs_to :imageable, polymorphic: true
end

class Comment < ActiveRecord::Base
  attr_accessible :user_id, :user, :content, :commentable_id, :commentable_type

  belongs_to :user
  has_one :image, as: :imageable
end

class User < ActiveRecord::Base
  has_one :avatar, as: :commentable, class_name: 'Comment'
end

class ConvertBeerImagesIntoComments < ActiveRecord::Migration
  def up
    Image.find_all_by_imageable_type('Beer').each do |image|
      beer = image.imageable
      comment = beer.comments.build
      comment.image_type = "Cover"
      comment.image = image
      comment.user = beer.user
      comment.created_at = image.created_at
      comment.save!
    end
  end

  def down
  end
end
