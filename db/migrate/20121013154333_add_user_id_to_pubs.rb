class AddUserIdToPubs < ActiveRecord::Migration
  def change
    add_column :pubs, :user_id, :integer
  end
end
