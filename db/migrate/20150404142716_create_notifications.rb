class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :notification_type, null: false
      t.references :pub, index: true
      t.references :review, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
