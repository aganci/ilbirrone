class AddPubTypeToPubs < ActiveRecord::Migration
  class Pub < ActiveRecord::Base
    attr_accessible :pub_type
  end

  def change
    add_column :pubs, :pub_type, :string

    Pub.reset_column_information
    Pub.all.each do |pub|
      pub.update_attributes!(pub_type: "Birreria")
    end
  end
end
