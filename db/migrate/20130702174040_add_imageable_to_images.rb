class AddImageableToImages < ActiveRecord::Migration
  class Image < ActiveRecord::Base
    attr_accessible :pub_id, :beer_id, :path, :imageable_type, :imageable_id
  end

  def up
    add_column :images, :imageable_id, :integer
    add_column :images, :imageable_type, :string
    Image.all.each do |image|
      if image.pub_id.present?
        image.path = "pubs/#{image.path}"
        image.imageable_type = "Pub"
        image.imageable_id = image.pub_id
      end
      
      if image.beer_id.present?
        image.path = "beers/#{image.path}"
        image.imageable_type = "Beer"
        image.imageable_id = image.beer_id
      end

      image.save!
    end
  end
end
