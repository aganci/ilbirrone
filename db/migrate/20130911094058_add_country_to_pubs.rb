class AddCountryToPubs < ActiveRecord::Migration
  
  class Pub < ActiveRecord::Base
    attr_accessible :country
  end

  def up
    add_column :pubs, :country, :string

    Pub.all.each do |pub|
      pub.country = "Italia"
      pub.save!
    end

    change_column :pubs, :country, :string, :null => false
  end

  def down
    remove_column :pubs, :country
  end
end
