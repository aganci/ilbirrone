class AddTypeToEvents < ActiveRecord::Migration
  class Event < ActiveRecord::Base
    attr_accessible :type
  end

  def up
    add_column :events, :type, :string

    Event.all.each do |event|
      event.type = "PubEvent"
      event.save!
    end
  end

  def down
    remove_column :events, :type
  end
end
