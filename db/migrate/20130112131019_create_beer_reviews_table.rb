class CreateBeerReviewsTable < ActiveRecord::Migration
  def change
    create_table :beer_reviews do |t|
      t.integer :beer_id, null: false
      t.integer :user_id, null: false
      t.integer :rating, null: false
      t.text    :content
      
      t.timestamps
    end
  end
end
