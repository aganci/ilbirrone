class ConvertCoverImagesIntoComments < ActiveRecord::Migration
  def up
    Image.find_all_by_imageable_type('Pub').each do |image|
      pub = image.imageable
      comment = pub.comments.build
      comment.image_type = "Cover"
      comment.image = image
      comment.user = pub.user
      comment.created_at = image.created_at
      comment.save!
    end
  end

  def down
  end
end
