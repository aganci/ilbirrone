class Visitor < ActiveRecord::Base
end

class AddConfirmedToVisitors < ActiveRecord::Migration
  def up
    add_column :visitors, :confirmed, :boolean, default: true
    Visitor.all.each do |visitor|
      visitor.confirmed = true
      visitor.save!
    end
  end

  def down
    remove_column :visitors, :confirmed
  end
end
