class RemovePubIdBeerIdFromImages < ActiveRecord::Migration
  def up
    remove_column :images, :pub_id
    remove_column :images, :beer_id
  end

  def down
    add_column :images, :beer_id, :integer
    add_column :images, :pub_id, :integer
  end
end
