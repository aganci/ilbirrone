class AddPostcodePhoneEmailWebsiteToPubs < ActiveRecord::Migration
  def change
    add_column :pubs, :postcode, :string
    add_column :pubs, :phone, :string
    add_column :pubs, :email, :string
    add_column :pubs, :website, :string
  end
end
