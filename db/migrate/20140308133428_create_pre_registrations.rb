class CreatePreRegistrations < ActiveRecord::Migration
  def change
    create_table :pre_registrations do |t|
      t.integer :user_id, null: false
      t.timestamps
    end
  end
end
