class RemoveNullConstraintFromVisitors < ActiveRecord::Migration
  def change
    change_column :visitors, :user_id, :integer, :null => true
  end
end
