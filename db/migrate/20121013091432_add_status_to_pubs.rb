class AddStatusToPubs < ActiveRecord::Migration
  class Pub < ActiveRecord::Base
    attr_accessible :status
  end

  def change
    add_column :pubs, :status, :string
    Pub.reset_column_information
    Pub.all.each do |pub|
      pub.update_attributes!(:status => 'approved')
    end
  end
end
