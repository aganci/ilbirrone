class CreateEventNotifications < ActiveRecord::Migration
  class Event < ActiveRecord::Base
  end

  class User < ActiveRecord::Base
  end

  class Notification < ActiveRecord::Base
    def self.notify_event(event)
      Notification.create!(notification_type: 'event', event: event, user: event.user)
    end

    attr_accessible :notification_type, :user, :event

    belongs_to :user
    belongs_to :event
  end

  def up
    Notification.connection.schema_cache.clear!
    Notification.reset_column_information
    User.connection.schema_cache.clear!
    User.reset_column_information
    Event.connection.schema_cache.clear!
    Event.reset_column_information

    Event.all.each do |event|
      n = Notification.notify_event(event)
      n.update_attribute :created_at, event.created_at
    end
  end
end
