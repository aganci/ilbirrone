class ChangeBirrificioToMicrobirrificio < ActiveRecord::Migration
  
  class Pub < ActiveRecord::Base
    attr_accessible :pub_type
  end


  def up
    Pub.reset_column_information
    Pub.all.select {|pub| pub.pub_type == "Birrificio"}.each do |pub|
      pub.update_attributes!(pub_type: "Microbirrificio")
    end
  end

  def down
    Pub.reset_column_information
    Pub.all.select {|pub| pub.pub_type == "Microbirrificio"}.each do |pub|
      pub.update_attributes!(pub_type: "Birrificio")
    end
  end
end
