class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.references :user, null: false
      t.references :pub, null: false

      t.timestamps
    end
    add_index :visitors, [:user_id, :pub_id], unique: true 
  end
end
