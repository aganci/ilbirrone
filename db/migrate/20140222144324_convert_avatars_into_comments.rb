class Image < ActiveRecord::Base
  attr_accessible :path, :width, :height, :imageable, :imageable_id, :imageable_type

  belongs_to :imageable, polymorphic: true
end

class Comment < ActiveRecord::Base
  attr_accessible :user_id, :user, :content, :commentable_id, :commentable_type

  belongs_to :user
  has_one :image, as: :imageable
end

class User < ActiveRecord::Base
  has_one :avatar, as: :commentable, class_name: 'Comment'
end

class ConvertAvatarsIntoComments < ActiveRecord::Migration
  def up
    Image.find_all_by_imageable_type('User').each do |image|
      user = image.imageable
      comment = user.build_avatar
      comment.image_type = "Cover"
      comment.image = image
      comment.user = user
      comment.created_at = image.created_at
      comment.save!
    end
  end

  def down
  end
end
