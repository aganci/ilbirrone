class AddSlugToEvents < ActiveRecord::Migration
  class Event < ActiveRecord::Base
    include FriendlyId
    attr_accessible :title
    friendly_id :title, use: [:slugged, :finders]
  end

  def change
    add_column :events, :slug, :string
    Event.all.each { |event| event.save! }
    add_index :events, :slug, unique: true
  end
end


