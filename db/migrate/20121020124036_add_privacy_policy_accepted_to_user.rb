class AddPrivacyPolicyAcceptedToUser < ActiveRecord::Migration
  def change
    add_column :users, :privacy_policy_accepted, :boolean
  end
end
