class AddAlcoholByVolumeColorToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :alcohol_by_volume, :string
    add_column :beers, :color, :string
  end
end
