class CreateBeerReviewNotifications < ActiveRecord::Migration
  class BeerReview < ActiveRecord::Base
    belongs_to :user
    belongs_to :beer
  end
  
  class Beer < ActiveRecord::Base
  end

  class User < ActiveRecord::Base
  end

  class Notification < ActiveRecord::Base
    def self.notify_beer_review(review)
      Notification.create!(notification_type: 'beer_review', beer_review: review, beer: review.beer, user: review.user)
    end

    attr_accessible :notification_type, :pub, :review, :user, :comment, :event, :beer, :beer_review

    belongs_to :user
    belongs_to :beer
    belongs_to :beer_review
  end

  def up
    Notification.connection.schema_cache.clear!
    Notification.reset_column_information
    User.connection.schema_cache.clear!
    User.reset_column_information
    Beer.connection.schema_cache.clear!
    Beer.reset_column_information
    BeerReview.connection.schema_cache.clear!
    BeerReview.reset_column_information

    BeerReview.all.each do |beer_review|
      n = Notification.notify_beer_review(beer_review)
      n.update_attribute :created_at, beer_review.created_at
    end
  end
end
