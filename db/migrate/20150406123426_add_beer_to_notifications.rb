class AddBeerToNotifications < ActiveRecord::Migration
  def change
    add_reference :notifications, :beer, index: true
  end
end
