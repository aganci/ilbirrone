class AddImageToComments < ActiveRecord::Migration
  def change
    add_column :comments, :path, :string
    add_column :comments, :width, :integer
    add_column :comments, :height, :integer
  end
end
