class AddSlugToBeer < ActiveRecord::Migration
  def change
    add_column :beers, :slug, :string
    Beer.all.each { |beer| beer.save }
    add_index :beers, :slug, unique: true
  end
end
