class AddAdimToUsers < ActiveRecord::Migration
  class User < ActiveRecord::Base
    attr_accessible :admin
  end

  def change
    add_column :users, :admin, :boolean, null: false, default: false
    User.reset_column_information
    User.all.each do |user|
      user.update_attributes!(admin: false)
    end
  end
end
