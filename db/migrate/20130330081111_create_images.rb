class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :path, null: false
      t.integer :width, null: false
      t.integer :height, null: false
      t.integer :pub_id
      t.integer :beer_id

      t.timestamps
    end
  end
end
