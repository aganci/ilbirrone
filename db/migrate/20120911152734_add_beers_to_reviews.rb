class AddBeersToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :beers, :integer
  end
end
