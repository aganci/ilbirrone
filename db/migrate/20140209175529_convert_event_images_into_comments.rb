class ConvertEventImagesIntoComments < ActiveRecord::Migration
  Comment.reset_column_information

  def up
    Image.find_all_by_imageable_type('Event').each do |image|
      event = image.imageable
      comment = event.comments.build
      comment.image_type = "Cover"
      comment.image = image
      comment.user = event.user
      comment.created_at = image.created_at
      comment.save!
    end
  end

  def down
  end
end
