class AddFoodServiceValueAtmosphereToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :food, :integer
    add_column :reviews, :service, :integer
    add_column :reviews, :value, :integer
    add_column :reviews, :atmosphere, :integer
  end
end
