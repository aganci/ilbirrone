class Image < ActiveRecord::Base
  attr_accessible :path, :width, :height

  belongs_to :imageable, polymorphic: true
end  

class Pub < ActiveRecord::Base
  has_many :images, as: :imageable
  has_many :events
end

class Event < ActiveRecord::Base
  belongs_to :pub
  has_many :images, as: :imageable  
end

class CopyPubImagesToEvent < ActiveRecord::Migration
  def up
    Event.all.each do |event|
      pub = event.pub
      pub.images.each do |image|
        event_image = event.images.build
        event_image.path = image.path
        event_image.width = image.width
        event_image.height = image.height
        event_image.save!
      end
    end
  end
end
