class ConvertImagesToComments < ActiveRecord::Migration
  Comment.reset_column_information
  
  class Image < ActiveRecord::Base
    attr_accessible :path, :width, :height, :imageable, :imageable_id, :imageable_type
  end

  def up
    Image.all.each do |image|
      comment = Comment.find(image.imageable_id)
      comment.path = image.path
      comment.width = image.width
      comment.height = image.height
      comment.save!
    end
  end

  def down
  end
end
