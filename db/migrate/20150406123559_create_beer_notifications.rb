class CreateBeerNotifications < ActiveRecord::Migration
  class Beer < ActiveRecord::Base
    belongs_to :pub
    belongs_to :user
  end
  
  class Pub < ActiveRecord::Base
  end

  class User < ActiveRecord::Base
  end

  class Notification < ActiveRecord::Base
    def self.notify_beer(beer)
      Notification.create!(notification_type: 'beer', beer: beer, pub: beer.pub, user: beer.user)
    end

    attr_accessible :notification_type, :pub, :user, :beer

    belongs_to :pub
    belongs_to :user
    belongs_to :beer
  end

  def up
    Notification.connection.schema_cache.clear!
    Notification.reset_column_information
    User.connection.schema_cache.clear!
    User.reset_column_information
    Beer.connection.schema_cache.clear!
    Beer.reset_column_information
    Pub.connection.schema_cache.clear!
    Pub.reset_column_information

    Beer.all.each do |beer|
      n = Notification.notify_beer(beer)
      n.update_attribute :created_at, beer.created_at
    end
  end
end
