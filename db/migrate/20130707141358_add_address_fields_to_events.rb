class AddAddressFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :location, :string
    add_column :events, :address, :string
    add_column :events, :city, :string
    add_column :events, :province, :string
    add_column :events, :region, :string
    add_column :events, :postcode, :string
    add_column :events, :latitude, :float
    add_column :events, :longitude, :float
  end
end
