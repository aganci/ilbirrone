class AddFrequenterToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :frequenter, :boolean
  end
end
