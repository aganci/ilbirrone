class AddRegionToPubs < ActiveRecord::Migration
  def change
    add_column :pubs, :region, :string
  end
end
