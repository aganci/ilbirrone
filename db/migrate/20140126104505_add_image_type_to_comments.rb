class AddImageTypeToComments < ActiveRecord::Migration
  def change
    add_column :comments, :image_type, :string
  end
end
