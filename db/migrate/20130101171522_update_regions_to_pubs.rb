require 'place'

class UpdateRegionsToPubs < ActiveRecord::Migration
  class Pub < ActiveRecord::Base
    attr_accessible :province, :region
  end

  def up
    Pub.all.each do |pub|
      begin
        pub.region = Place.region_of(pub.province)
        pub.save!
      rescue
        puts "Unknown region of #{pub.id} #{pub.name} #{pub.province}"
      end
    end
  end
end
