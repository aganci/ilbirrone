class AddTechnicalColumnsToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :fermentation, :string
    add_column :beers, :family, :string
    add_column :beers, :style, :string
    add_column :beers, :beer_type, :string
    add_column :beers, :serving_temperature, :string
    add_column :beers, :description, :text
  end
end
