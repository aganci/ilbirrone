class CreateUserRegistrationNotification < ActiveRecord::Migration
  class User < ActiveRecord::Base
  end

  class Notification < ActiveRecord::Base
    def self.notify_user_registration(user)
      Notification.create!(notification_type: 'user_registration', user: user)
    end

    attr_accessible :notification_type, :user

    belongs_to :user
  end

  def up
    User.all.each do |user|
      n = Notification.notify_user_registration(user)
      n.update_attribute :created_at, user.created_at
    end
  end
end
