class AddSlugToPubs < ActiveRecord::Migration
  def change
    add_column :pubs, :slug, :string
    Pub.all.each { |pub| pub.save }
    add_index :pubs, :slug, unique: true
  end
end
