class RemoveNullConstraintToPubIdOnEvents < ActiveRecord::Migration
  def change
    change_column :events, :pub_id, :integer, :null => true
  end
end
