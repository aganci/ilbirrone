class RemoveConstraintFromComments < ActiveRecord::Migration
  def change
    change_column :pubs, :country, :string, :null => true
  end
end
