class AddCommentableToComments < ActiveRecord::Migration
  def up
    add_column :comments, :commentable_id, :integer
    add_column :comments, :commentable_type, :string

    Comment.all.each do |comment|
      comment.commentable_id = comment.pub_id
      comment.commentable_type = 'Pub'
      comment.save!
    end 
  end
end
