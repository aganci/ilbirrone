class CreateBeers < ActiveRecord::Migration
  def change
    create_table :beers do |t|
      t.string :name, null: false
      t.integer :pub_id, null: false
      t.integer :user_id, null: false

      t.timestamps
    end

  end

end
