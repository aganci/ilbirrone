class AddBeerReviewToNotification < ActiveRecord::Migration
  def change
    add_reference :notifications, :beer_review, index: true
  end
end
