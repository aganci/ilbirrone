class RemovePubIdFromComments < ActiveRecord::Migration
  def up
    remove_column :comments, :pub_id
  end
end
