class PubReviewsNotifications < ActiveRecord::Migration
  
  class Notification < ActiveRecord::Base
    def self.notify_pub_review(review)
      Notification.create!(notification_type: 'review', review: review, pub: review.pub, user: review.user, created_at: review.created_at)
    end

    attr_accessible :notification_type, :pub, :review, :user

    belongs_to :pub
    belongs_to :review
    belongs_to :user
  end

  class Review < ActiveRecord::Base
    attr_accessible :pub, :user
    
    belongs_to :pub
    belongs_to :user
  end

  class Pub < ActiveRecord::Base
  end

  def up
    Review.all.each do |review|
      n = Notification.notify_pub_review(review)
      n.update_attribute :created_at, review.created_at
    end
  end
end
