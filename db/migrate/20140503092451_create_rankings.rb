class CreateRankings < ActiveRecord::Migration
  def change
    create_table :rankings do |t|
      t.references :pub, null: false
      t.integer :position, null: false

      t.timestamps
    end
    add_index :rankings, :pub_id
  end
end
