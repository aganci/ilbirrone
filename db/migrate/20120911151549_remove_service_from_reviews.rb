class RemoveServiceFromReviews < ActiveRecord::Migration
  def up
    remove_column :reviews, :service
  end

  def down
    add_column :reviews, :service, :integer
  end
end
