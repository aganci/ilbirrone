class CreatePubs < ActiveRecord::Migration
  def change
    create_table :pubs do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :province

      t.timestamps
    end
  end
end
