class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :content, null: false
      t.references :pub, null: false
      t.references :user, null: false

      t.timestamps
    end
    add_index :comments, :pub_id
    add_index :comments, :user_id
  end
end
