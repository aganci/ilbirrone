class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :pub_id, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.string :title, null: false
      t.text :description, null: false
      t.string :website

      t.timestamps
    end
  end
end
