class AddIndexToVisitors < ActiveRecord::Migration
  def change
    remove_index :visitors, [:user_id, :pub_id]
  end
end
