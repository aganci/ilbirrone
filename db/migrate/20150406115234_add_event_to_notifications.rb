class AddEventToNotifications < ActiveRecord::Migration
  def change
    add_reference :notifications, :event, index: true
  end
end
