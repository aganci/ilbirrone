class CreateUserNotifications < ActiveRecord::Migration
  class Comment < ActiveRecord::Base
    belongs_to :user
    belongs_to :commentable, polymorphic: true
  end

  class User < ActiveRecord::Base
  end

  class Notification < ActiveRecord::Base
    def self.notify_user_comment(comment)
      Notification.create!(notification_type: 'user_comment', comment: comment, user: comment.user)
    end

    attr_accessible :notification_type, :user, :comment

    belongs_to :user
    belongs_to :comment
  end

  def up
    Notification.connection.schema_cache.clear!
    Notification.reset_column_information
    Comment.connection.schema_cache.clear!
    Comment.reset_column_information
    User.connection.schema_cache.clear!
    User.reset_column_information


    Comment.where(commentable_type: 'User', image_type: nil).each do |comment|
      n = Notification.notify_user_comment(comment)
      n.update_attribute :created_at, comment.created_at
    end
  end
end
