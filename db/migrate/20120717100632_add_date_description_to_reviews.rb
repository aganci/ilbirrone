class AddDateDescriptionToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :date, :date
    add_column :reviews, :description, :text
  end
end
