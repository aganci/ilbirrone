class AddSensoryAnalysisToBeerReviews < ActiveRecord::Migration
  def change
    add_column :beer_reviews, :aroma, :string
    add_column :beer_reviews, :appearance, :string
    add_column :beer_reviews, :taste, :string
    add_column :beer_reviews, :palate, :string
  end
end
