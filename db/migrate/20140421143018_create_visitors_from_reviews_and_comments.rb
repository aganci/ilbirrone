class Pub < ActiveRecord::Base
end

class User < ActiveRecord::Base
end

class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :commentable, polymorphic: true
end

class Review < ActiveRecord::Base
  belongs_to :pub
  belongs_to :user
end


class CreateVisitorsFromReviewsAndComments < ActiveRecord::Migration
  def up
    Review.all.each do |review|
      next if Visitor.where(pub_id: review.pub_id).where(user_id: review.user_id).present?

      visitor = Visitor.new
      visitor.pub = review.pub
      visitor.user = review.user

      visitor.save!
    end

    Comment.where(commentable_type: 'Pub').all.each do |comment|
      next if Visitor.where(pub_id: comment.commentable.id).where(user_id: comment.user.id).present?
      
      visitor = Visitor.new
      visitor.pub = comment.commentable
      visitor.user = comment.user
      visitor.save!
    end

  end

end
